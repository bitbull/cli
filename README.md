## Bitbull CLI

A collection of useful scripts to deploy, init and configure PHP applications.

### Install

#### Using installation script

Use the installation script hosted from https://cli.bitbull.io/install, is it possibile to pass a parameter with the desidered version (default is "latest").

```bash
curl -s https://cli.bitbull.io/install | sudo bash -s latest
```

#### Manually download and nstall phar file

Download the latest stable phar-file:

```bash
wget https://cli.bitbull.io/dev/bb-cli.phar
```

Now you can make the phar-file executable:

```bash
chmod +x ./bb-cli.phar
```

And now you can run it using
```bash
./bb-cli.phar --version
```

If you want to use the command system wide you can copy it to /usr/local/bin (for both Mac and Linux users).

```bash
sudo mv ./bb-cli.phar /usr/local/bin/bb-cli
```

and use it
```bash
bb-cli --version
```

#### Composer project

Require it in you project

```bash
composer require bitbull/cli
```

Use it from `vendor/bin`

```bash
./vendor/bin/bb-cli --version
```

#### Global composer require

```bash
composer global require bitbull/cli
```

Use it using
```bash
bb-cli --version
```

#### Using Docker image

Copy this snippet into `/usr/local/bin/bb-cli` file
```bash
#!/bin/bash

docker run -it --rm -v $(pwd):/app bitbull/cli:dev $@
```
make the script executable
```bash
sudo chmod +x /usr/local/bin/bb-cli
```
now you can use `bb-cli` command to execute a temporary Docker container without installing any other dependencies.

### Contributing

Clone this repo and install composer modules
```bash
composer install
```
#### Deployment

Build the PHAR srchive using the internal command `phar:build`
```bash
./bin/bb-cli phar:build
```

#### Add new commands

Commands are wrapped in different class inside `src\Commands`.
If you want to add new commands add new class in the commands directory
or edit and existing one adding the command function, remember to extend `Bitbull\Cli\Commands\BaseCommand` parent command.

```php
<?php

namespace Bitbull\Cli\Commands\CommandDirectoryHere;

use Bitbull\Cli\Commands\BaseCommand;
use Symfony\Component\Console\Input\InputOption;

class ClassNameCommand extends BaseCommand
{
    /**
     * Command description
     *
     * @param $myparameter
     * @param $opts array
     */
    function myCommandName($myparameter, $opts = [
        'param' => null,
        'flag' => false,
        'required' => InputOption::VALUE_REQUIRED,
        'withDefault' => 'defaultvalue',
    ])
    {
        // Use task here
    }
}
```

Command class are automatic loaded and Robo convert function name into command: `my:command-name`.

Follow the [Robo command guide](https://robo.li/getting-started/#commands) for more details.

#### Add new tasks

Task are simply helper function used to exec commands or editing file where the true intelligence resides.
Add new task in `src\Tasks` creating a new class or editing an existing one following the [Robo task guide](https://robo.li/extending/#creating-a-robo-extension).

```php
<?php

namespace Bitbull\Cli\Tasks\Utils;

use Bitbull\Cli\Tasks\BaseTask;
use Robo\Result;

class MyTask extends BaseTask {

    function run()
    {
        return Result::success($this);
    }
}
```

If your task use only a process call extends `Bitbull\Cli\Tasks\BaseTaskCommand` and implement the `getCommand` method.

```php
<?php

namespace Bitbull\Cli\Tasks\Utils;

use Bitbull\Cli\Tasks\BaseTaskCommand;

class MyCommandTask extends BaseTaskCommand {
    
    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return "ls";
    }
}

```

To load the task in command class you have to import the specific trait,
so when you add a new task remember to create the connected trait or adding the load method.
```php
<?php

namespace Bitbull\Cli\Tasks\Utils;

trait UtilsTasks
{
    protected function taskMyTask()
    {
        return $this->task(MyTask::class); //pass you task class
    }
    
    protected function taskMyTaskWithConstructorParam($param1)
    {
        return $this->task(MyCommandTask::class, $param1); //pass you task class with constructor params
    }
}
```

#### Add new applications

Application resides in `src\Application` directory, add new class extending `Bitbull\Cli\Application\BaseApplication`
```php
<?php
namespace Bitbull\Cli\Application;

use Bitbull\Cli\Application\BaseApplication;

class NewApplication extends BaseApplication
{
    // override parent methods
}
```
Then edit the `Bitbull\Cli\Application\ApplicationFactory` to handle the new application name in `app.type` config
```php
<?php

  /**
   * @return ApplicationInterface
   */
  public function getInstance()
  {

      switch ($this->type) {
          case "laravel":
              return new Laravel($this->rootPath);
              break;
          case "magento":
              return new Magento($this->rootPath);
              break;
          case "magentowp":
              return new MagentoWordpress($this->rootPath);
              break;
          case "magento2":
              return new Magento2($this->rootPath);
              break;
          case "wordpress":
              return new Wordpress($this->rootPath);
              break;
          case "composer":
              return new Composer($this->rootPath);
              break;
          case "newapplication": // add the new application
              return new NewApplication($this->rootPath);
              break;
      }

      return new Composer($this->rootPath);
  }
  
```

### Configuration file

Robo can load command and task options from a [yaml configuration file](https://robo.li/getting-started/#configuration),
anyway is possible to add any configuration structure and load the config value using dot notation:
```php
<?php

$value = Robo::Config()->get('app.config.custom', 'default value');
```

By default configurations are loaded from `.bb-cli.yml` file in the CWD,
it's possible to merge or override some values using a different configuration file:
```bash
./bb-cli
```

You can also use environment variable interpolation with bash-like syntax:
```yml
test:
  config: "${CONFIG_FILE}"
  locale:
    default: "${REGION}_${LANG}"
```

### Working directory

By default all commands will use the CWD base on where the command is called,
you can specify a different root path with option:
```bash
./bb-cli --root=/my/different/directory
```  
Remember that the configuration file location is based on this param,
so the default configuration will be searched in `/my/different/directory/.bb-cli.yml`
unless you use the configuration file options, example:

```bash
cd /project
./bb-cli
```
config file `/project/.bb-cli.yml` will be used

```bash
./bb-cli --root=/my/different/directory --config="/my/other/directory/env-override.yml"
```
config file `/my/different/directory/.bb-cli.yml` will be merged with `/my/other/directory/env-override.yml`

```bash
./bb-cli --root=/my/different/directory --config="env-override.yml"
```
config file `/my/different/directory/.bb-cli.yml` will be merged with `/project/env-override.yml`

### Application

To make life easier we created a set of command based on the application type, the most famous CMS/Framework are supported:
Wordpress
Laravel
Magento 1
Magento 2
Magento + Wordpress

Every application respect the `Bitbull\Cli\Application\ApplicationInterface` to expose build, install and deploy commands.

The selected application is loaded in the `Bitbull\Cli\Commands\BaseCommand` based on file configuration
```yml
app:
  type: "magento"
  version: "2.2"
```
or using `APP_TYPE` environment variable.

### Dependencies

Every application have different dependencies, this tool does not replace the application specific CLI.

#### Common requirements

- PHP > 7.0
- [wget](https://www.gnu.org/software/wget/)
- [mysql-cli](https://dev.mysql.com/doc/refman/5.7/en/mysql.html)
- [mysqldump](https://dev.mysql.com/doc/refman/5.7/en/mysqldump.html)
- [git](https://git-scm.com/)

#### Wordpress

- [WP-CLI](http://wp-cli.org/it/)

#### Magento 1

- [n98-magerun](https://github.com/netz98/n98-magerun)

#### Magento 2

- [n98-magerun2](https://github.com/netz98/n98-magerun2)
- [magento-cli](http://devdocs.magento.com/guides/v2.0/config-guide/cli/config-cli-subcommands.html)

#### Laravel

- [artisan](https://laravel.com/docs/artisan)

### CLI Options

Option                               | Description
-------------------------------------|------------------------------------ 
-h, --help                           | Display this help message
-q, --quiet                          | Do not output any message
-V, --version                        | Display this application version
    --ansi                           | Force ANSI output
    --no-ansi                        | Disable ANSI output
-n, --no-interaction                 | Do not ask any interactive question
    --simulate                       | Run in simulated mode (show what would have happened).
    --progress-delay=PROGRESS-DELAY  | Number of seconds before progress bar is displayed in long-running task collections. Default: 2s. [default: 2]
-D, --define=DEFINE                  | Define a configuration item value. (multiple values allowed)
    --config[=CONFIG]                | Configuration file.
    --root[=ROOT]                    | Custom root path.
-v, -vv, -vvv, --verbose             | Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug


### Available commands

#### Base

Command                      | Description
-----------------------------|------------------------------------   
help                         | Displays help for a command
list                         | Lists commands
self:install                 | Install CLI
self:remove                  | Remove CLI

#### Configuration

Command                      | Description
-----------------------------|------------------------------------  
config:env-interpolate       | Interpolate environment variable into file

#### Application

Command                      | Description
-----------------------------|------------------------------------   
app:anonymize                | Anonymize application data
app:build                    | Build application
app:cache-clean              | Clean application cache
app:cache-disable            | Disable application cache
app:cache-enable             | Enable application cache
app:clean                    | Clean application directory
app:config-generate          | Create application config file
app:update-database          | Update application database

#### CDN

Command                      | Description
-----------------------------|------------------------------------
cdn:cloudfront-invalidate    | Invalidate CDN

#### Development

Command                      | Description
-----------------------------|------------------------------------
dev:dump-config              | Dump config
dev:dump-cwd                 | Get current working directory
dev:dump-php                 | Dump PHP infos
dev:image-minify             | Minify Images
dev:javascript-minify        | Minify Javascript
dev:less-compile             | Compile Less
dev:scss-compile             | Compile Scss
dev:style-minify             | Minify Style
dev:watch                    | Watch for file changes

#### Phar

Command                      | Description
-----------------------------|------------------------------------
phar:build                   | Build the phar executable
phar:disable-write           | Deny phar creation
phar:enable-write            | Allow phar creation

#### GIT

Command                      | Description
-----------------------------|------------------------------------
git:update-changelog         | Update Changelog

#### Init
Command                      | Description
-----------------------------|------------------------------------
init:bitbucket               | Init Bitbucket pipeline
init:gitlab                  | Init Gitlab pipeline
init:travis                  | Init Travis pipeline

#### Local environment

Command                      | Description
-----------------------------|------------------------------------
local:docker-down            | Destroy local Docker environment
local:docker-init            |  Create Docker compose file
local:docker-start           | Start Docker environment
local:docker-stop            | Stop local Docker environment
local:docker-up              | Create local Docker environment
local:vagrant-down           | Destroy local Vagrant environment
local:vagrant-init           | Create Vagrant file
local:vagrant-start          | Start local Vagrant environment
local:vagrant-stop           | Stop local Vagrant environment
local:vagrant-up             | Create local Vagrant environment

#### Pipeline CI/CD

Command                         | Description
--------------------------------|------------------------------------
check:grumphp                   | Check CodeStyle using grumphp
check:phpcs                     | Check CodeStyle using php cs
check:phpmd                     | Check CodeStyle using php md
test:phpunit                    | Test application using PHP Unit
pipeline:deploy-codedeploy      | Deploy application using AWS CodeDeploy
pipeline:deploy-docker          | Deploy application to Docker repository
pipeline:deploy-dockercomposeui | Deploy application using Docker Compose UI
pipeline:deploy-sftp            | Deploy application using SFTP
pipeline:release-activate       | Activated release using blue-green
pipeline:release-install        | Install application
pipeline:remote-ssh             | Deploy application to a remote server with SSH

#### Sync Environment

Command                      | Description
-----------------------------|------------------------------------
sync:database-anonymize      | Anonymize database
sync:database-create         | Create Database
sync:database-dump           | Create dump from database
sync:database-dump-download  | Download database dump
sync:database-dump-upload    | Upload database dump
sync:database-restore        | Restore database from dump
sync:media-archive           | Create media archive
sync:media-archive-download  | Download media archive
sync:media-archive-upload    | Upload media archive
sync:media-extract           | Extract media archive

#### WebServer

Command                            | Description
-----------------------------------|------------------------------------
webserver:apache-auth              | Add http auth
webserver:apache-forwarded-header  | Patch htaccess behind a proxy
webserver:apache-httpsredirect     | Add http to https redirect

#### Cloud Management

Command                      | Description
-----------------------------|------------------------------------ 
aws:ami-update               | Update AMI
aws:s3-share                 | Create sharing link for S3 bucket object
aws:security-group-export    | Export security group
aws:security-group-import    | Import security group dump
aws:sns-publish              | Send message to SNS topic
aws:ssm-describe-command     | Describe SSM command
newrelic:deployment          | Notify new deployment on New Relic
rundeck:job-execute          | Execute Rundeck Job
maxmind:database-update      | Update and check MaxMind Database
maxmind:ipinfo               | Retrieve IP info from MaxMind database 

#### Swarm

Command                      | Description
-----------------------------|------------------------------------ 
portainer:execute-command    | Execute command inside container
portainer:service-update     | Deploy application to portainer
portainer:stack-delete       | Delete portainer stack
portainer:stack-update       | Deploy application to portainer
swarm:execute-command        | Execute command inside container