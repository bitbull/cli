<?php

namespace Bitbull\Cli\Commands\Webserver;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Apache\ApacheTasks;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;

class ApacheCommand extends BaseCommand
{
    use ApacheTasks;

    /**
     * Add http auth
     *
     * @param string $htaccessFile
     * @param string $authFile
     * @param string $authFileAbsolutePath
     *      the final absolute path to set for $authFile
     *      (cannot be autocomputed at build time, and apache does not allow relative paths)
     * @param $opts array
     */
    function webserverApacheAuth($htaccessFile, $authFile, $authFileAbsolutePath, $opts = [
        'users' => [],
        'excludePaths' => [],
        'excludeIPs' => [],
        'excludeUserAgents' => [],
    ])
    {
        $this->say("Generating auth file..");

        $task = $this->taskHtpasswd($authFile);
        foreach ($opts['users'] as $user) {
            $parts = explode(':', $user);
            $task->addUser($parts[0], $parts[1]);
        }
        $result = $task->run();

        if(!$result->wasSuccessful()){
            return;
        }
        $this->say("Auth file generated");

        $this->say("Patching htaccess file..");
        $task = $this->taskWriteToFile($htaccessFile)
            ->append()
            ->line('')
            ->line('############################################')
            ->line('## HTTP Authentication')
            ->line('AuthType Basic')
            ->line('AuthName "Authentication required"')
            ->line('AuthUserFile ' . $authFileAbsolutePath)
            ->line('Require valid-user')
            ->line('');

        foreach ($opts['excludePaths'] as $path) {
            $task->line('SetEnvIf Request_URI '.$path.' allow');
        }

        foreach ($opts['excludeIPs'] as $ip) {
            $task->line('SetEnvIf X-Forwarded-For '.$ip.' allow');
            $task->line('SetEnvIf Remote_Addr '.$ip.' allow');
        }

        foreach ($opts['excludeUserAgents'] as $ua) {
            $task->line('SetEnvIf User-Agent '.$ua.' allow');
        }

        $task->line('SetEnvIf Remote_Addr ^127\.0\.0\.1 allow');

        $task->line('')
            ->line('Order allow,deny')
            ->line('Allow from env=allow')
            ->line('Allow from env=REDIRECT_allow')
            ->line('Satisfy any')
            ->line('');

        $result = $task->run();
        if($result->wasSuccessful()){
            $this->say("Htaccess file patched");
        }
    }

    /**
     * Add http to https redirect
     *
     * @param $htaccessFile
     * @param $opts array
     */
    function webserverApacheHTTPSRedirect($htaccessFile, $opts = [
        'redirectCode' => '301',
    ])
    {
        $this->say("Patching htaccess for HTTPS redirect..");
        $task = $this->taskWriteToFile($htaccessFile)
            ->append()
            ->line('')
            ->line('############################################')
            ->line('## Redirect HTTP to HTTPS')
            ->line('RewriteCond %{HTTP:X-Forwarded-Proto} =http [OR]')
            ->line('RewriteCond %{HTTPS} off')
            ->line('RewriteCond %{REMOTE_HOST} !^127\.0\.0\.1')
            ->line('RewriteRule . https://%{HTTP:Host}%{REQUEST_URI} [L,R='.$opts['redirectCode'].']')
            ->line('')
        ;
        $result = $task->run();

        if($result->wasSuccessful()){
            $this->say("Htaccess file patched");
        }
    }

    /**
     * Patch htaccess behind a proxy
     *
     * @param $htaccessFile
     */
    function webserverApacheForwardedHeader($htaccessFile)
    {
        $this->say("Patching htaccess for proxy..");

        $result = $this->taskWriteToFile($htaccessFile)
            ->append()
            ->line('')
            ->line('############################################')
            ->line('## Use X-Forwarded-Proto to check https')
            ->line('SetEnvIf X-Forwarded-Proto https HTTPS=on')
            ->line('')
            ->run();

        if($result->wasSuccessful()){
            $this->say("Htaccess file patched");
        }
    }

    /**
     * Patch htaccess to pass ENV variable to WEB application
     *
     * @param $variables string with CSV format
     * @param $htaccessFile
     */
    function webserverApacheEnvVariable($variables, $htaccessFile = '.htaccess')
    {
        $this->say("Patching htaccess for environment variable..");

        $result = $this->taskWriteToFile($htaccessFile)
            ->append()
            ->line('')
            ->line('############################################')
            ->line('## Add custom environment variables')
            ->line('PassEnv '.implode(' ', explode(',', $variables)))
            ->line('')
            ->run();

        if($result->wasSuccessful()){
            $this->say("Htaccess file patched");
        }
    }

}
