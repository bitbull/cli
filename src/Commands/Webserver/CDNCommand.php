<?php

namespace Bitbull\Cli\Commands\Webserver;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\AWS\AWSTasks;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;

class CDNCommand extends BaseCommand
{
    use AWSTasks;

    /**
     * Invalidate CDN
     *
     * @param $distribution
     * @param $opts array
     */
    function cdnCloudfrontInvalidate($distribution, $opts = [
        'profile' => null,
        'paths' => ['/*'],
    ])
    {
        $this->say("Invalidating CDN paths..");

        $result = $this->taskCloudFrontInvalidation($distribution)
            ->region('us-west-1')
            ->profile($opts['profile'])
            ->paths($opts['paths'])
            ->run();

        if($result->wasSuccessful()){
            $this->say("CDN paths invalidated");
        }
    }

}
