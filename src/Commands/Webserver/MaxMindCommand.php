<?php

namespace Bitbull\Cli\Commands\Webserver;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\MaxMind\MaxMindTasks;

class MaxMindCommand extends BaseCommand
{
    use MaxMindTasks;

    const DOWNLOAD_URL_CITY_MMDB = 'http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz';
    const DOWNLOAD_URL_CITY_MMDB_CHECKSUM = 'http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz.md5';

    const DOWNLOAD_URL_COUNTRY_MMDB = 'http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz';
    const DOWNLOAD_URL_COUNTRY_MMDB_CHECKSUM = 'http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz.md5';

    const DOWNLOAD_URL_ASN_MMDB = 'http://geolite.maxmind.com/download/geoip/database/GeoLite2-ASN.tar.gz';
    const DOWNLOAD_URL_ASN_MMDB_CHECKSUM = 'http://geolite.maxmind.com/download/geoip/database/GeoLite2-ASN.tar.gz.md5';

    protected $databaseTypes = [
        'city',
        'country',
        'asn',
    ];

    protected $databaseFormats = [
        'mmdb'
    ];

    /**
     * Update MaxMind database file
     *
     * @param $destFilePath string
     * @param $opts array
     * @option $format database file format, could be mmdb or csv
     * @option $type database type, could be city, country or asn
     * @option $integrityCheck check database integrity
     */
    function maxmindDatabaseUpdate($destFilePath = '/usr/local/share/GeoIP/', $opts = [
        'type' => 'city',
        'format' => 'mmdb',
        'integrityCheck' => false,
    ]){
        if (in_array($opts['type'], $this->databaseTypes, true) === false) {
            $allowedValues = implode(',', $this->databaseTypes);
            $this->taskEmptyError("Type '${opts['type']}' is not supported, allowed values are: ${allowedValues}")->run();
        }

        if (in_array($opts['format'], $this->databaseFormats, true) === false) {
            $allowedValues = implode(',', $this->databaseFormats);
            $this->taskEmptyError("Format '${opts['format']}' is not supported, allowed values are: ${allowedValues}")->run();
        }

        $fileKey = strtoupper("${opts['type']}_${opts['format']}");
        $downloadUrl = constant("self::DOWNLOAD_URL_$fileKey");
        $ext = pathinfo($downloadUrl, PATHINFO_EXTENSION);
        if ($ext === 'gz' && strpos($downloadUrl, 'tar.gz') === 0) {
            $ext = 'tar.gz';
        }

        $this->say("Updating MaxMind database from $downloadUrl..");

        $collection = $this->collectionBuilder();
        $work = $collection->tmpDir();

        $collection->taskDownload($downloadUrl)
            ->from($downloadUrl)
            ->to("$work/maxmind.$ext");

        $collection->taskExtract("$work/maxmind.$ext")
            ->to("$work/maxmind_extracted");

        $fileName = $this->getDefaultFileName($opts['type'], $opts['format']);
        if ($fileName === null) {
            $collection->taskEmptyError("Combination of format '${opts['format']}' and type '${opts['type']}' is not supported");
        }

        if ($opts['integrityCheck'] === true) {
            $collection->taskMaxMindIPInfo('216.58.212.238')
                ->database("$work/maxmind_extracted/$fileName")
                ->type($opts['type']);
        }

        if ($this->_isDirectory($destFilePath)) {
            if (substr($destFilePath, -1) !== '/'){
                $destFilePath .= '/';
            }
            $destFilePath .= $fileName;
        }

        $collection->taskFilesystemStack()
            ->copy("$work/maxmind_extracted/$fileName", $destFilePath, true);

        $result = $collection->run();
        if($result->wasSuccessful()){
            $this->say('MaxMind database updated');
        }

    }

    /**
     * MaxMind get IP info
     *
     * @param $dbFilePath string
     * @param $ip string
     * @param $opts array
     * @option $format database file format, could be mmdb or csv
     * @option $type database type, could be city, country or asn
     */
    function maxmindIPInfo($ip, $dbFilePath = '/usr/local/share/GeoIP/', $opts = [
        'type' => 'city',
        'format' => 'mmdb',
    ]){

        if ($this->_isDirectory($dbFilePath)) {
            $fileName = $this->getDefaultFileName($opts['type'], $opts['format']);
            if ($fileName === null) {
                $this->taskEmptyError("Combination of format '${opts['format']}' and type '${opts['type']}' is not supported");
            }
            if (substr($dbFilePath, -1) !== '/'){
                $dbFilePath .= '/';
            }
            $dbFilePath .= $fileName;
        }

        $this->say("Retrieving IP info from MaxMind database $dbFilePath..");
        $result = $this->taskMaxMindIPInfo($ip)
            ->database($dbFilePath)
            ->type($opts['type'])
            ->run();

        if($result->wasSuccessful()){
            $infos = $result->getData()['infos'];

            switch ($opts['type']) {
                case 'city':
                    $this->say('Country name: '.$infos->country->name);
                    $this->say('ISO code: '.$infos->country->isoCode);
                    $this->say('City name: '.$infos->city->name);
                    $this->say('Postal Code: '.$infos->postal->code);
                    $this->say('Latitude: '.$infos->location->latitude);
                    $this->say('Longitude: '.$infos->location->longitude);
                    break;
                case 'country':
                    $this->say('Country name: '.$infos->country->name);
                    $this->say('ISO code: '.$infos->country->isoCode);
                    break;
                case 'asn':
                    $this->say('Autonomous system number: '.$infos->autonomousSystemNumber);
                    $this->say('Autonomous system organization: '.$infos->autonomousSystemOrganization);
                    break;
            }
        }
    }

    /**
     * Get default file name
     *
     * @param $type
     * @param $format
     * @return string|null
     */
    private function getDefaultFileName($type, $format) {
        $fileKey = strtoupper("${type}_${format}");

        $fileName = null;
        switch ($fileKey) {
            case 'CITY_MMDB':
                $fileName = 'GeoLite2-City.mmdb';
                break;
            case 'COUNTRY_MMDB':
                $fileName = 'GeoLite2-Country.mmdb';
                break;
            case 'ASN_MMDB':
                $fileName = 'GeoLite2-ASN.mmdb';
                break;
        }

        return $fileName;
    }
}
