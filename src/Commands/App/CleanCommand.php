<?php

namespace Bitbull\Cli\Commands\App;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Laravel\LaravelTasks;
use Bitbull\Cli\Tasks\Magento\MagentoTasks;
use Bitbull\Cli\Tasks\Magento2\Magento2Tasks;
use Bitbull\Cli\Tasks\TemplateFile\TemplateFileTasks;
use Bitbull\Cli\Tasks\Wordpress\WordpressTasks;

class CleanCommand extends BaseCommand
{
    use Magento2Tasks, MagentoTasks, LaravelTasks, WordpressTasks, TemplateFileTasks;

    /**
     * Clean application directory
     *
     * @param $opts
     */
    function appClean($opts = ['paths' => []])
    {
        $this->say("Cleaning paths..");

        $pathsToClean = array_merge($opts['paths'], $this->app->getUnwantedPaths());

        $self = $this;
        $pathsToClean = array_filter(
            array_map(function($path) {
                return "{$this->rootPath}/{$path}";
            }, $pathsToClean),
            function ($path) use ($self) {
                return $self->_exist($path);
        });

        $pathsToClean = array_merge(
            $pathsToClean,
            $this->findDirectories('.git', $this->rootPath),
            $this->findFiles('.gitignore', $this->rootPath)
        );

        $result = $this->taskFilesystemStack()->remove($pathsToClean)->run();

        if($result->wasSuccessful()){
            $this->say("Paths cleaned");
        }
    }

    /**
     * Anonymize application data
     *
     */
    function appAnonymize()
    {
        $this->say("Anonymizing data..");

        $result = $this->app->anonymizeData();

        if($result->wasSuccessful()){
            $this->say("Data anonymized");
        }
    }
}
