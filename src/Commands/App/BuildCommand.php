<?php

namespace Bitbull\Cli\Commands\App;

use Bitbull\Cli\Application\ApplicationFactory;
use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Laravel\LaravelTasks;
use Bitbull\Cli\Tasks\Magento\MagentoTasks;
use Bitbull\Cli\Tasks\Magento2\Magento2Tasks;
use Bitbull\Cli\Tasks\Wordpress\WordpressTasks;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;

class BuildCommand extends BaseCommand
{
    use Magento2Tasks, MagentoTasks, LaravelTasks, WordpressTasks;

    /**
     * Build application
     *
     * @param $opts
     */
    function appBuild($opts = [
        'prod' => false,
        'dev' => false
    ])
    {
        $env = 'local';
        if($opts['dev'] === true){
            $env = 'development';
        }
        if($opts['prod'] === true){
            $env = 'production';
        }

        $this->say("Building application for $env environment ..");

        $result = $this->app->build($env);
        if($result->wasSuccessful()){
            $this->say("Application build successfully");
        }
    }

    /**
     * Update application database
     *
     * @param $opts array
     */
    function appUpdateDatabase($opts = [
        'maintenance' => false,
    ])
    {
        $this->say("Updating application database..");

        if($opts['maintenance'] === true){
            $this->say("Activate maintenance mode");
            $this->app->maintenance(true);
        }

        $result = $this->app->updateDatabase();
        if($result->wasSuccessful()){
            $this->say("Application database updated successfully");
        }

        if($opts['maintenance'] === true){
            $this->say("Disable maintenance mode");
            $this->app->maintenance(false);
        }
    }

    /**
     * Install application
     *
     * @param $opts array
     */
    function appInstall($opts = [
        'type' => InputOption::VALUE_REQUIRED,
        'version' => InputOption::VALUE_REQUIRED,
    ])
    {
        $this->say("Installing application..");

        $appFactory = new ApplicationFactory($opts['type'], $this->rootPath);
        $newApp = $appFactory->getInstance();

        $result = $newApp->install();
        if($result->wasSuccessful()){
            $this->say("Application installed successfully");

            $this->say("Creating config..");
            $result = $newApp->createConfig();
            if($result->wasSuccessful()){
                $this->say("Application config created");

                $this->say("Building application..");
                $result = $newApp->build();
                if($result->wasSuccessful()){
                    $this->say("Application build successfully");
                }
            }
        }
    }

}
