<?php

namespace Bitbull\Cli\Commands\App;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Laravel\LaravelTasks;
use Bitbull\Cli\Tasks\Magento\MagentoTasks;
use Bitbull\Cli\Tasks\Magento2\Magento2Tasks;
use Bitbull\Cli\Tasks\TemplateFile\TemplateFileTasks;
use Bitbull\Cli\Tasks\Wordpress\WordpressTasks;
use Robo\Robo;

class ConfigCommand extends BaseCommand
{
    use Magento2Tasks, MagentoTasks, LaravelTasks, WordpressTasks, TemplateFileTasks;

    /**
     * Create application config file
     */
    function appConfigGenerate()
    {
        $this->say("Generating configuration file..");
        $result = $this->app->createConfig();
        if($result->wasSuccessful()){
            $this->say("Configuration file generated");
        }
    }
}
