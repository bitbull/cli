<?php

namespace Bitbull\Cli\Commands\App;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Laravel\LaravelTasks;
use Bitbull\Cli\Tasks\Magento\MagentoTasks;
use Bitbull\Cli\Tasks\Magento2\Magento2Tasks;
use Bitbull\Cli\Tasks\Wordpress\WordpressTasks;

class IndexCommand extends BaseCommand
{
    use Magento2Tasks, MagentoTasks, LaravelTasks, WordpressTasks;

    /**
     * Reindex application
     *
     * @param array $indexes
     */
    function appIndexReindex(array $indexes)
    {
        $this->say('Re-indexing application..');

        $cleanedIndexesValue = [];
        foreach ($indexes as $type) {
            if (strpos($type, ',')) {
                foreach (explode(',', $type) as $csvType) {
                    if (trim($type) === '') {
                        continue;
                    }
                    $cleanedIndexesValue[] = $csvType;
                }
            } else {
                $cleanedIndexesValue[] = $type;
            }
        }

        $result = $this->app->reindex($cleanedIndexesValue);

        if($result->wasSuccessful()){
            $this->say('Application re-indexed');
        }
    }

    /**
     * Reset application index
     *
     * @param array $indexes
     */
    function appIndexReset(array $indexes)
    {
        $this->say('Reset application index..');

        $cleanedIndexesValue = [];
        foreach ($indexes as $type) {
            if (strpos($type, ',')) {
                foreach (explode(',', $type) as $csvType) {
                    if (trim($type) === '') {
                        continue;
                    }
                    $cleanedIndexesValue[] = $csvType;
                }
            } else {
                $cleanedIndexesValue[] = $type;
            }
        }

        $result = $this->app->resetIndex($cleanedIndexesValue);

        if($result->wasSuccessful()){
            $this->say('Application index reset');
        }
    }

}
