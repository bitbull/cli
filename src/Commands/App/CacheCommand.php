<?php

namespace Bitbull\Cli\Commands\App;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Laravel\LaravelTasks;
use Bitbull\Cli\Tasks\Magento\MagentoTasks;
use Bitbull\Cli\Tasks\Magento2\Magento2Tasks;
use Bitbull\Cli\Tasks\Wordpress\WordpressTasks;

class CacheCommand extends BaseCommand
{
    use Magento2Tasks, MagentoTasks, LaravelTasks, WordpressTasks;

    /**
     * Clean application cache
     *
     * @param array $types
     */
    function appCacheClean(array $types)
    {
        $this->say("Cleaning application cache..");

        $cleanedTypesValue = [];
        foreach ($types as $type) {
            if (strpos($type, ',')) {
                foreach (explode(',', $type) as $csvType) {
                    if (trim($type) === '') {
                        continue;
                    }
                    $cleanedTypesValue[] = $csvType;
                }
            } else {
                $cleanedTypesValue[] = $type;
            }
        }

        $result = $this->app->cleanCache($cleanedTypesValue);

        if($result->wasSuccessful()){
            $this->say("Application cache cleaned");
        }
    }

    /**
     * Flush application cache
     *
     * @param array $types
     */
    function appCacheFlush(array $types)
    {
        $this->say("Flushing application cache..");

        $cleanedTypesValue = [];
        foreach ($types as $type) {
            if (strpos($type, ',')) {
                foreach (explode(',', $type) as $csvType) {
                    if (trim($type) === '') {
                        continue;
                    }
                    $cleanedTypesValue[] = $csvType;
                }
            } else {
                $cleanedTypesValue[] = $type;
            }
        }

        $result = $this->app->cleanFlush($cleanedTypesValue);

        if($result->wasSuccessful()){
            $this->say("Application cache flushed");
        }
    }

    /**
     * Disable application cache
     */
    function appCacheDisable()
    {
        $this->say("Disabling application cache..");

        $result = $this->app->setCache(false);

        if($result->wasSuccessful()){
            $this->say("Application cache disabled");
        }
    }

    /**
     * Enable application cache
     */
    function appCacheEnable()
    {
        $this->say("Enabling application cache..");

        $result = $this->app->setCache(true);

        if($result->wasSuccessful()){
            $this->say("Application cache enabled");
        }
    }
}
