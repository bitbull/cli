<?php

namespace Bitbull\Cli\Commands\Cloud\AWS;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\AWS\AWSTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;

class S3Bucket extends BaseCommand
{
    use UtilsTasks, AWSTasks;

    /**
     * Create sharing link for S3 bucket object
     *
     * @param $path
     * @param $opts
     */
    function AwsS3Share($path, $opts = [
        'profile' => null,
        'region' => null,
        'expiresIn' => '+20 minutes',
        'rawOutput' => false,
    ])
    {
        if ($opts['rawOutput'] === false) {
            $this->say("Creating presigned URL..");
        }

        $result = $this->taskS3PresignedURL($path)
            ->expiresIn($opts['expiresIn'])
            ->profile($opts['profile'])
            ->region($opts['region'])
            ->run();

        if ($result->wasSuccessful()) {
            $presigedUrl = $result->getData()['url'];
            if ($opts['rawOutput'] === false) {
                $this->say("You can download it from: $presigedUrl");
            }else{
                $this->output->writeln($presigedUrl);
            }
        }
    }

}
