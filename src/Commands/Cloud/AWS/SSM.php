<?php

namespace Bitbull\Cli\Commands\Cloud\AWS;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\AWS\AWSTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;

class SSM extends BaseCommand
{
    use UtilsTasks, AWSTasks;

    /**
     * Execute SSM command
     *
     * @param $documentName
     * @param $opts
     */
    function AwsSsmExecuteCommand($documentName, $opts = [
        'wait' => false,
        'profile' => null,
        'region' => null,
        'instanceIds' => []
    ])
    {
        $this->say('Checking command status..');
        $result = $this->taskSSMExecuteCommand($documentName)
            ->profile($opts['profile'])
            ->region($opts['region'])
            ->instanceIds($opts['instanceIds'])
            ->run();
        $commandId = $result->getData()['id'];

        if ($opts['wait'] === false) {
            $this->say("Command '$commandId' executed");
            return;
        }

        $this->say("Waiting for command '$commandId' to be finished..");
        $result = $this->taskSSMDescribeCommand($commandId)
            ->profile($opts['profile'])
            ->region($opts['region'])
            ->wait(true)
            ->run();

        $invocations = $result->getData()['invocations'];
        foreach ($invocations as $invocation) {
            $this->say("Command on instance '".$invocation['InstanceId']."' exited with status '".$invocation['Status']."'");
            if ($invocation['Status'] === 'Failed') {
                $this->taskEmptyError($invocation['CommandPlugins'][0]['Output'])->run();
                return;
            }
            $this->io()->block($invocation['CommandPlugins'][0]['Output']);
        }
    }

    /**
     * Describe SSM command
     *
     * @param $commandId
     * @param $opts
     */
    function AwsSsmDescribeCommand($commandId, $opts = [
        'wait' => false,
        'profile' => null,
        'region' => null,
    ])
    {
        $this->say('Checking command status..');
        $result = $this->taskSSMDescribeCommand($commandId)
            ->profile($opts['profile'])
            ->region($opts['region'])
            ->wait($opts['wait'])
            ->run();

        foreach ($result->getData()['invocations'] as $invocation) {
            $this->io()->table([
                'Instance',
                'DocumentName',
                'Comment',
                'Status',
            ], [[
                $invocation['InstanceId'],
                $invocation['DocumentName'],
                $invocation['Comment'],
                $invocation['Status'],
            ]]);
            $this->say('Command output:');
            $this->io()->block($invocation['CommandPlugins'][0]['Output']);
        }

    }

}
