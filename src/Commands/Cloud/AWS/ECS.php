<?php

namespace Bitbull\Cli\Commands\Cloud\AWS;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\AWS\AWSTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;

class ECS extends BaseCommand
{
    use UtilsTasks, AWSTasks;

    /**
     * Deploy ECS Service
     *
     * @param string $serviceName
     * @param $declarationFile
     * @param array $opts array of options:<br>
     *      <code>targetGroupNames</code>   list of LB Target Groups to bind to the Service
     *      <code>containerNames</code>     ordered (matches position in targetGroupNames) list of container names from the Task Definition that will receive traffic from the LB<br>
     *      <code>containerPorts</code>     ordered (matches position in targetGroupNames) list of container ports from the Task Definition that will receive traffic from the LB<br>
     * @throws \Exception
     */
    function AwsEcsServiceDeploy($serviceName, $declarationFile = null, $opts = [
        'profile'          => null,
        'region'           => null,
        'clusterName'      => null,
        'targetGroupNames' => [],
        'containerNames'   => [],
        'containerPorts'   => [],
    ])
    {
        $this->say('Reading declaration file..');
        if ($this->_exist($declarationFile) === false) {
            $this->taskEmptyError("Declaration file $declarationFile not found")->run();
            return;
        }

        $ext = pathinfo($declarationFile, PATHINFO_EXTENSION);
        switch ($ext) {
            case 'json':
                $declarationFileContent = $this->loadJsonFile($declarationFile);
                break;
            case 'yml':
                $declarationFileContent = $this->loadYamlFile($declarationFile);
                break;
            default:
                $this->taskEmptyError("Declaration file extension $ext not supported")->run();
                return;
        }
        $this->say("Declaration file $declarationFile read!");

        $targetGroupArns = [];
        foreach($opts['targetGroupNames'] as $targetGroupName) {
             $result = $this->taskALBDescribeTargetGroup($targetGroupName)
                ->profile($opts['profile'])
                ->region($opts['region'])
                ->run();
            $targetGroupArns[] = $result->getData()['arn'];
        }

        $this->say('Checking service..');
        $oldStopOnFail = $this->stopOnFail(false);
        $describeResult = $this->taskECSServiceDescribe($serviceName)
            ->profile($opts['profile'])
            ->region($opts['region'])
            ->clusterName($opts['clusterName'])
            ->run();
        $this->stopOnFail($oldStopOnFail);

        if($describeResult->wasSuccessful()) {
            $service = $describeResult->getData();
            if ($service['status'] === 'DRAINING') {
                $this->taskEmptyError("Service $serviceName is still draining, cannot be deployed at the moment")->run();
                return;
            }
        }
        $this->say("Service $serviceName can be deployed");

        $this->say('Deploying Service..');
        $taskCreate = $this->taskECSServiceCreate($serviceName)
            ->profile($opts['profile'])
            ->region($opts['region'])
            ->clusterName($opts['clusterName'])
            ->declaration($declarationFileContent);

        if (count($targetGroupArns) > 0) {
            $containerPorts = $opts['containerPorts'];
            $containerNames = $opts['containerNames'];
            foreach ($targetGroupArns as $i => $targetGroupArn) {
                $containerPort = isset($containerPorts[$i]) ? $containerPorts[$i] : '80';
                $containerName = isset($containerNames[$i]) ? $containerNames[$i] : $this->serviceName;
                $taskCreate->addLoadBalancerTarget($targetGroupArn, $containerName, $containerPort);
            }
        }

        $taskCreate->run();
        $this->say("Service $serviceName deployed!");
    }

}
