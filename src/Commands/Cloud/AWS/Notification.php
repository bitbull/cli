<?php

namespace Bitbull\Cli\Commands\Cloud\AWS;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\AWS\AWSTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;

class Notification extends BaseCommand
{
    use UtilsTasks, AWSTasks;

    /**
     * Send message to SNS topic
     *
     * @param $topicARN
     * @param $message
     */
    function AwsSnsPublish($topicARN, $message)
    {
        $this->say("Sending message to SNS..");

        $result = $this->taskSNSPublish($topicARN)
            ->message($message)
            ->run();

        if ($result->wasSuccessful()) {
            $this->say("Message sent");
        }
    }

}
