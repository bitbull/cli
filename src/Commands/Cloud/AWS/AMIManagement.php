<?php

namespace Bitbull\Cli\Commands\Cloud\AWS;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\AWS\AWSTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;

class AMIManagement extends BaseCommand
{
    use UtilsTasks, AWSTasks;

    /**
     * Update AMI
     *
     * @param $autoScalingGroup
     * @param $opts
     */
    function AwsAmiUpdate($autoScalingGroup, $opts = [
        'profile' => null,
        'region' => null,
        'noForceMinMax' => false
    ])
    {
        $this->say("Updating AMI..");

        $infos = $this->taskASGDescribe($autoScalingGroup)
            ->profile($opts['profile'])
            ->region($opts['region'])
            ->run()
            ->getData();

        $toUpdate = $infos['inServiceInstancesCount'];
        if ($toUpdate == 0) {
            $this->say("No instance to update");
            return;
        }
        $this->say("$toUpdate instances to update");

        $this->stopOnFail(false);
        for ($i = $toUpdate; $i > 0; $i--) {
            $this->say("[$i] Launching new instance..");
            $result = $this->taskASGScaleUp($autoScalingGroup)->waitUntilComplete()
                ->profile($opts['profile'])
                ->region($opts['region'])
                ->forceMax(!$opts['noForceMinMax'])
                ->run();
            if (!$result->wasSuccessful()){
                break;
            }
            $this->say("[$i] Removing old instance..");
            $result = $this->taskASGScaleDown($autoScalingGroup)->waitUntilComplete()
                ->profile($opts['profile'])
                ->region($opts['region'])
                ->forceMin(!$opts['noForceMinMax'])
                ->run();
            if (!$result->wasSuccessful()){
                break;
            }
        }
        $this->stopOnFail(true);

        $this->say("Resetting Auto Scaling Group parameters");
        $this->taskASGEdit($autoScalingGroup)
            ->minSize($infos['minSize'])
            ->maxSize($infos['maxSize'])
            ->desiredCapacity($infos['desiredCapacity'])
            ->profile($opts['profile'])
            ->region($opts['region'])->run();

        $this->say("AMI updated");
    }

}
