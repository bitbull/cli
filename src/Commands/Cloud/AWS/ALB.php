<?php

namespace Bitbull\Cli\Commands\Cloud\AWS;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\AWS\AWSTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;

class ALB extends BaseCommand
{
    use UtilsTasks, AWSTasks;

    /**
     * Deploy ALB target group
     *
     * @param $targetGroupName
     * @param $loadBalancerArn
     * @param $opts
     * @throws \Exception
     */
    function AwsAlbTargetGroupDeploy($targetGroupName, $loadBalancerArn = null, $opts = [
        'profile' => null,
        'region' => null,
        'listenerArns' => [],
        'port' => 80,
        'paths' => [],
        'hosts' => [],
        'stickiness' => null,
        'healthCheckPath' => '/'
    ])
    {
        if ($opts['listenerArns'] === []) {
            $this->say('No listeners provided, describing ALB listeners..');
            $result = $this->taskALBDescribeListeners($loadBalancerArn)
                ->profile($opts['profile'])
                ->region($opts['region'])
                ->run();
            $httpsListeners = array_filter($result->getData()['listeners'], function ($listener) {
                return $listener['Protocol'] === 'HTTPS' && $listener['Port'] === 443;
            });
            $opts['listenerArns'] = array_map(function ($httpsListener) {
                return $httpsListener['ListenerArn'];
            }, $httpsListeners);
            if (count($opts['listenerArns']) === 0) {
                $this->say('No ALB listeners found');
            } else {
                $this->say(count($opts['listenerArns']).' ALB listeners loaded!');
            }
        }

        $this->say('Deploying target group..');
        $result = $this->taskALBCreateTargetGroup($loadBalancerArn)
            ->profile($opts['profile'])
            ->region($opts['region'])
            ->name($targetGroupName)
            ->port($opts['port'])
            ->stickiness($opts['stickiness'])
            ->healthCheckPath($opts['healthCheckPath'])
            ->run();
        $targetGroupArn = $result->getData()['arn'];
        $this->say('Target group deployed!');

        foreach ($opts['listenerArns'] as $listenerArn) {
            $this->say("Creating rule for '$listenerArn'..");

            $taskRuleCreate = $this->taskALBRuleCreate($listenerArn)
                ->profile($opts['profile'])
                ->region($opts['region'])
                ->targetGroupArn($targetGroupArn);
            $targetGroupArns[] = $targetGroupArn;

            if (count($opts['paths']) > 0) {
                $taskRuleCreate->addPathsCondition($opts['paths']);
            }
            if (count($opts['hosts']) > 0) {
                $taskRuleCreate->addHostCondition($opts['hosts']);
            }
            $taskRuleCreate->run();

            $this->say('Rule created!');
        }
    }

    /**
     * Deploy ALB listener rule
     *
     * @param $listenerArn
     * @param $targetGroupArn
     * @param $opts
     * @throws \Exception
     */
    function AwsAlbListenerRuleDeploy($listenerArn, $targetGroupArn, $opts = [
        'profile' => null,
        'region' => null,
        'ruleIdentifier' => 'listener',
        'priority' => null,
        'paths' => [],
        'hosts' => []
    ])
    {
        $this->say("Deploying rule for '$listenerArn'..");

        $taskRuleCreate = $this->taskALBRuleCreate($listenerArn, $opts['priority'])
            ->useRuleIdentifier($opts['ruleIdentifier'])
            ->profile($opts['profile'])
            ->region($opts['region'])
            ->targetGroupArn($targetGroupArn);

        if (count($opts['paths']) > 0) {
            $taskRuleCreate->addPathsCondition($opts['paths']);
        }
        if (count($opts['hosts']) > 0) {
            $taskRuleCreate->addHostCondition($opts['hosts']);
        }
        if (count($opts['methods']) > 0) {
            $taskRuleCreate->addMethodCondition($opts['methods']);
        }
        if (count($opts['ips']) > 0) {
            $taskRuleCreate->addSourceIpCondition($opts['ips']);
        }
        if (count($opts['queryStrings']) > 0) {
            $taskRuleCreate->addQueryStringCondition($opts['queryStrings']);
        }
        if (count($opts['headers']) > 0) {
            $taskRuleCreate->addHeaderCondition($opts['headers']);
        }
        $taskRuleCreate->run();

        $this->say('Rule deployed!');
    }

}
