<?php

namespace Bitbull\Cli\Commands\Cloud\AWS;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\AWS\AWSTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;

class SecurityGroup extends BaseCommand
{
    use UtilsTasks, AWSTasks;

    /**
     * Export security group
     *
     * @param $securityGroupId
     * @param $exportFile
     * @param $opts
     */
    function AwsSecurityGroupExport($securityGroupId, $exportFile = null, $opts = [
        'profile' => null,
        'region' => null,
        'csvSeparator' => ';',
    ])
    {
        $this->say("Retrieving SecurityGroup rules..");
        $infos = $this->taskSGDescribe($securityGroupId)
            ->profile($opts['profile'])
            ->region($opts['region'])
            ->run()
            ->getData();

        $tableHeader = ['type', 'protocol', 'from', 'to' , 'value', 'description'];
        $this->say("Ingress rules");
        $tableRawsIngress = $this->flatRules($infos['ingressRules']);
        $this->io()->table($tableHeader, $tableRawsIngress);
        $this->say("Egress rules");
        $tableRawsEgress = $this->flatRules($infos['egressRules']);
        $this->io()->table($tableHeader, $tableRawsEgress);

        if ($exportFile !== null){
            $this->say("Exporting into file $exportFile..");
            $taskExport = $this->taskWriteToFile($exportFile);
            foreach($tableRawsIngress  as $rows) {
                $taskExport->line(implode($opts['csvSeparator'], array_merge(['ingress'],$rows)));
            }
            foreach($tableRawsEgress as $rows) {
                $taskExport->line(implode($opts['csvSeparator'], array_merge(['egress'],$rows)));
            }
            $result = $taskExport->run();
            if($result->wasSuccessful()){
                $this->say("File exported");
            }
        }
        
    }

    /**
     * Import security group dump
     *
     * @param $securityGroupId
     * @param $exportedFile
     * @param $opts
     */
    function AwsSecurityGroupImport($securityGroupId, $exportedFile, $opts = [
        'profile' => null,
        'region' => null,
        'csvSeparator' => ';',
    ])
    {
        $this->say("Importing file..");
        $newRules = $this->loadCsvFile($exportedFile, $opts['csvSeparator']);

        $infos = $this->taskSGDescribe($securityGroupId)
            ->profile($opts['profile'])
            ->region($opts['region'])
            ->run()
            ->getData();
        $oldRulesIngress = $this->flatRules($infos['ingressRules']);
        $oldRulesEgress = $this->flatRules($infos['egressRules']);

        $this->say("Elaborating delta..");
        $editTask = $this->taskSGEdit($securityGroupId)
            ->profile($opts['profile'])
            ->region($opts['region']);
        foreach ($newRules as $newRule){
            $duplicated = false;
            $type = array_shift($newRule);
            if ($type == 'ingress'){
                $oldRules = $oldRulesIngress;
            }else{
                $oldRules = $oldRulesEgress;
            }
            foreach ($oldRules as $oldRule){
                // exclude comment diff check (last column)
                $diff = array_diff([
                    $oldRule[0],
                    $oldRule[1],
                    $oldRule[2],
                    $oldRule[3],
                    $oldRule[4],
                ], [
                    $newRule[0],
                    $newRule[1],
                    $newRule[2],
                    $newRule[3],
                    $newRule[4],
                ]);
                if(sizeof($diff) == 0){
                    $duplicated = true;
                    break;
                }
            }

            if($duplicated == false){
                if ($type == 'ingress'){
                    $editTask->addIngress([
                        'type' => $newRule[0],
                        'protocol' => $newRule[1],
                        'fromPort' => $newRule[2],
                        'toPort' => $newRule[3],
                        'value' => $newRule[4],
                        'description' => $newRule[5]
                    ]);
                }else{
                    $editTask->addEgress([
                        'type' => $newRule[0],
                        'protocol' => $newRule[1],
                        'fromPort' => $newRule[2],
                        'toPort' => $newRule[3],
                        'value' => $newRule[4],
                        'description' => $newRule[5]
                    ]);
                }
            }
        }
        $result = $editTask->run();
        if($result->wasSuccessful()){
            $this->say("File imported");
        }
    }

    /**
     * Flat rules
     *
     * @param $rules
     * @return array
     */
    private function flatRules($rules){
        $flatted = [];
        foreach ($rules as $rule) {
            array_push($flatted, [
                $rule['type'],
                $rule['protocol'],
                $rule['port']['from'],
                $rule['port']['to'],
                $rule['ipRanges'],
                $rule['description'],
            ]);
        }
        return $flatted;
    }

}
