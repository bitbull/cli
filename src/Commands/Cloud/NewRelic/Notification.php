<?php

namespace Bitbull\Cli\Commands\Cloud\NewRelic;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Git\GitTasks;
use Bitbull\Cli\Tasks\NewRelic\NewRelicTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;

class Notification extends BaseCommand
{
    use UtilsTasks, NewRelicTasks, GitTasks;

    /**
     * Notify new deployment on New Relic
     *
     * @param $appId
     * @param $revision
     * @param $opts
     */
    function NewrelicDeployment($appId, $revision, $opts = [
        'apiKey' => null,
        'changelog' => null,
        'description' => null,
        'user' => null,
    ])
    {
        if ($this->_exist('.git')){
            $this->say("Getting information from git message..");
            $result = $this->taskGitCommitInfo()->run();
            $commitInfos = $result->getData();

            if ($opts['description'] === null && isset($commitInfos['title'])) {
                $opts['description'] = $commitInfos['title'];
            }
            if ($opts['user'] === null && isset($commitInfos['authorName'])) {
                $opts['user'] = $commitInfos['authorName'];
            }
            if ($opts['changelog'] === null && isset($commitInfos['hash'])) {
                $opts['changelog'] = 'See commit '.$commitInfos['hash'].' for details';
            }
            $this->say("Retrieved description, user and changelog information");
        }

        $this->say("Creating deployment..");

        $result = $this->taskNewRelicCreateDeployment($appId)
            ->revision($revision)
            ->apiKey($opts['apiKey'])
            ->changelog($opts['changelog'])
            ->description($opts['description'])
            ->user($opts['user'])
            ->run();

        if ($result->wasSuccessful()) {
            $this->say("Deployment created");
        }
    }

}
