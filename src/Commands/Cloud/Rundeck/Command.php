<?php

namespace Bitbull\Cli\Commands\Cloud\Rundeck;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\AWS\AWSTasks;
use Bitbull\Cli\Tasks\Rundeck\RundeckTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;

class Command extends BaseCommand
{
    use UtilsTasks, RundeckTasks;

    /**
     * Execute Rundeck Job
     *
     * @param $jobId
     * @param $opts
     */
    function RundeckJobExecute($jobId, $opts = [
        'host' => 'http://localhost',
        'username' => null,
        'password' => null,
        'loglevel' => null,
        'user' => null,
        'options' => [],
    ])
    {
        $this->say("Executing Job with id $jobId..");

        $task = $this->taskRundeckGetSessionCookie($opts['host'])
            ->username($opts['username'])
            ->password($opts['password']);

        $result = $task->run();
        if($result->wasSuccessful()){
            $sessionCookie = $result->getData()['sessionCookie'];

            $task = $this->taskRundeckExecuteJob($opts['host'], $jobId)
                ->sessionCookie($sessionCookie);

            if($opts['loglevel'] !== null){
                $task->loglevel($opts['loglevel']);
            }
            if($opts['user'] !== null){
                $task->user($opts['user']);
            }else{
                $task->user($opts['username']);
            }

            if(sizeof($opts['options']) > 0){
                foreach ($opts['options'] as $option) {
                    $optionParts = explode(':', $option);
                    if (sizeof($optionParts) === 2 && $optionParts[1] !== '') {
                        $task->addOption($optionParts[0], $optionParts[1]);
                    } else {
                        $this->say("Warn: Incorrect option '$option', it should be 'key:value'");
                    }
                }
            }
            $result = $task->run();

            if($result->wasSuccessful()){
                $this->say("Job executed");
            }
        }
    }

}
