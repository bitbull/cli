<?php

namespace Bitbull\Cli\Commands\Config;

use Bitbull\Cli\Application\ApplicationFactory;
use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Magento2\Magento2Tasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;

class ConfigFileCommand extends BaseCommand
{
    use UtilsTasks;

    /**
     * Interpolate environment variable into file
     * FIXME: no error on non-existing input file
     * FIXME: no error on empty input file
     * FIXME: in both cases silently produces empty output
     *
     * @param $from
     * @param $to
     */
    function configEnvInterpolate($from, $to = null)
    {
        $this->say("Interpolating environment variables..");

        if($to == null){
            $to = $from;
        }

        $task = $this->taskWriteToFile($to)
            ->textFromFile($from);

        $vars = getenv();
        foreach ($vars as $key => $value) {
            $task->replace('$'.$key, $value);
            $task->replace('${'.$key.'}', $value);
        }

        $result = $task->run();
        if($result->wasSuccessful()){
            $this->say("Interpolation completed");
        }

    }

}
