<?php

namespace Bitbull\Cli\Commands\Test;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Test\TestTasks;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;

class CodeCheckCommand extends BaseCommand
{
    use TestTasks;

    /**
     * Check CodeStyle using grumphp
     *
     * @param $opts array
     */
    function checkGrumphp($opts = [
        'rules' => null,
    ])
    {
        $this->say("Checking application code quality..");

        $task = $this->taskGrumPHP();
        if($opts['rules'] !== null){
            $task->configFile($opts['rules']);
        }

        $result = $task->run();
        if($result->wasSuccessful()){
            $this->say("Application checks passed successfully");
        }
    }

    /**
     * Check CodeStyle using php cs
     *
     * @param $path
     * @param $opts array
     */
    function checkPhpcs($path = './',$opts = [
        'rules' => null,
        'warningSeverity' => null
    ])
    {
        $this->say("Checking application code quality..");

        $task = $this->taskPHPCodingStandards()
            ->path($path);
        if($opts['rules'] !== null){
            $task->configFile($opts['rules']);
        }
        if($opts['warningSeverity'] !== null){
            $task->warningSeverity($opts['warningSeverity']);
        }

        $result = $task->run();
        if($result->wasSuccessful()){
            $this->say("Application checks passed successfully");
        }
    }

    /**
     * Check CodeStyle using php md
     *
     * @param $path
     * @param $opts array
     */
    function checkPhpmd($path = './', $opts = [
        'rules' => null,
        'suffixes' => null,
    ])
    {
        $this->say("Checking application code quality..");

        $task = $this->taskPHPMessDetector()
            ->path($path);
        if($opts['rules'] !== null){
            $task->configFile($opts['rules']);
        }
        if($opts['suffixes'] !== null){
            $task->suffixes($opts['suffixes']);
        }

        $result = $task->run();
        if($result->wasSuccessful()){
            $this->say("Application checks passed successfully");
        }
    }

    /**
     * Check CodeStyle using php stan
     *
     * @param $path
     * @param $opts array
     */
    function checkPhpstan($path = './', $opts = [
        'level' => null,
        'suffixes' => null,
    ])
    {
        $this->say("Checking application code quality..");

        $task = $this->taskPHPStaticAnalysisToolTask()
            ->path($path);
        
        if($opts['level'] !== null){
            $task->level(intval($opts['level']));
        }

        $result = $task->run();
        if($result->wasSuccessful()){
            $this->say("Application checks passed successfully");
        }
    }

}
