<?php

namespace Bitbull\Cli\Commands\Test;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Test\TestTasks;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;

class TestCommand extends BaseCommand
{
    use TestTasks;

    /**
     * Test application using PHP Unit
     *
     * @param $paths
     * @param $opts array
     */
    function testPHPUnit($paths = '', $opts = [
        'rules' => null,
    ])
    {
        $this->say("Testing application..");

        if($paths == ''){
            $paths = 'test';
        }

        $task = $this->taskPHPUnit();
        foreach (explode(',', $paths) as $path){
            $task->dir($path);
        }
        if($opts['rules'] !== null){
            $task->configFile($opts['rules']);
        }

        $result = $task->run();
        if($result->wasSuccessful()){
            $this->say("Application tests passed successfully");
        }
    }

    /**
     * Test application using Mocha
     *
     * @param $files
     */
    function testMocha($files = 'test/**/*.js')
    {
        $this->say("Testing application..");

        $task = $this->taskMochaTest()
            ->files($files);

        $result = $task->run();
        if($result->wasSuccessful()){
            $this->say("Application tests passed successfully");
        }
    }

}
