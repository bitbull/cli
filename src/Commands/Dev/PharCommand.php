<?php

namespace Bitbull\Cli\Commands\Dev;

use Bitbull\Cli\Commands\BaseCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Finder\Finder;

class PharCommand extends BaseCommand
{
    /**
     * Build the phar executable
     *
     * @param $opts array
     * @return mixed
     */
    public function pharBuild($opts = [
        'executable' => InputOption::VALUE_REQUIRED,
        'dist' => InputOption::VALUE_REQUIRED,
        'ignoreVCS' => null,
        'exclude' => null,
    ])
    {
        $this->say("Building phar archive..");

        $executableFile = $opts['executable'];
        $distFile = $opts['dist'];

        $finder = Finder::create();
        $finder->files()
            ->name('*.php')
            ->name('*.twig');

        if($opts['ignoreVCS']){
            $finder->ignoreVCS(true);
        }

        if($opts['exclude']){
            if(is_array($opts['exclude'])){

                foreach ($opts['exclude'] as $path) {
                    $finder->notPath($path);
                }

            }else{
                $finder->notPath($opts['exclude']);
            }

        }

        $files = $finder->in($this->rootPath);

        $result = $this->taskPackPhar($distFile)
            ->addFiles($files)
            ->addFile($executableFile, $executableFile)
            ->executable($executableFile)
            ->taskFilesystemStack()
            ->run();

        if($result->wasSuccessful()){
            $this->say("Phar archive builded");
        }
    }

    /**
     * Allow phar creation
     *
     */
    public function pharEnableWrite()
    {
        $this->say("Enabling phar write..");

        $collection = $this->collectionBuilder();
        $collection->taskReplaceInFile(php_ini_loaded_file())
            ->from(';phar.readonly')
            ->to('phar.readonly');

        $collection->taskReplaceInFile(php_ini_loaded_file())
            ->from('phar.readonly = On')
            ->to('phar.readonly = Off');

        $result = $collection->run();

        if($result->wasSuccessful()){
            $this->say("Phar archive write allowed");
        }
    }

    /**
     * Deny phar creation
     *
     */
    public function pharDisableWrite()
    {
        $this->say("Disabling phar write..");

        $collection = $this->collectionBuilder();
        $collection->taskReplaceInFile(php_ini_loaded_file())
            ->from(';phar.readonly')
            ->to('phar.readonly');

        $collection->taskReplaceInFile(php_ini_loaded_file())
            ->from('phar.readonly = Off')
            ->to('phar.readonly = On')
            ->run();

        $result = $collection->run();

        if($result->wasSuccessful()){
            $this->say("Phar archive write denied");
        }
    }

}
