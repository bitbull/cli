<?php

namespace Bitbull\Cli\Commands\Dev;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Git\GitTasks;

class GitCommand extends BaseCommand
{
    use GitTasks;

    /**
     * Update Changelog
     *
     * @param $version
     * @param $changes
     */
    function gitUpdateChangelog($version, array $changes = [])
    {
        $this->say("Updating changelog..");

        if(sizeof($changes) > 0){
            $result = $this->taskChangelog()
                ->version($version)
                ->changes($changes)
                ->run();
        }else{
            $result = $this->taskChangelog()
                ->version($version)
                ->askForChanges()
                ->run();
        }

        if($result->wasSuccessful()){
            $this->say("Changelog updated");
        }
    }

    /**
     * Exec a command when commit title or body has a specific key
     *
     * @param $key
     * @param $execWhenFound
     * @param $execWhenNotFound
     */
    function gitSmartCommand($key, $execWhenFound, $execWhenNotFound = null)
    {
        $this->say("Checking current commit title and body..");

        $result = $this->taskGitCommitInfo()->run();
        $commitInfos = $result->getData();

        if(strpos($commitInfos['title'], $key) !== false || strpos($commitInfos['body'], $key) !== false){
            $this->say("Key '$key' found, running the command..");
            $this->_exec($execWhenFound);
        }else if ($execWhenNotFound !== null){
            $this->say("Key '$key' not found, running the command..");
            $this->_exec($execWhenNotFound);
        }else{
            $this->say("Key '$key' not found in commit title or body");
        }
    }

}
