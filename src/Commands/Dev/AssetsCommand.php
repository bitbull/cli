<?php

namespace Bitbull\Cli\Commands\Dev;

use Bitbull\Cli\Commands\BaseCommand;
use Lurker\Event\FilesystemEvent;
use Robo\Robo;

class AssetsCommand extends BaseCommand
{
    /**
     * Compile Less
     *
     * @param $src string
     * @param $dist string
     */
    function devLessCompile($src = "src/less/", $dist = "dist/less/")
    {
        $this->say("Compiling Scss..");

        $result = $this->taskLess([
                "$src" => $dist
            ])
            ->compiler('lessphp')
            ->run();

        if($result->wasSuccessful()){
            $this->say("Scss compiled");
        }
    }

    /**
     * Compile Scss
     *
     * @param $src string
     * @param $dist string
     */
    function devScssCompile($src = "src/scss/", $dist = "dist/css/")
    {
        $this->say("Compiling Scss..");

        $result = $this->taskScss([
                "$src" => $dist
            ])
            ->run();

        if($result->wasSuccessful()){
            $this->say("Scss compiled");
        }
    }

    /**
     * Minify Style
     *
     * @param $src string
     * @param $dist string
     */
    function devStyleMinify($src = "src/css/", $dist = "dist/css/")
    {
        $this->say("Compressing style..");

        $result = $this->taskMinify($src)
            ->to($dist)
            ->type('css')
            ->run();

        if($result->wasSuccessful()){
            $this->say("Style compressed");
        }
    }

    /**
     * Minify Javascript
     *
     * @param $src string
     * @param $dist string
     */
    function devJavascriptMinify($src = "src/js/", $dist = "dist/app.js")
    {
        $this->say("Compressing javascript..");

        $result = $this->taskMinify($src)
            ->to($dist)
            ->type('js')
            ->run();

        if($result->wasSuccessful()){
            $this->say("Javascript compressed");
        }
    }

    /**
     * Minify Images
     *
     * @param $src string
     * @param $dist string
     */
    function devImageMinify($src = "src/img/", $dist = "dist/img/")
    {
        $this->say("Compressing images..");

        $result = $this->taskImageMinify($src)
            ->to($dist)
            ->run();

        if($result->wasSuccessful()){
            $this->say("Images compressed");
        }
    }

    /**
     * Watch for file changes
     *
     * @param $src string
     */
    function devWatch($src = "src/")
    {
        $this->say("Watching for files changes..");

        $self = $this;
        $this->taskWatch()
            ->monitor($src, function(FilesystemEvent $event) use ($self) {
                $file = $event->getResource()->__toString();
                $self->say("File $file changed");
                switch (pathinfo($file, PATHINFO_EXTENSION)){
                    case "js":
                        $self->devJavascriptMinify(Robo::Config()->get('command.dev.javascript-minify.from'), Robo::Config()->get('command.dev.javascript-minify.to'));
                        break;
                    case "css":
                        $self->devStyleMinify(Robo::Config()->get('command.dev.style-minify.from'), Robo::Config()->get('command.dev.style-minify.to'));
                        break;
                    case "scss":
                        $self->devScssCompile(Robo::Config()->get('command.dev.scss-compile.from'), Robo::Config()->get('command.dev.scss-compile.to'));
                        break;
                    case "less":
                        $self->devLessCompile(Robo::Config()->get('command.dev.less-compile.from'), Robo::Config()->get('command.dev.less-compile.to'));
                        break;
                    case "png":
                    case "jpg":
                    case "jpeg":
                        $self->devImageMinify(Robo::Config()->get('command.dev.image-minify.from'), Robo::Config()->get('command.dev.image-minify.to'));
                        break;
                }
            })
            ->run();
    }

}
