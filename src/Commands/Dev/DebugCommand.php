<?php

namespace Bitbull\Cli\Commands\Dev;

use Bitbull\Cli\Commands\BaseCommand;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;

class DebugCommand extends BaseCommand
{
    /**
     * Dump database config
     *
     */
    function devDumpAppType()
    {
        $this->say($this->appType);
    }

    /**
     * Dump config
     *
     * @param $opts array
     */
    function devDumpConfig($opts = [
        'format' => null,
    ])
    {
        switch ($opts['format']) {
            case 'json':
                echo json_encode(Robo::config()->export()).PHP_EOL;
                break;
            case 'php':
                echo serialize(Robo::config()->export()).PHP_EOL;
                break;
            default:
                $this->say(print_r(Robo::config()->export(), true));
        }
    }

    /**
     * Dump database config
     *
     * @param $opts array
     */
    function devDumpDatabaseConfig($opts = [
        'format' => null,
    ])
    {
        $data = $this->app->getDatabaseConfigurations();

        switch ($opts['format']) {
            case 'json':
                echo json_encode($data).PHP_EOL;
                break;
            case 'php':
                echo serialize($data).PHP_EOL;
                break;
            default:
                $this->say(print_r($data, true));
        }
    }

    /**
     * Get current working directory
     */
    function devDumpCWD()
    {
        $this->say($this->rootPath);
    }

    /**
     * Dump PHP infos
     *
     * @param $opts array
     */
    function devDumpPHP($opts = [
        'type' => null,
    ])
    {
        switch ($opts['type']) {
            case 'version':
                $this->say('Current PHP version: ' . phpversion());
                break;
            case 'general':
                phpinfo(INFO_GENERAL);
                break;
            case 'credits':
                phpinfo(INFO_CREDITS);
                break;
            case 'configuration':
                phpinfo(INFO_CONFIGURATION);
                break;
            case 'modules':
                phpinfo(INFO_MODULES);
                break;
            case 'environment':
                phpinfo(INFO_ENVIRONMENT);
                break;
            case 'variables':
                phpinfo(INFO_VARIABLES);
                break;
            case 'license':
                phpinfo(INFO_LICENSE);
                break;
            default:
                phpinfo(INFO_ALL);
        }
    }

}
