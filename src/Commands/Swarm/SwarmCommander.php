<?php
/**
 * @author   Fabio Gollinucci <fabio.gollinucci@bitbull.it>
 */
namespace Bitbull\Cli\Commands\Swarm;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\AWS\AWSTasks;
use Bitbull\Cli\Tasks\SwarmCommander\SwarmCommanderTasks;

class SwarmCommander extends BaseCommand
{
    use SwarmCommanderTasks, AWSTasks;

    /**
     * Execute command inside container
     *
     * @param $service
     * @param $commandToExec
     * @param $opts array
     */
    function swarmExecuteCommand($service, $commandToExec, $opts = [
        'endpoint' => null,
        'apiKey' => null,
        'wait' => false,
        'profile' => null,
        'region' => null,
    ])
    {
        if ($opts['endpoint'] === null) {
            $this->taskEmptyError('Endpoint parameters is required')->run();
            return;
        }

        $this->say("Execute command on service $service using SwarmCommander..");

        $task = $this->taskSwarmCommanderExecuteCommand($opts['endpoint'], $opts['apiKey'])
            ->service($service)
            ->command($commandToExec);

        $result = $task->run();

        if ($opts['wait'] === true) {
            $commandId = $result->getData()['ssmCommandId'];
            $this->say("Waiting for command '$commandId' to be finished..");
            $result = $this->taskSSMDescribeCommand($commandId)
                ->profile($opts['profile'])
                ->region($opts['region'])
                ->wait(true)
                ->run();

            $invocations = $result->getData()['invocations'];
            foreach ($invocations as $invocation) {
                $this->say("Command on instance '".$invocation['InstanceId']."' exited with status '".$invocation['Status']."'");
                if ($invocation['Status'] === 'Failed') {
                    $this->taskEmptyError($invocation['CommandPlugins'][0]['Output'])->run();
                    return;
                }
                $this->io()->block($invocation['CommandPlugins'][0]['Output']);
            }
        }

        if($result->wasSuccessful()){
            $this->say("Command executed for service $service triggered");
        }
    }
}