<?php

namespace Bitbull\Cli\Commands\Swarm;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Docker\DockerTasks;
use Bitbull\Cli\Tasks\Portainer\PortainerTasks;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;

class DockerRegistry extends BaseCommand
{
    use PortainerTasks, DockerTasks;

    /**
     * Get Docker Hub image tags
     *
     * @param $image
     * @param $opts array
     */
    function dockerhubImageTags($image, $opts = [
        'username' => null,
        'password' => null,
    ])
    {
        $this->say('Retrieving image tags from Docker Hub..');

        $task = $this->taskDockerHubGetAuthToken();

        if($opts['username'] !== null){
            $task->username($opts['username']);
        }
        if($opts['password'] !== null){
            $task->password($opts['password']);
        }

        $result = $task->run();
        if ($result->wasSuccessful()) {
            $token = $result->getData()['token'];

            $result = $this->taskDockerHubImageTags($image)
                ->token($token)
                ->run();

            $tags = $result->getData()['tags'];

            $tableHeader = ['name', 'size' ,'last update'];
            $tableRaws = [];
            foreach ($tags as $tag) {
                array_push($tableRaws, [
                    $tag->name,
                    $tag->full_size,
                    $tag->last_updated,
                ]);
            }
            $this->io()->table($tableHeader, $tableRaws);
        }
    }
}