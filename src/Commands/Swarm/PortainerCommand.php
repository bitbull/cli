<?php
/**
 * @author   Gennaro Vietri <gennaro.vietri@bitbull.it>
 */
namespace Bitbull\Cli\Commands\Swarm;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Docker\DockerTasks;
use Bitbull\Cli\Tasks\Portainer\PortainerTasks;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;

class PortainerCommand extends BaseCommand
{
    use PortainerTasks, DockerTasks;

    /**
     * Deploy application to portainer
     *
     * @param $stack
     * @param $opts array
     */
    function portainerStackUpdate($stack, $opts = [
        'host' => 'http://localhost',
        'username' => null,
        'password' => null,
        'swarmId' => null,
        'dockerComposeFile' => null,
        'envFile' => null,
    ])
    {
        $this->say('Updating Docker stack using portainer..');

        $task = $this->taskPortainerGetAuthToken($opts['host']);

        if($opts['username'] !== null){
            $task->username($opts['username']);
        }
        if($opts['password'] !== null){
            $task->password($opts['password']);
        }

        $result = $task->run();
        if($result->wasSuccessful()){
            $token = $result->getData()['token'];

            $task = $this->taskPortainerCreateOrUpdateStack($opts['host'])
                ->stackName($stack)
                ->token($token);

            if($opts['swarmId'] !== null){
                $task->swarmId($opts['swarmId']);
            }
            if($opts['dockerComposeFile'] !== null){
                $task->dockerComposeFile($opts['dockerComposeFile']);
            }
            if($opts['envFile'] !== null){
                $task->envFile($opts['envFile']);
            }

            $result = $task->run();

            if($result->wasSuccessful()){
                $this->say("Deploy for stack $stack triggered");
            }
        }

    }

    /**
     * Delete portainer stack
     *
     * @param $stack
     * @param $opts array
     */
    function portainerStackDelete($stack, $opts = [
        'host' => 'http://localhost',
        'username' => null,
        'password' => null,
        'swarmId' => null,
    ])
    {
        $this->say('Deleting stack from portainer..');

        $task = $this->taskPortainerGetAuthToken($opts['host']);

        if($opts['username'] !== null){
            $task->username($opts['username']);
        }
        if($opts['password'] !== null){
            $task->password($opts['password']);
        }

        $result = $task->run();
        if($result->wasSuccessful()){
            $token = $result->getData()['token'];

            $task = $this->taskPortainerDeleteStack($opts['host'])
                ->stackName($stack)
                ->token($token);

            if($opts['swarmId'] !== null){
                $task->swarmId($opts['swarmId']);
            }

            $result = $task->run();

            if($result->wasSuccessful()){
                $this->say("Stack $stack deleted");
            }
        }

    }

    /**
     * Deploy application to portainer
     *
     * @param $service
     * @param $image
     * @param $opts array
     */
    function portainerServiceUpdate($service, $image = null, $opts = [
        'host' => 'http://localhost',
        'username' => null,
        'password' => null,
        'force' => false
    ])
    {
        $this->say('Updating Docker service using portainer..');

        $task = $this->taskPortainerGetAuthToken($opts['host']);

        if($opts['username'] !== null){
            $task->username($opts['username']);
        }
        if($opts['password'] !== null){
            $task->password($opts['password']);
        }

        if ($image !== null){
            if(strpos($image, ':') === false){
                $this->say("Elaborating tag..");
                $tag = $this->elaborateDockerTag();
                $this->say("Using tag $tag");
                $image .= ':'.$tag;
            }
        }

        $result = $task->run();
        if ($result->wasSuccessful()) {
            $token = $result->getData()['token'];

            $task = $this->taskPortainerUpdateService($opts['host'])
                ->serviceName($service)
                ->token($token)
                ->force($opts['force']);
            if ($image !== null) {
                $task->image($image);
            }

            $result = $task->run();

            if($result->wasSuccessful()){
                $this->say("Deploy for service $service triggered");
            }
        }
    }

    /**
     * Execute command inside container
     *
     * @param $service
     * @param $commandToExec
     * @param $opts array
     */
    function portainerExecuteCommand($service, $commandToExec, $opts = [
        'host' => 'http://localhost',
        'username' => null,
        'password' => null
    ])
    {
        $this->say("Execute command on service $service using portainer..");

        $task = $this->taskPortainerGetAuthToken($opts['host']);

        if($opts['username'] !== null){
            $task->username($opts['username']);
        }
        if($opts['password'] !== null){
            $task->password($opts['password']);
        }

        $result = $task->run();
        if ($result->wasSuccessful()) {
            $token = $result->getData()['token'];

            $task = $this->taskPortainerCommandExec($opts['host'])
                ->token($token)
                ->service($service)
                ->waitUntilComplete(true)
                ->command($commandToExec);

            $result = $task->run();

            if($result->wasSuccessful()){
                $this->say("Command executed for service $service triggered");
            }
        }
    }
}