<?php

namespace Bitbull\Cli\Commands\CLI;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Runner;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Robo;
use Symfony\Component\Yaml\Yaml;

class ConfigurationCommand extends BaseCommand
{
    use UtilsTasks;

    /**
     * Generate stub configuration file
     *
     */
    function selfConfigGenerate()
    {
        $this->say("Generating stub configuration file..");

        $config = [
            'app' => $this->app->getDefaultConfigData()
        ];

        $action = 'override';
        if ($this->_exist(Runner::CONFIG_FILE_NAME)) {
            $action = $this->askChoice("File ".Runner::CONFIG_FILE_NAME." already exist, what action you want to do?", [
                'override' => 'Replace config file content',
                'merge' => 'Merge file with default configuration',
                'exit' => 'Exit now, do not touch the file',
            ], $action);

            if ($action == 'exit') {
                return;
            }
        }

        if ($action == 'merge') {
            $config = array_merge($config, Robo::config()->export());
        }

        $result = $this->taskWriteToFile(Runner::CONFIG_FILE_NAME)
            ->text(Yaml::dump($config, 10))
            ->run();

        if($result->wasSuccessful()){
            $this->say("Stub configuration file generated");
        }
    }

}
