<?php

namespace Bitbull\Cli\Commands\CLI;

use Bitbull\Cli\Application\ApplicationFactory;
use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Magento2\Magento2Tasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;

class InstallationCommand extends BaseCommand
{
    use UtilsTasks;

    const REMOTE_PATH_STABLE = 'https://cli.bitbull.io/bb-cli.phar';
    const REMOTE_PATH_DEV = 'https://cli.bitbull.io/dev/bb-cli.phar';
    const EXECUTABLE_PATH = '/usr/local/bin/bb-cli';

    /**
     * Install CLI
     *
     * @param $opts array
     */
    function selfInstall($opts = [
        'forceRemote' => false,
        'dev' => false
    ])
    {
        $this->say("Installing CLI..");
        $collection = $this->collectionBuilder();

        if($this->_exist('dist/bb-cli.phar') && !$opts['forceRemote']){
            $collection->taskFilesystemStack()->copy('dist/bb-cli.phar', self::EXECUTABLE_PATH);
        }else{

            $collection->taskDownload();

            if($opts['dev']){
                $collection->from(self::REMOTE_PATH_DEV);
            }else{
                $collection->from(self::REMOTE_PATH_STABLE);
            }

            $collection->to(self::EXECUTABLE_PATH);
        }

        $collection->taskFilesystemStack()->chmod(self::EXECUTABLE_PATH, 0755);

        $result = $collection->run();
        if($result->wasSuccessful()){
            $this->say("CLI installed");
        }
    }

    /**
     * Remove CLI
     *
     */
    function selfRemove()
    {
        $this->say("Removing CLI..");

        if(!$this->_exist(self::EXECUTABLE_PATH)){
            $this->say("CLI not installed");
        }else{
            $result = $this->taskFilesystemStack()
                ->remove(self::EXECUTABLE_PATH)
                ->run();
            if($result->wasSuccessful()){
                $this->say("CLI removed");
            }
        }

    }

}
