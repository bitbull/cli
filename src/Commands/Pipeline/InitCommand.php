<?php

namespace Bitbull\Cli\Commands\Pipeline;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\TemplateFile\TemplateFileTasks;
use Robo\Robo;

class InitCommand extends BaseCommand
{
    use TemplateFileTasks;

    /**
     * Init Bitbucket pipeline
     *
     * @param $envs array
     */
    function pipelineInitBitbucket($envs = 'staging,production')
    {
        $this->say('Init Bitbucket pipeline..');

        $config = $this->getPipelineConfig();
        $config['envs'] = explode(',', $envs);

        $result = $this->taskTemplateFileRender()
            ->template('Pipeline/bitbucket-pipelines.yml.twig')
            ->params($config)
            ->destination('bitbucket-pipelines.yml')
            ->run();

        if($result->wasSuccessful()){
            $this->say('Bitbucket pipeline initialized');
        }
    }

    /**
     * Get pipeline configuration based on configs
     */
    private function getPipelineConfig()
    {
        $data = [
            'docker' => false,
            'dockerComposeUI' => false,
            'codeDeploy' => false,
            'sftp' => false,
            'remoteSsh' => false,
        ];

        if(Robo::config()->get('command.pipeline.deployDocker.options', false)){
            $data['docker'] = true;
        }
        if(Robo::config()->get('command.pipeline.deployDockercomposeui.options', false)){
            $data['dockerComposeUI'] = true;
        }
        if(Robo::config()->get('command.pipeline.deployCodedeploy.options', false)){
            $data['codeDeploy'] = true;
        }
        if(Robo::config()->get('command.pipeline.deploySftp.options', false)){
            $data['sftp'] = true;
        }
        if(Robo::config()->get('command.pipeline.deployRemoteSsh.options', false)){
            $data['remoteSsh'] = true;
        }

        return $data;
    }

}
