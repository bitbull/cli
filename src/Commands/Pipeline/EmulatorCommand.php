<?php

namespace Bitbull\Cli\Commands\Pipeline;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;

class EmulatorCommand extends BaseCommand
{
    use UtilsTasks;

    /**
     * Emulate bitbucket pipeline using Docker
     *
     * @param $pipeline
     * @param $opts
     */
    function emulateBitbucket($pipeline, $opts = [
        'envFile' => null,
        'keepTmpDirectory' => false,
        'keyFile' => null
    ])
    {
        $this->say("Emulating pipeline..");

        if(!$this->_exist('bitbucket-pipelines.yml')){
            $this->say("<error>File $this->rootPath/bitbucket-pipelines.yml not found </error>");
            return;
        }

        $pipelineConfig = $this->loadYamlFile('bitbucket-pipelines.yml');

        $pipelines = array_merge(
            $pipelineConfig['pipelines']['custom'] ? $pipelineConfig['pipelines']['custom'] : [],
            $pipelineConfig['pipelines']['branches'] ? $pipelineConfig['pipelines']['branches'] : [],
            $pipelineConfig['pipelines']['tags'] ? $pipelineConfig['pipelines']['tags'] : []
        );
        if(!isset($pipelines[$pipeline])){
            $this->say("<error>Pipeline $pipeline not found</error>");
            $this->say("Available pipelines: ");
            foreach ($pipelines as $pipeline => $value){
                $this->say("    - $pipeline");
            }
            return;
        }

        $tmpPath = realpath($this->_tmpDir());
        $this->_mirrorDir($this->rootPath, $tmpPath);

        $steps = $pipelines[$pipeline];
        $result = $this->taskEmptySuccess()->run();

        foreach ($steps as $key => $step) {

            $stepData = $step['step'];

            if($pipelineConfig['image']){
                $image = $pipelineConfig['image'];
            }else{
                $image = $stepData['image'];
            }

            $stepName = $key;
            if(isset($stepData['name'])){
                $stepName = $stepData['name'];
            }

            $this->say('Executing step '.$stepName);

            // Adding the ssh host key for the most common hosts
            // that is, Bitbucket and Github
            $hostsKeysVerification = [
                "mkdir -p /root/.ssh && touch /root/.ssh/known_hosts",
                "/usr/bin/ssh-keyscan -t rsa bitbucket.org >> /root/.ssh/known_hosts",
                "/usr/bin/ssh-keyscan -t rsa github.com >> /root/.ssh/known_hosts"
            ];

            $tmpScript = implode("\n", $hostsKeysVerification) . "\n" . implode("\n", $stepData['script']);

            $this->taskWriteToFile($tmpPath . '/scripts.sh')
                ->text($tmpScript)
                ->run();
            $this->_chmod($tmpPath . '/scripts.sh', 0755);

            $envVars = [];
            if($opts['envFile'] != null){
                $envVars = $this->loadEnvFile($opts['envFile']);
            }
            
            $task = $this->taskDockerRun($image)
                ->volume($tmpPath, '/pipeline')
                ->containerWorkdir('/pipeline')
                ->exec('/bin/sh -c "sleep 600"')
                ->envVars($envVars)
                ->detached();

            if ($opts['keyFile'] != null) {
                $task->volume($opts['keyFile'], '/root/.ssh/id_rsa');
            }

            $container = $task->run();

            $result = $this->taskDockerExec($container['cid'])
                ->exec('/bin/bash -c /pipeline/scripts.sh')
                ->interactive()
                ->run();

            if(!$result->wasSuccessful()){
                break;
            }

            $result = $this->taskDockerRemove($container['cid'])
                ->silent(true)
                ->run();

            if(!$result->wasSuccessful()){
                break;
            }
        }

        $this->_remove($tmpPath);

        if($result->wasSuccessful()){
            $this->say("Pipeline executed successfully");
        }
    }
}
