<?php

namespace Bitbull\Cli\Commands\Pipeline;

use Bitbull\Cli\Commands\BaseCommand;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;

class ReleaseCommand extends BaseCommand
{
    /**
     * Install application
     *
     * @param $opts array
     */
    function releaseInstall($opts = [
        'cronFile' => null,
        'remoteHost' => null,
        'remoteUsername' => null,
        'remoteKey' => null
    ])
    {
        $this->say('Installing release..');
        $collection = $this->collectionBuilder();

        if($opts['cronFile'] !== null){
            $destination = '/etc/cron.d/application';
            $collection->taskFilesystemStack()
                ->rename($opts['cronFile'], $destination)
                ->chmod($destination, 0644)
                ->chgrp($destination, 'root')
                ->chown($destination, 'root');
        }

        if($opts['remoteHost'] !== null && $opts['remoteUsername'] !== null && $opts['remoteKey'] !== null){
            $result = $this->taskSshExec($opts['remoteHost'], $opts['remoteUsername'])
                ->identityFile($opts['remoteKey'])
                ->exec($collection);
        }else{
            $result = $collection->run();
        }

        if($result->wasSuccessful()){
            $this->say('Release installed');
        }
    }

    /**
     * Activated release
     *
     * @param $identifier
     * @param $opts array
     */
    function releaseActivate($identifier = null, $opts = [
        'opcache' => false,
        'opcacheFilePrefix' => '',  // needed for providing basic-auth exclusion
        'opcacheURLInvalidate' => null,
        'infoFile' => null,
        'links' => [],
        'permission' => null,
        'remoteHost' => null,
        'remoteUsername' => null,
        'remoteKey' => null,
        'current' => '.'
    ])
    {
        $this->say('Activating release..');

        $collection = $this->collectionBuilder();

        foreach ($opts['links'] as $link){
            $linksPart = explode(':', $link);
            if(!isset($linksPart[0]) || !isset($linksPart[1])){
                continue;
            }
            $from = $linksPart[0];
            $to = $linksPart[1];
            if($from[0] !== '/'){
                $from = $this->rootPath.'/'.$from;
            }
            $collection->taskFilesystemStack()->remove($from);
            if(pathinfo($to, PATHINFO_EXTENSION) == null){
                $collection->mkdir($to);
            }else{
                $collection->mkdir(pathinfo($to, PATHINFO_DIRNAME));
                $collection->touch($to);
            }
            $collection->symlink($to, $from);
            if($opts['permission'] !== null){
                $parts = explode(':', $opts['permission']);
                $collection->chown($to, $parts[0], true);
                if(isset($parts[1])){
                    $collection->chgrp($to, $parts[1], true);
                }
            }
        }

        if($opts['infoFile'] !== null){
            if($identifier == null){
                $identifier = $this->elaborateBuildIdentifier();
            }
            $collection->taskWriteToFile($opts['infoFile'])->line($identifier);
        }

        if($opts['permission'] !== null){
            $parts = explode(':', $opts['permission']);
            $collection->taskFilesystemStack()->chown($this->rootPath, $parts[0], true);
            if(isset($parts[1])){
                $collection->chgrp($this->rootPath, $parts[1], true);
            }
        }

        if($opts['opcache'] === true){
            $this->say('Cleaning opcache..');

            if($opts['opcacheURLInvalidate'] !== null){
                $collection->taskHttpRequest($opts['opcacheURLInvalidate']);
            }else{
                if (isset($opts['opcacheFilePrefix'])) {
                    $fileName = $opts['opcacheFilePrefix'] . $this->strRandom(5, 'char|number').'.php';
                }
                else {
                    $fileName = $this->strRandom(5, 'char|number').'.php';
                }
                $filePath = $this->app->getPublicPath().'/'.$fileName;
                $collection->taskWriteToFile($filePath)
                    ->line('<?php opcache_reset(); ?>');
                $collection->taskHttpRequest($this->app->getDomainName().'/'.$fileName);
                $collection->taskFilesystemStack()
                    ->remove($filePath);
            }

        }

        if($opts['remoteHost'] !== null && $opts['remoteUsername'] !== null && $opts['remoteKey'] !== null){
            $result = $this->taskSshExec($opts['remoteHost'], $opts['remoteUsername'])
                ->identityFile($opts['remoteKey'])
                ->exec($collection);
        }else{
            $result = $collection->run();
        }

        if($result->wasSuccessful()){
            $this->say('Release activated');
        }
    }

    /**
     * Enable current release using blue-green
     *
     * @param $symlink
     * @param $path
     * @param $opts array
     */
    function releaseBlueGreen($symlink = null, $path = null, $opts = [
        'ondeck' => null,
        'previous' => null,
        'current' => null,
        'remoteHost' => null,
        'remoteUsername' => null,
        'remoteKey' => null
    ])
    {

        $this->say('Enabling release..');

        /* @var \Robo\Task\Filesystem\FilesystemStack $fsStack */
        $fsStack = $this->collectionBuilder()->taskFilesystemStack();

        if ($this->_exist($opts['previous'])) {
            $fsStack->remove($opts['previous']);
        }
        if ($this->_exist($opts['current'])) {
            $fsStack->rename($opts['current'], $opts['previous']);
        }

        $fsStack
            ->rename($opts['ondeck'], $opts['current'])
            ->remove($symlink)
            ->symlink($opts['current'] . ($path ?: ''), $symlink);

        if ($opts['remoteHost'] !== null && $opts['remoteUsername'] !== null && $opts['remoteKey'] !== null) {
            $result = $this->taskSshExec($opts['remoteHost'], $opts['remoteUsername'])
                ->identityFile($opts['remoteKey'])
                ->exec($fsStack);
        } else {
            $result = $fsStack->run();
        }

        if ($result->wasSuccessful()) {
            $this->say('Release enabled');
        }
    }
}
