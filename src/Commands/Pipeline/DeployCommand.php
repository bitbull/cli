<?php

namespace Bitbull\Cli\Commands\Pipeline;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\AWS\AWSTasks;
use Bitbull\Cli\Tasks\Docker\DockerTasks;
use Bitbull\Cli\Tasks\Portainer\PortainerTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Robo;
use Symfony\Component\Console\Input\InputOption;

class DeployCommand extends BaseCommand
{
    use DockerTasks, AWSTasks, PortainerTasks, UtilsTasks;

    /**
     * Get deploy identifier
     *
     * @param $default
     * @param $opts
     */
    function deployIdentifier($default = 'latest', $opts = [
        'prefix' => '',
        'suffix' => '',
        'removeWords' => [],
        'limit' => 128,
        'limitFromStart' => false,
        'limitFromEnd' => false,
        'replacementChar' => '-',
        'noNewLine' => false,
        'forceLowercase' => false,
        'forceUppercase' => false,
    ])
    {
        $identifier = $this->elaborateNiceBuildIdentifier($default, $opts['replacementChar']);
        $identifier = $opts['prefix'].$identifier.$opts['suffix'];

        if ($opts['noNewLine'] === false) {
            $identifier .= PHP_EOL;
        }

        if (sizeof($opts['removeWords']) > 0) {
            foreach ($opts['removeWords'] as $word) {
                $identifier = str_replace($word, '', $identifier);
            }
        }

        if (strlen($identifier) > intval($opts['limit'])) {
            if ($opts['limitFromStart']) {
                $identifier = substr($identifier, 0, intval($opts['limit']));
            } else if ($opts['limitFromEnd']) {
                $identifier = substr($identifier, strlen($identifier) - intval($opts['limit']));
            } else {
                $identifier = substr($identifier, 0, intval($opts['limit']));
            }
        }

        if ($opts['forceLowercase'] == true) {
            $identifier = strtolower($identifier);
        }

        if ($opts['forceUppercase'] == true) {
            $identifier = strtoupper($identifier);
        }

        echo $opts['prefix'].$identifier.$opts['suffix'];
    }

    /**
     * Deploy application using AWS CodeDeploy
     *
     * @param $identifier
     * @param $opts array
     */
    function deployCodedeploy($identifier = null, $opts = [
        'releaseBucket' => null,
        'applicationName' => null,
        'deploymentGroup' => null,
        'region' => null,
        'profile' => null,
        'dryrun' => false,
    ])
    {
        if($identifier == null){
            $identifier = $this->elaborateNiceBuildIdentifier();
        }

        if(!isset($opts['region']) || $opts['region'] == null){
            $opts['region'] = getenv('AWS_DEFAULT_REGION');
        }

        $this->say('Deploying to CodeDeploy..');
        $collection = $this->collectionBuilder();
        $work = $collection->tmpDir();

        $artifact = 'release-'.$identifier.'.zip';
        $collection->taskZipDirectory($this->rootPath)
            ->to($work.'/'.$artifact);
        $collection->taskS3UploadFile()
            ->region($opts['region'])
            ->profile($opts['profile'])
            ->from($work.'/'.$artifact)
            ->to('s3://'.$opts['releaseBucket'].'/'.$artifact);

        if($opts['dryrun'] == false){
            $collection->taskCodeDeploy()
                ->region($opts['region'])
                ->profile($opts['profile'])
                ->applicationName($opts['applicationName'])
                ->identifier($identifier)
                ->deploymentGroup($opts['deploymentGroup'])
                ->releaseBucket($opts['releaseBucket'])
                ->artifact($artifact);
        }

        $result = $collection->run();

        if($result->wasSuccessful()){
            $this->say('Deploy executed');
        }

    }

    /**
     * Deploy application using SFTP
     *
     * @param $opts array
     */
    function deploySftp($opts = [
        'host' => null,
        'username' => null,
        'removePath' => null,
        'key' => null
    ])
    {
        $this->say('Uploading to server..');

        $result = $this->taskRsync()
            ->fromPath($this->rootPath)
            ->toHost($opts['host'])
            ->toUser($opts['username'])
            ->toPath($opts['removePath'])
            ->remoteShell('ssh -i '.$opts['key'])
            ->recursive()
            ->excludeVcs()
            ->checksum()
            ->wholeFile()
            ->verbose()
            ->progress()
            ->humanReadable()
            ->stats()
            ->run();

        if($result->wasSuccessful()){
            $this->say('All file uploaded');
        }
    }

    /**
     * Deploy application to Docker repository
     *
     * @param $image
     * @param $tag
     * @param $opts
     */
    function deployDocker($image, $tag = null, $opts = [
        'username' => null,
        'password' => null,
        'repository' => null,
        'memoryLimit' => null,
        'buildArgs' => [],
    ])
    {
        if($opts['username'] !== null && $opts['password'] !== null ){
            $this->say('Login to docker repository..');
            $result = $this->taskDockerLogin($opts['repository'])
                ->username($opts['username'])
                ->password($opts['password'])
                ->run();
            if($result->wasSuccessful()){
                $this->say('Login successful');
            }
        }

        if($tag === null){
            $this->say("Elaborating tag..");
            $tag = $this->elaborateDockerTag();
            $this->say("Using tag $tag");
        }

        $imageName = $image.':'.$tag;
        if ($opts['repository'] !== null) {
            $repository = $opts['repository'];
            $this->say("Using repository $repository..");
            $imageName = $repository.'/'.$imageName;
        }

        $this->say("Building Docker image $imageName..");

        $task = $this->taskDockerBuild()
            ->tag($imageName);

        if ($opts['memoryLimit'] !== null) {
            $task->option('--memory', $opts['memoryLimit']);
        }

        foreach ($opts['buildArgs'] as $buildArg) {
            $task->option('--build-arg', $buildArg);
        }

        $result = $task->run();

        if($result->wasSuccessful()){
            $this->say('Docker image successfully builded');
        }

        $this->say("Push Docker image $imageName..");
        $result =$this->taskDockerPush($imageName)
            ->run();

        if($result->wasSuccessful()){
            $this->say('Docker image successfully pushed');
        }
    }

    /**
     * Deploy application using Docker Compose UI
     *
     * @param $opts array
     */
    function deployDockercomposeui($opts = [
        'image' => null,
        'projectName' => null,
        'dockerComposeFile' => null,
        'host' => null,
        'username' => null,
        'password' => null,
    ])
    {
        $opts = array_merge([
                'dockerComposeFile' => 'docker-compose.yml'
            ],
            $opts
        );

        $this->say('Deploying to Docker Compose UI..');

        $result = $this->taskDockerComposeUI()
            ->image($opts['image'])
            ->projectName($opts['projectName'])
            ->dockerComposeFile($opts['image'])
            ->host($opts['host'])
            ->username($opts['username'])
            ->password($opts['password'])
            ->run();

        if($result->wasSuccessful()){
            $this->say('Deploy executed');
        }
    }

    /**
     * Deploy application to a remote server with SSH
     *
     * @param $identifier
     * @param $opts array
     */
    function deployRemoteSsh($identifier = null, $opts = [
        'host' => null,
        'username' => null,
        'key' => null,
        'remotePath' => null
    ])
    {
        $this->say('Deploying to remote server..');

        $collection = $this->collectionBuilder();
        $work = $collection->tmpDir();

        if($identifier == null){
            $identifier = $this->elaborateBuildIdentifier();
        }
        $artifact = 'release-'.$identifier.'.zip';

        $collection->taskPack($work.'/'.$artifact)
            ->add('.');

        $collection->taskScpUpload()
            ->from($work.'/'.$artifact)
            ->to('/tmp/'.$artifact)
            ->host($opts['host'])
            ->username($opts['username'])
            ->key($opts['key'])
            ->run();

        $collection->taskSshExec($opts['host'], $opts['username'])
            ->identityFile($opts['key'])
            ->exec(
                $this->taskExtract('/tmp/'.$artifact)
                     ->to($opts['remotePath'])
            );

        $result = $collection->run();

        if($result->wasSuccessful()){
            $this->say('Deploy executed');
        }
    }

    /**
     * Deploy static website to S3
     *
     * @param $localPath
     * @param $remotePath
     * @param $opts array
     */
    function deployS3bucket($remotePath, $localPath = null, $opts = [
        'region' => null,
        'profile' => null,
        'public' => false
    ])
    {
        $this->say('Deploying to S3 bucket..');

        if ($localPath == null) {
            $localPath = $this->rootPath;
        }

        if (is_dir($localPath)) {
            $task = $this->taskS3UploadDirectory();
        } else {
            $task = $this->taskS3UploadFile();
        }

        $result = $task->from($localPath)
            ->to($remotePath)
            ->public($opts['public'])
            ->region($opts['region'])
            ->profile($opts['profile'])
            ->run();

        if($result->wasSuccessful()){
            $this->say('Deploy executed');
        }
    }

}
