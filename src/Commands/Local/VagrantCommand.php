<?php

namespace Bitbull\Cli\Commands\Local;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Vagrant\VagrantTasks;
use Robo\Robo;

class VagrantCommand extends BaseCommand
{
    use VagrantTasks;

    /**
     * Create Vagrant file
     *
     */
    function localVagrantInit()
    {
        $this->say("Initialising Vagrant environment..");

        $result = $this->taskTemplateFileRender()
            ->template('Vagrant/vagrantfile.twig')
            ->params($this->app->getAppConfig())
            ->destination('Vagrantfile')
            ->run();

        if($result->wasSuccessful()){
            $this->say("Vagrant env initialized");
        }
    }

    /**
     * Create local Vagrant environment
     */
    function localVagrantUp()
    {
        $this->say("Creating Vagrant environment..");
        $result = $this->taskVagrantUp()->run();

        if($result->wasSuccessful()){
            $this->say("Vagrant environment created");
        }
    }

    /**
     * Destroy local Vagrant environment
     */
    function localVagrantDown()
    {
        $this->say("Destroying Vagrant environment..");
        $result = $this->taskVagrantDown()->run();

        if($result->wasSuccessful()){
            $this->say("Vagrant environment destroyed");
        }
    }

    /**
     * Start local Vagrant environment
     */
    function localVagrantStart()
    {
        $this->say("Starting Vagrant environment..");
        $result = $this->taskVagrantUp()->run();

        if($result->wasSuccessful()){
            $this->say("Vagrant environment started");
        }
    }

    /**
     * Stop local Vagrant environment
     */
    function localVagrantStop()
    {
        $this->say("Stopping Vagrant environment..");
        $result = $this->taskVagrantStop()->run();

        if($result->wasSuccessful()){
            $this->say("Vagrant environment stopped");
        }
    }

    /**
     * Connect to Vagrant using SSH
     */
    function localVagrantConnect()
    {
        $this->say("Connecting to Vagrant environment..");
        $this->taskVagrantConnect()
            ->run();
    }
}
