<?php

namespace Bitbull\Cli\Commands\Local;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Docker\DockerTasks;
use Bitbull\Cli\Tasks\TemplateFile\TemplateFileTasks;
use Robo\Result;
use Robo\Robo;

class DockerCommand extends BaseCommand
{
    use TemplateFileTasks, DockerTasks;

    /**
     * Ensure Docker manager is running
     *
     * @param $type string
     * @return Result
     */
    private function ensureDockerManagerIsRunning($type)
    {
        if($type == "resolvable"){
            $managerContainerImage = "mgood/resolvable";
        }else{
            $managerContainerImage = "jwilder/nginx-proxy";
        }

        $result = $this->taskDockerPS()
            ->filterImage($managerContainerImage)
            ->filterRunning(false)
            ->run();

        $containers = $result->getData();
        if(sizeof($containers) > 0){

            $isRunning = false;
            foreach ($containers as $container) {
                print_r($container);
                if(explode(' ', $container['Status']) == 'Up'){
                    $isRunning = true;
                    break;
                }
            }

            if($isRunning){
                $result = $this->taskEmptySuccess()->run();
            }else{
                $result = $this->taskDockerStart($containers[0]['ID'])->run();
            }
        }else{
            $result = $this->taskDockerRun($managerContainerImage)->run();
        }

        return $result;
    }

    /**
     * Create Docker compose file
     *
     * @param $opts array
     */
    function localDockerInit($opts = [
        'type' => 'resolvable',
    ])
    {
        $this->say("Initialising Docker environment..");

        switch ($this->appType) {
            case 'laravel':
                $template = 'Laravel/docker-compose';
                break;
            case 'magento':
            case 'magentowp':
                $template = 'Magento/docker-compose';
                break;
            case 'magento2':
                $template = 'Magento2/docker-compose';
                break;
            case 'wordpress':
                $template = 'Wordpress/docker-compose';
                break;
            default:
                $template = 'Docker/base';
        }

        $template .= '-'.$opts['type'].'.twig';

        $result = $this->taskTemplateFileRender()
            ->template($template)
     		->params($this->app->getAppConfig())
     		->destination('docker-compose.yml')
            ->run();

        if($result->wasSuccessful()){
            $this->say("Docker environment initialized");
        }
    }

    /**
     * Create local Docker environment
     *
     * @param $opts
     */
    function localDockerUp($opts = [
        'type' => 'resolvable',
    ])
    {
        $this->say("Creating Docker environment..");

        $result = $this->ensureDockerManagerIsRunning($opts['type']);
        if($result->wasSuccessful()){
            $result = $this->taskDockerComposeUp()->run();

            if($result->wasSuccessful()){
                $this->say("Docker environment up&running");
            }
        }

    }

    /**
     * Destroy local Docker environment
     */
    function localDockerDown()
    {
        $this->say("Destroying Docker environment..");
        $result = $this->taskDockerComposeDown()->run();

        if($result->wasSuccessful()){
            $this->say("Docker environment destroyed");
        }
    }

    /**
     * Start Docker environment
     *
     * @param $opts array
     */
    function localDockerStart($opts = [
        'type' => 'resolvable',
    ])
    {
        $this->say("Starting Docker environment..");

        $result = $this->ensureDockerManagerIsRunning($opts['type']);
        if($result->wasSuccessful()){
            $result = $this->taskDockerComposeDown()->run();

            if($result->wasSuccessful()){
                $this->say("Docker environment running");
            }
        }
    }

    /**
     * Stop local Docker environment
     */
    function localDockerStop()
    {
        $this->say("Stopping Docker environment..");
        $result = $this->taskDockerComposeDown()->run();

        if($result->wasSuccessful()){
            $this->say("Docker environment stopped");
        }
    }

    /**
     * Connect to docker creating a new shell
     *
     * @param $service string
     */
    function localDockerConnect($service = 'web')
    {
        $this->say("Connecting to Docker environment..");
        $this->taskDockerComposeConnect($service)->run();
    }
}
