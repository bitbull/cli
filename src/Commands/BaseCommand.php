<?php

namespace Bitbull\Cli\Commands;

use Bitbull\Cli\Application\ApplicationFactory;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Collection\CollectionBuilder;
use Robo\Robo;
use Robo\Tasks;
use Bitbull\Cli\Application\ApplicationInterface;

class BaseCommand extends Tasks
{
    use UtilsTasks;

    /**
     * @var ApplicationInterface
     */
    protected $app = null;

    /**
     * @var string
     */
    protected $appType = '';

    /**
     * @var string
     */
    protected $rootPath = '';

    /**
     * BaseCommand constructor.
     */
    public function _init()
    {
        // Set root path
        $this->rootPath = Robo::application()->getRootPath();

        // Load specific application configuration
        $appType = Robo::Config()->get('app.type', getenv('APP_TYPE'));
        if ($appType == null) {
            if ($this->getContainer()->has('appType')) {
                $appType = $this->getContainer()->get('appType');
            } else {
                $appType = $this->_discoverAppType();
                $this->getContainer()->add('appType', $appType);
            }
        }
        $this->appType = $appType;

        if ($this->getContainer()->has('appInstance')) {
            $appInstance = $this->getContainer()->get('appInstance');
        } else {
            $appFactory = new ApplicationFactory($appType, $this->rootPath);
            $appInstance = $appFactory->getInstance();
            $this->getContainer()->add('appInstance', $appInstance);
        }
        $this->app = $appInstance;

        $this->stopOnFail(true);
    }


    /**
     * Discover application type based on file tree
     *
     * @return string
     */
    private function _discoverAppType()
    {
        if ($this->_exist('wp-load.php')) {
            return 'wordpress';
        }

        if ($this->_exist('app/Mage.php')) {
            if($this->_exist('wp/wp-load.php') || $this->_exist('blog/wp-load.php')){
                return 'magentowp';
            }

            return 'magento';
        }

        if ($this->_exist('composer.json')) {
            $composer = $this->loadJsonFile('composer.json');
            $dependencies = $composer['require'];
            if(array_key_exists('laravel/framework', $dependencies)){
                return 'laravel';
            }
            if(array_key_exists('laravel/lumen-framework', $dependencies)){
                return 'laravel';
            }
            if(array_key_exists('magento/product-community-edition', $dependencies)){
                return 'magento2';
            }
            if(array_key_exists('magento/product-enterprise-edition', $dependencies)){
                return 'magento2';
            }

            // Magento 1
            if (array_key_exists('magento-hackathon/magento-composer-installer', $dependencies)) {

                // Please note: this check works if:
                // - WP is added as dependency in Composer
                // - WP is under version control in a folder named "blog" or "wp"
                if (array_key_exists('wordpress', $dependencies) || array_key_exists('wordpress/core', $dependencies)
                    || $this->_exist('wp/wp-load.php') || $this->_exist('blog/wp-load.php')) {
                    return 'magentowp';
                }

                return 'magento';
            }

            return 'composer';
        }

        if ($this->_exist('package.json')) {
            return 'npm';
        }

        return 'base';
    }

}
