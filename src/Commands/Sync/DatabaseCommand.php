<?php

namespace Bitbull\Cli\Commands\Sync;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\Anonymizer\AnonymizerTasks;
use Bitbull\Cli\Tasks\AWS\AWSTasks;
use Bitbull\Cli\Tasks\Database\MySQLTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;

class DatabaseCommand extends BaseCommand
{
    use MySQLTasks, UtilsTasks, AWSTasks, AnonymizerTasks;

    /**
     * Create Database
     *
     * @param $database
     * @param $opts
     */
    function syncDatabaseCreate($database, $opts = [
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => 'root',
        'dump' => null,
        'dumpAnonymize' => false,
        'profile' => null,
        'region' => null,
    ])
    {
        $result = $this->taskMySQLSelect("SHOW DATABASES LIKE \"".$database."\"")
            ->host($opts['host'])
            ->username($opts['username'])
            ->password($opts['password'])
            ->run();

        if (sizeof($result->getData()['result']) > 0) {
            $this->say("Database already exist");
            return;
        }

        $this->say("Creating database..");

        $result = $this->taskMySQLStatement("CREATE DATABASE IF NOT EXISTS ".$database)
            ->host($opts['host'])
            ->username($opts['username'])
            ->password($opts['password'])
            ->run();

        if($result->wasSuccessful()){
            $this->say("Database $database created");
        }

        if($opts['dump'] === null){
            return;
        }

        $tmpDir = $this->_tmpDir();
        $dumpPath = $tmpDir.parse_url($opts['dump'],  PHP_URL_PATH);
        $parentPath = \dirname(parse_url($opts['dump'],  PHP_URL_PATH));
        if ($parentPath !== '/') {
            $this->_mkdir($tmpDir.$parentPath);
        }

        $this->syncDatabaseDumpDownload($opts['dump'], $dumpPath, [
            'profile' => $opts['profile'],
            'region' => $opts['region'],
        ]);
        $this->syncDatabaseRestore($database, $dumpPath, [
            'host' => $opts['host'],
            'username' => $opts['username'],
            'password' => $opts['password'],
            'anonymize' => $opts['dumpAnonymize'],
            'deleteBeforeImport' => false,
        ]);
        $this->_remove($dumpPath);
    }

    /**
     * Delete Database
     *
     * @param $database
     * @param $opts
     */
    function syncDatabaseDelete($database, $opts = [
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => 'root',
    ])
    {
        $result = $this->taskMySQLSelect("SHOW DATABASES LIKE \"".$database."\"")
            ->host($opts['host'])
            ->username($opts['username'])
            ->password($opts['password'])
            ->run();

        if (sizeof($result->getData()['result']) == 0) {
            $this->say("Database does not exist");
            return;
        }

        $this->say("Deleting database..");

        $result = $this->taskMySQLStatement("DROP DATABASE IF EXISTS ".$database)
            ->host($opts['host'])
            ->username($opts['username'])
            ->password($opts['password'])
            ->run();

        if($result->wasSuccessful()){
            $this->say("Database $database deleted");
        }
    }

    /**
     * Create dump from database
     *
     * @param string $database
     * @param string $to
     * @param array $opts
     */
    function syncDatabaseDump($database, $to, $opts = [
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => 'root',
        'exclude' => [],
        'gzip' => true
    ])
    {
        // Database dump

        $this->say("Dumping database..");

        if ($this->_exist($to)) {
            $this->say("Removing old $to dump file..");
            $this->_remove($to);
            $this->say("Removed old $to dump file");
        }
        if ($opts['gzip'] && $this->_exist($to.'.gz')) {
            $this->say("Removing old $to.gz dump file..");
            $this->_remove($to.'.gz');
        }

        $result = $this->taskMySQLDump()
            ->host($opts['host'])
     		->database($database)
            ->username($opts['username'])
     		->password($opts['password'])
     		->gzip($opts['gzip'])
            ->excludeTables($opts['exclude'])
     		->to($to)
            ->run();

        if($result->wasSuccessful()){
            $this->say("Database dump completed");
        }
    }

    /**
     * Restore database from dump
     *
     * @param string $database
     * @param string $from
     * @param array $opts
     */
    function syncDatabaseRestore($database, $from, $opts = [
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => 'root',
        'deleteBeforeImport' => false,
    ])
    {
        // Database restore

        $this->say("Restoring database..");
        $result = $this->taskMySQLImport()
            ->host($opts['host'])
            ->database($database)
            ->username($opts['username'])
            ->password($opts['password'])
            ->from($from)
            ->deleteBeforeImport($opts['deleteBeforeImport'])
            ->run();

        if($result->wasSuccessful()){
            $this->say("Database restored");
        }

    }

    /**
     * Anonymize database
     *
     * @param string $database
     * @param string $table
     * @param string $primaryKey
     * @param array $opts
     */
    function syncDatabaseAnonymize($database, $table, $primaryKey, $opts = [
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => 'root',
        'columns' => [],
        'formatters' => [],
    ])
    {
        $this->say("Anonymizing database..");
        $task = $this->taskAnonymizeMySQLTable($table, $primaryKey)
            ->host($opts['host'])
            ->database($database)
            ->username($opts['username'])
            ->password($opts['password']);

        foreach ($opts['columns'] as $index => $column){
            if(!isset($opts['formatters'][$index])){
                $this->say("Column $column skipped, no formatter match found");
                continue;
            }
            $formatter = $opts['formatters'][$index];
            $task->column($column, $formatter);
        }

        $result = $task->run();
        if($result->wasSuccessful()){
            $this->say("Database anonymized");
        }
    }

    /**
     * Upload database dump
     *
     * @param string $from
     * @param string $to
     * @param $opts
     */
    function syncDatabaseDumpUpload($from, $to, $opts = [
        'region' => null,
        'profile' => null,
    ])
    {
        $this->say("Uploading dump file..");

        $result = $this->taskS3UploadFile()
            ->region($opts['region'])
            ->profile($opts['profile'])
            ->from($from)
            ->to($to)
            ->run();

        if($result->wasSuccessful()){
            $this->say("Dump file uploaded");
        }

    }

    /**
     * Download database dump
     *
     * @param string $from
     * @param string $to
     * @param $opts
     */
    function syncDatabaseDumpDownload($from, $to, $opts = [
        'profile' => null,
        'region' => null,
    ])
    {
        $this->say("Downloading dump file..");

        $result = $this->taskDownload()
            ->from($from)
            ->to($to)
            ->region($opts['region'])
            ->profile($opts['profile'])
            ->run();

        if($result->wasSuccessful()){
            $this->say("Dump file downloaded");
        }
    }
}
