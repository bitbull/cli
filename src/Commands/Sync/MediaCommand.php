<?php

namespace Bitbull\Cli\Commands\Sync;

use Bitbull\Cli\Commands\BaseCommand;
use Bitbull\Cli\Tasks\AWS\AWSTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;

class MediaCommand extends BaseCommand
{
    use UtilsTasks, AWSTasks;

    /**
     * Create media archive
     *
     * @param string $from
     * @param string $to
     */
    function syncMediaArchive($from, $to)
    {
        $this->say("Extracting archive file..");

        $result = $this->taskPack($to)
            ->addDir('.', $from)
            ->run();

        if($result->wasSuccessful()){
            $this->say("Archive file extracted");
        }
    }

    /**
     * Extract media archive
     *
     * @param string $from
     * @param string $to
     */
    function syncMediaExtract($from, $to)
    {
        $this->say("Extracting archive file..");

        $result = $this->taskExtract($from)
            ->to($to)
            ->preserveTopDirectory(false)
            ->run();

        if($result->wasSuccessful()){
            $this->say("Archive file extracted");
        }
    }

    /**
     * Upload media archive
     *
     * @param string $from
     * @param string $to
     * @param $opts
     */
    function syncMediaArchiveUpload($from, $to, $opts = [
        'profile' => null,
        'region' => null,
    ])
    {
        $this->say("Uploading archive file..");

        $result = $this->taskS3UploadFile()
            ->region($opts['region'])
            ->profile($opts['profile'])
            ->from($from)
            ->to($to)
            ->run();

        if($result->wasSuccessful()){
            $this->say("Archive file uploaded");
        }
    }

    /**
     * Download media archive
     *
     * @param string $from
     * @param string $to
     * @param $opts
     */
    function syncMediaArchiveDownload($from, $to, $opts = [
        'profile' => null,
        'region' => null,
    ])
    {
        $this->say("Downloading archive file..");

        $result = $this->taskDownload()
            ->from($from)
            ->to($to)
            ->region($opts['region'])
            ->profile($opts['profile'])
            ->run();

        if($result->wasSuccessful()){
            $this->say("Archive file downloaded");
        }
    }
}
