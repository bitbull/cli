<?php
namespace Bitbull\Cli;

use Robo\Application as RoboApplication;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Exception\InvalidArgumentException;

class Application extends RoboApplication
{

    /**
     * @var null|string
     */
    private $rootPath = null;

    /**
     * @param string $name
     * @param string $version
     */
    public function __construct($name, $version)
    {
        parent::__construct($name, $version);

        $this->getDefinition()
            ->addOption(
                new InputOption('--config', null, InputOption::VALUE_OPTIONAL, 'Configuration file.')
            );
        $this->getDefinition()
            ->addOption(
                new InputOption('--root', null, InputOption::VALUE_OPTIONAL, 'Custom root path.')
            );
    }

    /**
     * @return null|string
     */
    public function getRootPath()
    {
        return $this->rootPath;
    }

    /**
     * @param null|string $rootPath
     */
    public function setRootPath($rootPath)
    {
        if (!is_dir($rootPath)) {
            throw new InvalidArgumentException("$rootPath is not a valid directory");
        }

        $this->rootPath = $rootPath;
        chdir($this->rootPath);
    }
}
