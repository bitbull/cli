<?php

namespace Bitbull\Cli\Tasks\Magento2;

use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;
use Robo\Result;

/**
 * Enable or disable a specific module
 *
 * Example usage:
 * ```php
 * $this->taskMagentoCLI()
 * 		->command('setup:cache:enable')
 * 		->params()
 * ->run();
 * ```
 *
 * @return Result
 */
class MagentoCLI extends BaseTaskCommand {

    use DynamicParams;

    /** @var string */
    protected $executable = "php ./bin/magento";

    /** @var string */
    protected $command = "";

    /** @var array */
    protected $params = [];

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return $this->executable . ' ' . $this->command . ' ' . implode(' ', $this->params);
    }

    /**
     * Add parameter
     *
     * @param $params string
     * @return MagentoCLI
     */
    function addParam($params)
    {
        array_push($this->params, $params);

        return $this;
    }
}
