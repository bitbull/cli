<?php

namespace Bitbull\Cli\Tasks\Magento2;

use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;
use Robo\Result;

/**
 * Enable or disable a specific module
 *
 * Example usage:
 * ```php
 * $this->taskN98magerun2()
 * 		->command('dev:module:enable')
 * 		->params([
 *          'Cm_RedisSession'
 *      ])
 * ->run();
 * ```
 *
 * @return Result
 */
class N98magerun2 extends BaseTaskCommand {

    use DynamicParams;

    /** @var string */
    protected $executable = "n98-magerun2";

    /** @var string */
    protected $command = null;

    /** @var array */
    protected $params = [];

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return $this->executable.' --skip-root-check '.$this->command.' '. implode(' ', $this->params);
    }

    /**
     * Add parameter
     *
     * @param $params string
     * @return N98magerun2
     */
    function addParam($params)
    {
        array_push($this->params, $params);

        return $this;
    }

}
