<?php

namespace Bitbull\Cli\Tasks\Magento2;

trait Magento2Tasks
{
    /**
     * Magento2 N98 task
     *
     * @return N98magerun2
     */
    protected function taskN98magerun2()
    {
        return $this->task(N98magerun2::class);
    }

    /**
     * Magento CLI task
     *
     * @return MagentoCLI
     */
    protected function taskMagentoCLI()
    {
        return $this->task(MagentoCLI::class);
    }
}