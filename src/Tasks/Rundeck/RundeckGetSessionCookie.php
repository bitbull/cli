<?php

namespace Bitbull\Cli\Tasks\Rundeck;

use Bitbull\Cli\Tasks\BaseTask;
use Bitbull\Cli\Tasks\Utils\HttpRequest;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Common\DynamicParams;
use Robo\LoadAllTasks;
use Robo\Result;

class RundeckGetSessionCookie extends BaseTask
{
    use DynamicParams, UtilsTasks, LoadAllTasks;

    /** @var string */
    protected $username = null;

    /** @var string */
    protected $password = null;

    /** @var string */
    protected $host = null;

    public function __construct($host)
    {
        parent::__construct();

        $this->host = $host;
    }

    /**
     * Get a Rundeck session cookie
     *
     * Example usage:
     * ```php
     * $response = $this->taskRundeckGetSessionCookie('http://docker-swarm.example.com:9000')
     *      ->username('admin')
     * 		->password('123abc')
     * ->run();
     * $token = $response->getData()['cookie'];
     * ```
     *
     * @return Result
     */
    function run()
    {
        $this->printTaskDebug("Request token for user $this->username..");
        $response = $this->taskHttpRequest($this->host . '/j_security_check')
            ->payload([
                'j_username' => $this->username,
                'j_password' => $this->password,
            ])
            ->method('POST')
            ->statusesOk(['302'])
            ->contentType(HttpRequest::CONTENT_TYPE_FORM)
            ->run();

        if (!$response->wasSuccessful()) {
            $this->printTaskDebug('Error during token request');
            return $response;
        }

        $headers = $response->getData()['headers'];
        if ($headers['Location'] && strlen($headers['Location']) - strlen('/user/error') == strpos($headers['Location'], '/user/error')) {
            return Result::error($this, 'Rundeck autentication error');
        }

        $cookies = $response->getData()['cookies'];
        $sessionCookie = $cookies['JSESSIONID'];
        $this->printTaskDebug('Session cookie successfully retrieved');

        return Result::success($this, 'Logged to Rundeck', [
            'sessionCookie' => $sessionCookie
        ]);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        return Result::success($this, 'Logged to Rundeck', [
            'sessionCookie' => 'xxxxxxxxxxxxxxxxx'
        ]);
    }

}
