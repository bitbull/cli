<?php

namespace Bitbull\Cli\Tasks\Rundeck;

trait RundeckTasks
{
    /**
     * Task Execute Command
     *
     * @param $host
     * @return RundeckGetSessionCookie
     */
    protected function taskRundeckGetSessionCookie($host)
    {
        return $this->task(RundeckGetSessionCookie::class, $host);
    }

    /**
     * Task Execute Command
     *
     * @param $host
     * @param $id
     * @return RundeckExecuteJob
     */
    protected function taskRundeckExecuteJob($host, $id)
    {
        return $this->task(RundeckExecuteJob::class, $host, $id);
    }
}