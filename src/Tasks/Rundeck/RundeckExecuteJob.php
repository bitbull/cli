<?php

namespace Bitbull\Cli\Tasks\Rundeck;

use Bitbull\Cli\Tasks\BaseTask;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Common\DynamicParams;
use Robo\LoadAllTasks;
use Robo\Result;

class RundeckExecuteJob extends BaseTask
{
    use DynamicParams, UtilsTasks, LoadAllTasks;

    /** @var string */
    protected $sessionCookie = null;

    /** @var string */
    protected $host = null;

    /** @var string */
    protected $apiVersion = '18';

    /** @var string */
    protected $id = null;

    /** @var string */
    protected $loglevel = 'DEBUG';

    /** @var string */
    protected $user = 'bb-cli';

    /** @var string */
    protected $options = [];

    public function __construct($host, $id)
    {
        parent::__construct();

        $this->host = $host;
        $this->id = $id;
    }

    /**
     * Run a Rundeck job
     *
     * Example usage:
     * ```php
     * $response = $this->taskRundeckExecuteJob('http://rundeck.example.com', $jobId)
     *      ->sessionCookie('')
     * ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $this->printTaskDebug('Retrieving all stacks data..');
        $response = $this->taskHttpRequest($this->host . '/api/' . $this->apiVersion . '/job/' . $this->id. '/run')
            ->headers([
                'Cookie: JSESSIONID='.$this->sessionCookie
            ])
            ->payload([
                'loglevel' => $this->loglevel,
                'asUser' => $this->user,
                'options' => $this->options,
            ])
            ->method('POST')
            ->run();

        if (!$response->wasSuccessful()) {
            $this->printTaskDebug('Error job run request');
            return $response;
        }

        return Result::success($this);
    }

    public function addOption($key, $value)
    {
        $this->options[$key] = $value;
    }
}
