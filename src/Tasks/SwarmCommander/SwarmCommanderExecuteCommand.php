<?php

namespace Bitbull\Cli\Tasks\SwarmCommander;

use Bitbull\Cli\Tasks\BaseTask;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Common\DynamicParams;
use Robo\LoadAllTasks;
use Robo\Result;

class SwarmCommanderExecuteCommand extends BaseTask
{
    use DynamicParams, UtilsTasks, LoadAllTasks;

    /** @var string */
    protected $endpoint = null;

    /** @var string */
    protected $apiKey = null;

    /** @var string */
    protected $service = null;

    /** @var string */
    protected $command = null;

    public function __construct($endpoint, $apiKey)
    {
        parent::__construct();

        $this->endpoint = $endpoint;
        $this->apiKey = $apiKey;
    }

    /**
     * Execute command
     *
     * Example usage:
     * ```php
     * $response = $this->taskSwarmCommanderExecuteCommand('https://xxxxxxx.execute-api.eu-west-1.amazonaws.com/swarm/', 'xxxxxxxxx')
     *      ->service('example')
     *      ->command('cd /tmp/ && ls')
     *      ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $this->printTaskDebug('Retrieving containers data..');
        $response = $this->taskHttpRequest($this->endpoint . 'command')
            ->headers([
                'x-api-key: '.$this->apiKey
            ])
            ->method('POST')
            ->payload([
                'service' => $this->service,
                'command' => $this->command,
            ])
            ->run();

        $ssmCommandId = $response->getData()['rawResponse'];

        return Result::success($this, 'Command executed', [
            'ssmCommandId' => $ssmCommandId,
        ]);
    }
}
