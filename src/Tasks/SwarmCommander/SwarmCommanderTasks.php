<?php

namespace Bitbull\Cli\Tasks\SwarmCommander;

trait SwarmCommanderTasks
{
    /**
     * Execute command
     *
     * @param $endpoint
     * @param $apiKey
     * @return SwarmCommanderExecuteCommand
     */
    protected function taskSwarmCommanderExecuteCommand($endpoint, $apiKey)
    {
        return $this->task(SwarmCommanderExecuteCommand::class, $endpoint, $apiKey);
    }
}