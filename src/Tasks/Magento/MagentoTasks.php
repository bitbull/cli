<?php

namespace Bitbull\Cli\Tasks\Magento;

trait MagentoTasks
{
    /**
     * Magento n98 task
     *
     * @return N98magerun
     */
    protected function taskN98magerun()
    {
        return $this->task(N98magerun::class);
    }
}