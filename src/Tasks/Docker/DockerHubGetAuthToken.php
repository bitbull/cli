<?php

namespace Bitbull\Cli\Tasks\Docker;

use Bitbull\Cli\Tasks\BaseTask;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Common\DynamicParams;
use Robo\LoadAllTasks;
use Robo\Result;

class DockerHubGetAuthToken extends BaseTask
{
    use UtilsTasks;

    /** @var string */
    protected $username = null;

    /** @var string */
    protected $password = null;

    /** @var string */
    protected $host = 'https://hub.docker.com/v2';

    /**
     * Get a auth token
     *
     * Example usage:
     * ```php
     * $response = $this->taskDockerHubGetAuthToken()
     *      ->username('admin')
     * 		->password('123abc')
     * ->run();
     * $token = $response->getData()['token'];
     * ```
     *
     * @return Result
     */
    function run()
    {
        $this->printTaskDebug("Request token for user $this->username..");
        $response = $this->taskHttpRequest($this->host . '/users/login/')
            ->payload([
                'username' => $this->username,
                'password' => $this->password,
            ])
            ->method('POST')
            ->run();

        if (!$response->wasSuccessful()) {
            $this->printTaskDebug('Error during token request');
            return $response;
        }

        $response = $response->getData()['response'];
        $token = $response->token;

        $this->printTaskDebug('Token successfully retrieved');

        return Result::success($this, 'Logged to Docker Hub', [
            'token' => $token
        ]);
    }
}
