<?php

namespace Bitbull\Cli\Tasks\Docker;

use Bitbull\Cli\Tasks\BaseTask;
use Bitbull\Cli\Tasks\Docker\DockerTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Common\DynamicParams;
use Robo\LoadAllTasks;
use Robo\Result;

class DockerHubImageTags extends BaseTask
{
    use UtilsTasks, DockerTasks;

    /** @var string */
    protected $image = null;

    /** @var string */
    protected $token = null;

    /** @var string */
    protected $host = 'https://hub.docker.com/v2';

    /** @var string */
    protected $pageSize = 10000;

    /** @var string */
    protected $page = 1;

    /**
     * Constructor
     *
     * @param $image string
     */
    function __construct($image)
    {
        parent::__construct();

        $this->image = $image;
    }

    /**
     * Get image tags
     *
     * Example usage:
     * ```php
     * $response = $this->taskDockerHubImageDetails('myuser', 'myimage')
     *      ->token('123abc')
     *      ->run();
     * $image = $response->getData();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $response = $this->taskHttpRequest($this->host . '/repositories/' . $this->image . '/tags/?page_size=' . $this->pageSize . '&page=' . $this->page)
            ->headers([
                'Authorization: JWT '.$this->token
            ])
            ->method('GET')
            ->run();

        $response = $response->getData()['response'];
        $tags = $response->results;

        $this->printTaskDebug('Image tags successfully retrieved');

        return Result::success($this, "Image $this->image tags", [
            'tags' => $tags
        ]);
    }
}
