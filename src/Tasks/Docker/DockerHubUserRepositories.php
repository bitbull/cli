<?php

namespace Bitbull\Cli\Tasks\Docker;

use Bitbull\Cli\Tasks\BaseTask;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Common\DynamicParams;
use Robo\LoadAllTasks;
use Robo\Result;

class DockerHubUserRepositories extends BaseTask
{
    use UtilsTasks;

    /** @var string */
    protected $username = null;

    /** @var string */
    protected $token = null;

    /** @var string */
    protected $pageSize = 10000;

    /** @var string */
    protected $page = 1;

    /** @var string */
    protected $host = 'https://hub.docker.com/v2';

    /**
     * Constructor
     *
     * @param $username string
     */
    function __construct($username)
    {
        parent::__construct();

        $this->username = $username;
    }

    /**
     * Get user repositories
     *
     * Example usage:
     * ```php
     * $response = $this->taskDockerHubGetAuthToken()
     *      ->username('admin')
     * 		->token('123abc')
     *      ->run();
     * $repositories = $response->getData();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $this->printTaskDebug("Request repositories for user $this->username..");
        $response = $this->taskHttpRequest($this->host . '/repositories/' . $this->username . '/?page_size=' . $this->pageSize . '&page=' . $this->page)
            ->headers([
                'Authorization: JWT '.$this->token
            ])
            ->method('GET')
            ->run();

        if (!$response->wasSuccessful()) {
            $this->printTaskDebug('Error during repositories request');
            return $response;
        }

        $response = $response->getData()['response'];
        $repositories = $response->results;

        $this->printTaskDebug('Repositories successfully retrieved');

        return Result::success($this, 'User repositories', [
            'repositories' => $repositories
        ]);
    }
}
