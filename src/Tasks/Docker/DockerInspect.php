<?php

namespace Bitbull\Cli\Tasks\Docker;

use Bitbull\Cli\Tasks\BaseTask;
use Robo\Common\DynamicParams;
use Robo\Common\ExecOneCommand;
use Robo\Result;
use Symfony\Component\Process\Process;

class DockerInspect extends BaseTask {

    use DynamicParams, ExecOneCommand;

    /** @var string */
    protected $containerID;

    /**
     * Constructor
     *
     * @param $containerID string
     */
    function __construct($containerID)
    {
        parent::__construct();

        $this->containerID = $containerID;
    }

    /**
     * Get Docker container infos
     *
     * Example usage:
     * ```php
     * $result = $this->taskDockerInspect('container ID or image')
     * ->run();
     * $data = $result->getData();
     * ```
     *
     * @return Result
     */
    function run(){

        $command = 'docker inspect --format "{{json .}}" '.$this->containerID;

        $process = new Process($command, getcwd());
        $process->run();
        $commandOutput = $process->getOutput();

        if(!$process->isSuccessful()){
            return Result::error($this, $process->getErrorOutput());
        }

        $data = (array) json_decode($commandOutput);

        return Result::success($this, 'Command executed successfully', $data);
    }

}
