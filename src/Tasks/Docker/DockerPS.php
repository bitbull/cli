<?php

namespace Bitbull\Cli\Tasks\Docker;

use Bitbull\Cli\Tasks\BaseTask;
use Robo\Common\DynamicParams;
use Robo\Common\ExecOneCommand;
use Robo\Result;
use Symfony\Component\Process\Process;

class DockerPS extends BaseTask {

    use DynamicParams, ExecOneCommand;

    /** @var string */
    protected $filterRunning = false;

    /** @var string */
    protected $filterName = null;

    /** @var string */
    protected $filterImage = null;

    /**
     * Get all running Docker containers
     *
     * Example usage:
     * ```php
     * $result = $this->taskDockerPS()
     *      ->filterRunning(true)
     *      ->filterImage('mgood/resolvable')
     * ->run();
     * $images = $result->getData();
     * foreach($images as $image){
     *    ...
     * }
     * ```
     *
     * @return Result
     */

    function run(){

        $command = 'docker ps --format "{{json .}}" ';

        if(!$this->filterRunning){
            $command .= ' -a';
        }

        $process = new Process($command, getcwd());
        $process->run();
        $commandOutput = $process->getOutput();

        if(!$process->isSuccessful()){
            return Result::error($this, $process->getErrorOutput());
        }

        $lines = explode("\n" , $commandOutput);
        $containers = [];
        foreach ($lines as $containerData) {
            $container = (array) json_decode($containerData);
            if(sizeof($container) == 0){
                continue;
            }

            $isValid = true;
            if($this->filterImage !== null){
                $isValid = ($container['Image'] == $this->filterImage);
            }
            if($this->filterName !== null){
                $isValid = ($container['Name'] == $this->filterName);
            }
            if($isValid){
                array_push($containers, $container);
            }
        }

        return Result::success($this, 'Command executed successfully', $containers);
    }

}
