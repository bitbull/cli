<?php

namespace Bitbull\Cli\Tasks\Docker;

use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;

class DockerComposeDown extends BaseTaskCommand {

    use DynamicParams;

    /** @var string */
    protected $command = "docker-compose";

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return $this->command.' down';
    }
}
