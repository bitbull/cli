<?php

namespace Bitbull\Cli\Tasks\Docker;

use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;
use Robo\Result;

/**
 * Get all running Docker containers
 *
 * Example usage:
 * ```php
 * $this->taskDockerLogin()
 *      ->username('foo')
 *      ->password('bar')
 * ->run();
 * ```
 *
 * @return Result
 */
class DockerLogin extends BaseTaskCommand {

    use DynamicParams;

    /** @var string */
    protected $command = "docker login";

    /** @var string */
    protected $repo;

    /** @var string */
    protected $username;

    /** @var string */
    protected $password;

    /**
     * Constructor
     *
     * @param $repository string
     */
    function __construct($repository = null)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        $command = $this->command." --username $this->username --password $this->password";

        if($this->repository !== null){
            $command .= " $this->repository";
        }

        return $command;
    }
}
