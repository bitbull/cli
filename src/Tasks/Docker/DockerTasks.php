<?php

namespace Bitbull\Cli\Tasks\Docker;

trait DockerTasks
{
    /**
     * Deploy using docker compose UI API
     *
     * @return DockerComposeUI
     */
    protected function taskDockerComposeUI()
    {
        return $this->task(DockerComposeUI::class);
    }

    /**
     * Deploy docker image to repository
     *
     * @param $image string
     * @return DockerPush
     */
    protected function taskDockerPush($image)
    {
        return $this->task(DockerPush::class, $image);
    }

    /**
     * Get all running Docker containers
     *
     * @return DockerPS
     */
    protected function taskDockerPS()
    {
        return $this->task(DockerPS::class);
    }

    /**
     * Get all running Docker containers
     *
     * @param $repository
     * @return DockerLogin
     */
    protected function taskDockerLogin($repository = null)
    {
        return $this->task(DockerLogin::class, $repository);
    }

    /**
     * DockerCompose up
     *
     * @return DockerComposeUp
     */
    protected function taskDockerComposeUp()
    {
        return $this->task(DockerComposeUp::class);
    }

    /**
     * DockerCompose down
     *
     * @return DockerComposeDown
     */
    protected function taskDockerComposeDown()
    {
        return $this->task(DockerComposeDown::class);
    }

    /**
     * DockerCompose start
     *
     * @return DockerComposeStart
     */
    protected function taskDockerComposeStart()
    {
        return $this->task(DockerComposeStart::class);
    }

    /**
     * DockerCompose stop
     *
     * @return DockerComposeStop
     */
    protected function taskDockerComposeStop()
    {
        return $this->task(DockerComposeStop::class);
    }

    /**
     * DockerCompose connect
     *
     * @param $service
     * @return DockerComposeConnect
     */
    protected function taskDockerComposeConnect($service)
    {
        return $this->task(DockerComposeConnect::class, $service);
    }

    /**
     * Get DockerHub auth token
     *
     * @return DockerComposeConnect
     */
    protected function taskDockerHubGetAuthToken()
    {
        return $this->task(DockerHubGetAuthToken::class);
    }

    /**
     * Get DockerHub image tags
     *
     * @param $image
     * @return DockerComposeConnect
     */
    protected function taskDockerHubImageTags($image)
    {
        return $this->task(DockerHubImageTags::class, $image);
    }

    /**
     * Get DockerHub image repositories
     *
     * @param $username
     * @return DockerComposeConnect
     */
    protected function taskDockerHubUserRepositories($username)
    {
        return $this->task(DockerHubUserRepositories::class, $username);
    }

}