<?php

namespace Bitbull\Cli\Tasks\Docker;

use Bitbull\Cli\Tasks\BaseTask;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Common\DynamicParams;
use Robo\Common\TaskIO;
use Robo\LoadAllTasks;
use Robo\Result;
use Symfony\Component\Filesystem\Filesystem;

class DockerComposeUI extends BaseTask {

    use DynamicParams, TaskIO, LoadAllTasks, UtilsTasks;

    /** @var Filesystem */
    protected $fs = null;

    /** @var string */
    protected $projectName = null;

    /** @var string */
    protected $dockerComposeFile = null;

    /** @var string */
    protected $username = null;

    /** @var string */
    protected $password = null;

    /** @var string */
    protected $host = null;

    /** @var array */
    protected $envVars = [];

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $this->fs = new Filesystem();
        $this->path = getcwd();
    }

    /**
     * Deploy to Docker Compose UI
     *
     * Example usage:
     * ```php
     * $this->taskDockerComposeUI()
     *      ->name('my-project')
     * 		->dockerComposeFile('./docker-compose.yml')
     * ->run();
     * ```
     *
     * @return Result
     */

    function run(){

        $baseEndpoint = 'https://'.$this->username.':'.$this->password.'@'.$this->host;

        $response = $this->taskHttpRequest($baseEndpoint.'/projects/'.$this->projectName)->run();
        if(!$response->wasSuccessful()){

            $ymlContent = file_get_contents($this->dockerComposeFile);
            $envContent = "";
            foreach ($this->envVars as $key => $value) {
                $envContent .= $key."=".$value."\n";
            }
            $this->taskHttpRequest($baseEndpoint.'/create')->methd('POST')->payload([
                'name' => $this->projectName,
                'yml' => $ymlContent,
                'env' => $envContent
            ]);
            $this->taskHttpRequest($baseEndpoint.'/build')->methd('POST')->payload([
                'id' => $this->projectName,
                'pull' => true,
                'no_cache' => true,
            ])->run();

        }

        $collection = $this->collectionBuilder();
        $collection->taskHttpRequest($baseEndpoint.'/projects/'.$this->projectName);
        $collection->taskHttpRequest($baseEndpoint.'/down')->methd('POST')->payload([
            'id' => $this->projectName
        ]);
        $collection->taskHttpRequest($baseEndpoint.'/projects')->methd('PUT')->payload([
            'id' => $this->projectName
        ]);
        $collection->taskHttpRequest($baseEndpoint.'/projects')->methd('POST')->payload([
            'id' => $this->projectName
        ]);

        $result = $collection->run();

        if(!$result->wasSuccessful()){
            return $result;
        }

        return Result::success($this);
    }

}
