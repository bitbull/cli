<?php

namespace Bitbull\Cli\Tasks\Docker;

use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;

class DockerPush extends BaseTaskCommand {

    use DynamicParams;

    /** @var string */
    protected $command = "docker push";

    /**
     * @var string
     */
    protected $image;

    /**
     * Constructor
     *
     * @param $image string
     */
    function __construct($image)
    {
        $this->image = $image;
    }

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return $this->command.' '. $this->image;
    }
}
