<?php

namespace Bitbull\Cli\Tasks\Docker;

use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;

class DockerComposeStart extends BaseTaskCommand {

    use DynamicParams;

    /** @var string */
    protected $command = "docker-compose";

    /**
     * @var string
     */
    protected $detached = true;

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        $command = $this->command.' start';
        if($this->detached){
            $command .= ' -d';
        }

        return $command;
    }
}
