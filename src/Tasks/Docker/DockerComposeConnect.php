<?php

namespace Bitbull\Cli\Tasks\Docker;

use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;
use Robo\Result;
use Robo\Robo;
use Bitbull\Cli\Tasks\BaseTask;
use Symfony\Component\Process\Process;

class DockerComposeConnect extends BaseTask {

    use DynamicParams;

    /** @var string */
    protected $shell = '/bin/bash';

    /** @var string */
    protected $command = 'docker-compose exec';

    /** @var string */
    protected $service = 'web';

    /**
     * Constructor
     *
     * @param $service string
     */
    function __construct($service)
    {
        $this->service = $service;
    }

    /**
     * Get Docker container infos
     *
     * Example usage:
     * ```php
     * $this->taskDockerComposeConnect('web')
     *   ->shell('/bin/bash')
     *   ->run();
     * ```
     *
     * @return Result
     */
    public function run()
    {
        $process = new Process($this->command.' -it '.$this->service.' '.$this->shell, getcwd(), null, Robo::input());
        $process->run();

        if(!$process->isSuccessful()){
            return Result::error($this, $process->getErrorOutput());
        }else{
            return Result::success($this);
        }
    }
}
