<?php

namespace Bitbull\Cli\Tasks\Database;

use Bitbull\Cli\Tasks\BaseTask;
use Robo\Common\DynamicParams;
use Robo\Result;

class MySQLImport extends BaseTask {

    use DynamicParams, MySQLTasks;

    /** @var string */
    protected $client = "mysql";

    /** @var string */
    protected $compressClient = "gzip";

    /** @var string */
    protected $host = null;

    /** @var string */
    protected $database = null;

    /** @var string */
    protected $username = null;

    /** @var string */
    protected $password = null;

    /** @var string */
    protected $from = null;

    /** @var string */
    protected $deleteBeforeImport = false;

    /**
     * Import dump file in MySQL
     *
     * Example usage:
     * ```php
     * $this->taskMySQLImport()
     * 		->host('localhost')
     * 		->database('myschema')
     * 		->username('root')
     * 		->password('root')
     * 		->from('/dump.sql')
     * ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        // Decompress file

        $dumpExtension = pathinfo($this->from, PATHINFO_EXTENSION);
        if($dumpExtension === 'gz'){

            $result = $this->executeCommand("$this->compressClient -d $this->from");
            if (!$result->wasSuccessful()) {
                return $result;
            }

            $this->from = str_replace('.'.$dumpExtension, '', $this->from);
        }

        if ($this->deleteBeforeImport) {
            $this->taskMySQLStatement("DROP DATABASE $this->database")->run();
            $this->taskMySQLStatement("CREATE DATABASE $this->database")->run();
        }

        // Import dump file

        return $this->executeCommand("$this->client -h $this->host -u $this->username -p'$this->password' $this->database < $this->from");
    }
}
