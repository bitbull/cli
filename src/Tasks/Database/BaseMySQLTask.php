<?php

namespace Bitbull\Cli\Tasks\Database;

use Bitbull\Cli\Tasks\BaseTask;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Connection;

abstract class BaseMySQLTask extends BaseTask {

    use MySQLTasks;

    /** @var string */
    protected $host = null;

    /** @var string */
    protected $database = null;

    /** @var string */
    protected $username = null;

    /** @var string */
    protected $password = null;

    /** @var string */
    protected $prefix = null;

    /** @var string */
    protected $charset = 'utf8';

    protected function getConnection() {
        $capsule = new Manager();
        $params = [
            'driver'    => 'mysql',
            'host'      => $this->host,
            'database'  => $this->database,
            'username'  => $this->username,
            'password'  => $this->password,
            'prefix'    => $this->prefix,
            'charset'   => $this->charset,
        ];
        $this->printTaskDebug("Connecting to host '$this->host' with user '$this->username' using database '$this->database'");
        $capsule->addConnection($params);

        return $capsule->getConnection();
    }

}
