<?php

namespace Bitbull\Cli\Tasks\Database;

use Illuminate\Database\QueryException;
use Robo\Result;

class MySQLStatement extends BaseMySQLTask {

    /** @var string */
    protected $query = null;

    /**
     * MySQLQuery constructor.
     * @param $query
     */
    public function __construct($query)
    {
        parent::__construct();

        $this->query = $query;
    }

    /**
     * Execute MySQL query
     *
     * Example usage:
     * ```php
     * $this->taskMySQLStatement('create database test')
     * 		->host('localhost')
     * 		->username('root')
     * 		->password('root')
     * ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $db = $this->getConnection();

        try{
            $db->statement($this->query);
        }catch (QueryException $e){
            return Result::error($this, $e->getMessage(), $e);
        }

        return Result::success($this, "Query executed");
    }


}
