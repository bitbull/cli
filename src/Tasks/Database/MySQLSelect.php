<?php

namespace Bitbull\Cli\Tasks\Database;

use Illuminate\Database\QueryException;
use Robo\Result;

class MySQLSelect extends BaseMySQLTask {

    /** @var string */
    protected $query = null;

    /**
     * MySQLQuery constructor.
     * @param $query
     */
    public function __construct($query)
    {
        parent::__construct();

        $this->query = $query;
    }

    /**
     * Execute MySQL query
     *
     * Example usage:
     * ```php
     * $this->taskMySQLSelect('select * from table')
     * 		->host('localhost')
     * 		->database('database')
     * 		->username('root')
     * 		->password('root')
     * ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $db = $this->getConnection();

        try{
            $result = $db->select($this->query);
        }catch (QueryException $e){
            return Result::error($this, $e->getMessage(), $e);
        }

        return Result::success($this, "Query executed", [
            'result' => $result
        ]);
    }


}
