<?php

namespace Bitbull\Cli\Tasks\Database;

trait MySQLTasks
{

    /**
     * MySQL import
     *
     * @return MySQLImport
     */
    protected function taskMySQLImport()
    {
        return $this->task(MySQLImport::class);
    }

    /**
     * MySQL dump
     *
     * @return MySQLDump
     */
    protected function taskMySQLDump()
    {
        return $this->task(MySQLDump::class);
    }

    /**
     * MySQL select query
     *
     * @param $query string
     * @return MySQLSelect
     */
    protected function taskMySQLSelect($query)
    {
        return $this->task(MySQLSelect::class, $query);
    }

    /**
     * MySQL statement query
     *
     * @param $query string
     * @return MySQLStatement
     */
    protected function taskMySQLStatement($query)
    {
        return $this->task(MySQLStatement::class, $query);
    }

    /**
     * MySQL update query
     *
     * @param $query string
     * @return MySQLStatement
     */
    protected function taskMySQLUpdate($query)
    {
        return $this->task(MySQLUpdate::class, $query);
    }

}