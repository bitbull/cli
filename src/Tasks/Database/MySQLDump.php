<?php

namespace Bitbull\Cli\Tasks\Database;

use Bitbull\Cli\Tasks\BaseTask;
use Robo\Common\DynamicParams;
use Robo\Result;

class MySQLDump extends BaseTask {

    use DynamicParams;

    /** @var string */
    protected $client = "mysqldump";

    /** @var string */
    protected $compressClient = "gzip";

    /** @var string */
    protected $host = null;

    /** @var string */
    protected $database = null;

    /** @var string */
    protected $username = null;

    /** @var string */
    protected $password = null;

    /** @var array */
    protected $excludeTables = [];

    /** @var string */
    protected $gzip = false;

    /** @var string */
    protected $to = null;

    /**
     * Dump MySQL database
     *
     * Example usage:
     * ```php
     * $this->taskMySQLImport()
     * 		->host('localhost')
     * 		->database('myschema')
     * 		->username('root')
     * 		->password('root')
     * 		->gzip(true)
     *      ->excludeTables([
     *          'table1', 'table2'
     *      ])
     * 		->to('/dump.sql')
     * ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        // Build command

        $command = "$this->client --lock-tables=false --skip-lock-tables -h $this->host -u $this->username -p$this->password";

        // Create structure

        $structure_dump = "$command $this->database --skip-triggers --no-data > $this->to";
        $result = $this->executeCommand($structure_dump);

        if (!$result->wasSuccessful()) {
            return $result;
        }

        // Excluding tables for data dump

        foreach ($this->excludeTables as $table) {
            $command .= "--ignore-table=$this->database.$table";
        }

        // Create data dump

        $data_dump = "$command $this->database --no-create-info >> $this->to";
        $result = $this->executeCommand($data_dump);

        if (!$result->wasSuccessful()) {
            return $result;
        }

        // Compress file

        if($this->gzip){

            $result = $this->executeCommand($this->compressClient.' '.$this->to);
            if (!$result->wasSuccessful()) {
                return $result;
            }
        }

        return Result::success($this);
    }
}
