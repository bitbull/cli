<?php

namespace Bitbull\Cli\Tasks\Anonymizer;

use Bitbull\Cli\Tasks\BaseTask;
use Faker\Factory as FakerFactory;
use Faker\Generator;
use Robo\Result;
use Robo\Robo;

/**
 * Enable or disable a specific module
 *
 * Example usage:
 * ```php
 * $this->taskGenerateFakeData('firstName')
 * 		->params([])
 * ->run();
 * ```
 *
 * @return Result
 */
class GenerateFakeData extends BaseTask {

    /** @var string */
    protected $formatter = null;

    /** @var string */
    protected $locale = 'en_US';

    /** @var string */
    protected $unique = false;

    /** @var string */
    protected $optional = false;

    /** @var string */
    protected $valid = null;

    /** @var string */
    protected $options = null;

    /** @var string */
    protected $randomSuffix = false;

    /**
     * GenerateFakeData constructor.
     * @param $formatter
     */
    public function __construct($formatter)
    {
        parent::__construct();
        $this->formatter = $formatter;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $fakerInstance = $this->getFakerInstance();

        if ($this->unique) {
            $fakerInstance = $fakerInstance->unique();
        }
        if ($this->optional) {
            $fakerInstance = $fakerInstance->optional();
        }

        switch ($this->formatter) {
            case 'randomEmail':
                $data = $fakerInstance->lexify('??????????@example.com');
                break;
            default:
                $data = $fakerInstance->{$this->formatter}();
        }

        if ($this->randomSuffix) {
            $data .= '_'.$fakerInstance->lexify('?????');
        }
        if($data !== null) {
            Robo::getContainer()->add('fakerInstance', $fakerInstance);
        }

        return Result::success($this, 'Fake data generated', [
            'fakeData' => $data
        ]);
    }

    /**
     * @return Generator
     */
    private function getFakerInstance()
    {
        $container = Robo::getContainer();

        if ($container->has('fakerInstance')) {
            $fakerInstance = $container->get('fakerInstance');
        } else {
            $fakerInstance = FakerFactory::create($this->locale);
            $fakerInstance->addProvider(new \Faker\Provider\it_IT\Company($fakerInstance));
            $container->add('fakerInstance', $fakerInstance);
        }

        return $fakerInstance;
    }

}
