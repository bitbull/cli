<?php

namespace Bitbull\Cli\Tasks\Anonymizer;

use Bitbull\Cli\Tasks\Database\BaseMySQLTask;
use Illuminate\Database\QueryException;
use Robo\Common\ProgressIndicatorAwareTrait;
use Robo\Result;

/**
 * Enable or disable a specific module
 *
 * Example usage:
 * ```php
 * $this->taskGenerateFakeData('firstName')
 * 		->params([])
 * ->run();
 * ```
 *
 * @return Result
 */
class AnonymizeMySQLTable extends BaseMySQLTask {

    use ProgressIndicatorAwareTrait, AnonymizerTasks;

    const OPTIONS_SEPARATOR = '|';

    /** @var string */
    protected $table = null;

    /** @var array */
    protected $columns = [];

    /** @var string */
    protected $primaryKey = null;

    /** @var string */
    protected $totalRows = 0;

    /** @var string */
    protected $chunk = 1000;

    /**
     * AnonymizeMySQLTable constructor.
     * @param $table
     * @param $primaryKey
     */
    public function __construct($table, $primaryKey)
    {
        parent::__construct();

        $this->table = $table;
        $this->primaryKey = $primaryKey;
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $this->printTaskInfo("Anonymizing table $this->table..");

        $db = $this->getConnection();

        $this->totalRows = (int)$db->table($this->table)->count();
        $this->printTaskDebug("$this->totalRows rows for table $this->table");
        $this->startProgressIndicator();
        $this->showProgressIndicator();

        $this->printTaskDebug("Getting data from table $this->table with primary key '$this->primaryKey'");
        try{
            $db->table($this->table)->select($this->primaryKey)->orderBy($this->primaryKey)->chunk($this->chunk, function ($rows) use (&$db) {
                $rowsToArray = $rows->toArray();
                $db->beginTransaction();
                array_walk($rowsToArray, function($row) use ($db){
                    $updates = [];
                    $this->printTaskDebug("Readed row with $this->primaryKey '".$row->{$this->primaryKey}."'");
                    foreach ($this->columns as $column => $formatter) {
                        $updates[$column] = $this->getColumnsFakeData($row, $formatter, $column);
                    }
                    $db->table($this->table)->where($this->primaryKey, $row->{$this->primaryKey})->update($updates);
                    $this->advanceProgressIndicator();
                });
                $db->commit();
            });
        }catch (\Exception $e){
            $db->rollBack();
            return Result::error($this, $e->getMessage(), $e);
        }

        $this->stopProgressIndicator();
        $this->hideProgressIndicator();
        $this->printTaskInfo("Table $this->table anonymized");

        return Result::success($this, 'Fake data generated');
    }

    /**
     * {@inheritdoc}
     */
    public function progressIndicatorSteps()
    {
        return $this->totalRows;
    }

    /**
     * Set field to anonymize
     *
     * @param $column
     * @param $formatter
     */
    public function column($column, $formatter)
    {
        $this->columns[$column] = $formatter;
    }


    /**
     * @param $row
     * @param $formatter
     * @param $column
     * @param $updates
     * @return mixed
     */
    private function getColumnsFakeData($row, $formatter, $column)
    {
        $options = [];
        if (strpos($formatter, '|') === false) {
            $formatterName = $formatter;
        } else {
            $parts = explode(self::OPTIONS_SEPARATOR, $formatter);
            $formatterName = array_shift($parts);
            $options = $parts;
        }

        $this->printTaskDebug("Using formatter $formatter for column $column");

        $taskFaker = $this->taskGenerateFakeData($formatterName);
        if (\in_array("unique", $options)) {
            $taskFaker->unique(true);
        }
        if (\in_array("optional", $options)) {
            $taskFaker->optional(true);
        }
        if (\in_array("randomSuffix", $options)) {
            $taskFaker->randomSuffix(true);
        }

        $result = $taskFaker->run();
        $fakeData = $result->getData()['fakeData'];
        $this->printTaskDebug("Will replace $column with '$fakeData' for row with $this->primaryKey '" . $row->{$this->primaryKey} . "'");
        return $result->getData()['fakeData'];
    }

}
