<?php

namespace Bitbull\Cli\Tasks\Anonymizer;

trait AnonymizerTasks
{

    /**
     * Generate Fake data
     *
     * @param $formatter
     * @return GenerateFakeData
     */
    protected function taskGenerateFakeData($formatter)
    {
        return $this->task(GenerateFakeData::class, $formatter);
    }

    /**
     * Anonymize table
     *
     * @param $table
     * @param $primaryKey
     * @return AnonymizeMySQLTable
     */
    protected function taskAnonymizeMySQLTable($table, $primaryKey)
    {
        return $this->task(AnonymizeMySQLTable::class, $table, $primaryKey);
    }

}