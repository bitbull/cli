<?php

namespace Bitbull\Cli\Tasks\MaxMind;

use MaxMind\Db\Reader\InvalidDatabaseException;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;
use GeoIp2\Database\Reader;

class MaxMindIPInfo extends BaseTask {

    /** @var string */
    protected $ip = null;

    /** @var string */
    protected $type = 'country';

    /** @var string */
    protected $database = '/usr/local/share/GeoIP/GeoIP2-City.mmdb';

    public function __construct($ip)
    {
        parent::__construct();
        $this->ip = $ip;
    }

    /**
     * Get current IP infos
     *
     * Example usage:
     * ```php
     * $result = $this->taskMaxMindIPInfo('000.000.000.000')
     *  ->database('/usr/local/share/GeoIP/GeoLite2-City.mmdb')
     *  ->type('city')
     * $infos = $result->getData()['infos'];
     * ```
     *
     * @return Result
     */
    function run()
    {
        $this->printTaskDebug('Retrieving current commit infos..');

        try {
            $reader = new Reader($this->database);
        } catch (InvalidDatabaseException $e) {
            return Result::error($this, $e->getMessage(), $e);
        }

        $record = $reader->{$this->type}($this->ip);

        return Result::success($this, "Info for ip '$this->ip'", [
            'infos' => $record
        ]);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        return $this->run();
    }
}
