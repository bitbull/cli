<?php

namespace Bitbull\Cli\Tasks\MaxMind;

trait MaxMindTasks
{
    /**
     * Get IP info using MaxMind database
     *
     * @param string $ip
     * @return MaxMindIPInfo
     */
    protected function taskMaxMindIPInfo($ip)
    {
        return $this->task(MaxMindIPInfo::class, $ip);
    }

}