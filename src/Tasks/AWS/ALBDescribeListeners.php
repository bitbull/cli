<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\ElasticLoadBalancingV2\ElasticLoadBalancingV2Client;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Result;

class ALBDescribeListeners extends BaseAWSTask {

    use AWSTasks, UtilsTasks;

    /** @var string */
    protected $loadBalancerArn = null;

    public function __construct($loadBalancerArn)
    {
        parent::__construct();
        $this->loadBalancerArn = $loadBalancerArn;
    }

    /**
     * ALB list listeners
     *
     * Example usage:
     * ```php
     * $result = $this->taskALBDescribeListeners('alb-example')
     *  ->run();
     *
     * $listeners = $result->getData()['listeners'];
     * ```
     *
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        /** @var ElasticLoadBalancingV2Client $client */
        $client = $this->getClient(ElasticLoadBalancingV2Client::class);

        $result = $client->describeListeners([
            'LoadBalancerArn' => $this->loadBalancerArn
        ]);

        return Result::success($this, 'Load balancer listeners', [
            'listeners' => $result['Listeners']
        ]);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        return $this->run();
    }
}
