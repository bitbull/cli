<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\Ecs\EcsClient;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Common\DynamicParams;
use Robo\Common\TaskIO;
use Robo\Contract\SimulatedInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;

class ECSServiceCreate extends BaseAWSTask {

    use AWSTasks, UtilsTasks;

    /** @var string */
    protected $serviceName = null;

    /** @var array */
    protected $declaration = [];

    /** @var string */
    protected $clusterName = null;

    /** @var boolean */
    protected $forceNewDeployment = true;

    public function __construct($serviceName)
    {
        parent::__construct();
        $this->serviceName = $serviceName;
    }

    /**
     * ECS Service create or update
     *
     * Example usage:
     * ```php
     * $result = $this->taskECSServiceCreate('prod-env-example-service', [
     *      // follow https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-ecs-2014-11-13.html#createservice declaration specification
     *  ])
     *  ->run();
     * $service = $result->getData()['service'];
     * ```
     *
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        $this->declaration = array_merge($this->declaration, [
            'cluster' => $this->clusterName,
            'clientToken' => $this->strRandom(32),
        ]);

        if (!isset($this->declaration['taskDefinition'])) {
            $this->declaration['taskDefinition'] = $this->serviceName;
        }

        /** @var EcsClient $client */
        $client = $this->getClient(EcsClient::class);
        $result = $client->describeServices([
            'cluster' => $this->clusterName,
            'services' => [
                $this->serviceName
            ]
        ]);

        if (! count($result['services']) || reset($result['services'])['status'] === 'INACTIVE') {
            $this->printTaskDebug("Service $this->serviceName does not exist or was deleted, creating it..");
            $result = $client->createService(array_merge($this->declaration, [
                'serviceName' => $this->serviceName
            ]));
            $this->printTaskDebug("Service $this->serviceName created!");
            return Result::success($this, "Service '$this->serviceName' created", [
                'service' => $result['service']
            ]);
        } else {
            $this->printTaskDebug("Service $this->serviceName exists, updating it..");
            $result = $client->updateService(array_merge($this->declaration, [
                'service' => $this->serviceName,
                'forceNewDeployment' => $this->forceNewDeployment
            ]));
            $this->printTaskDebug("Service $this->serviceName updated!");
            return Result::success($this, "Service '$this->serviceName' updated", [
                'service' => $result['service']
            ]);
        }
    }

    /**
     * Add target group load balancer connection
     *
     * @param string $targetGroupArn
     * @param string $containerName
     * @param number $containerPort
     */
    public function addLoadBalancerTarget($targetGroupArn, $containerName = null, $containerPort = 80)
    {
        if (isset($this->declaration['loadBalancers']) === false) {
            $this->declaration['loadBalancers'] = [];
        }

        $this->declaration['loadBalancers'][] = [
            'targetGroupArn' => $targetGroupArn,
            'containerPort' => (int) $containerPort,
            'containerName' => $containerName ? $containerName : $this->serviceName
        ];
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        $this->declaration = array_merge($this->declaration, [
            'clusterArn' => "arn:::$this->clusterName",
            'createdAt' => new \DateTime(),
            'createdBy' => $this->strRandom(32),
            'serviceName' => $this->serviceName
        ]);

        return Result::success($this, "Service '$this->serviceName' created", [
            'service' => $this->declaration
        ]);
    }
}
