<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\Ecs\EcsClient;
use Robo\Common\DynamicParams;
use Robo\Common\TaskIO;
use Robo\Contract\SimulatedInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;

class ECSServiceDescribe extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $serviceName = null;

    /** @var string */
    protected $clusterName = null;

    public function __construct($serviceName)
    {
        parent::__construct();
        $this->serviceName = $serviceName;
    }

    /**
     * ECS Service describe
     *
     * Example usage:
     * ```php
     * $result = $this->taskECSServiceDescribe('prod-env-example-service')
     *  ->run();
     * $taskCount = $result->getData()['runningCount'];
     * ```
     *
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        /** @var EcsClient $client */
        $client = $this->getClient(EcsClient::class);
        $result = $client->describeServices([
            'cluster' => $this->clusterName,
            'services' => [
                $this->serviceName
            ]
        ]);


        if (count($result['services']) === 0) {
            return Result::error($this, "Service '$this->serviceName' not found");
        }

        $service = $result['services'][0];

        return Result::success($this, "Service '$this->serviceName'", [
            'name' => $service['serviceName'],
            'status' => $service['status'],
            'pendingCount' => $service['pendingCount'],
            'runningCount' => $service['runningCount'],
            'desiredCount' => $service['desiredCount'],
            'deployments' => $service['deployments']
        ]);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        return $this->run();
    }
}
