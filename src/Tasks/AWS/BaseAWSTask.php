<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\AwsClientInterface;
use Bitbull\Cli\Tasks\BaseTask;

abstract class BaseAWSTask extends BaseTask {

    use AWSTasks;

    /** @var string */
    protected $region = null;

    /** @var string */
    protected $profile = null;

    /**
     * @param $className
     * @return AwsClientInterface
     */
    protected function getClient($className)
    {
        if ($this->region === null) {
            $envValue = getenv('AWS_DEFAULT_REGION');
            if ($envValue !== false) {
                $this->region = $envValue;
            } else {
                $this->region = 'us-west-1';
            }
        }

        if ($this->profile) {
            $assumeRoleTask = $this->taskAssumeRole();
            $assumeRoleTask->profile($this->profile);
            $assumeRoleTask->region($this->region);

            $result = $assumeRoleTask->run();

            $client = new $className([
                'region' => $this->region,
                'version' => 'latest',
                'credentials' => $result->getData()['credentials'],
            ]);
        } else {
            $client = new $className([
                'region' => $this->region,
                'version' => 'latest',
            ]);
        }

        return $client;
    }

    function region($region = null)
    {
        if ($region == null || $region == '') {
            return $this;
        }
        $this->region = $region;
        return $this;
    }

    function profile($profile = null)
    {
        if ($profile == null || $profile == '') {
            return $this;
        }

        $this->profile = $profile;

        return $this;
    }
}
