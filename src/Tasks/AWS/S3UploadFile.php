<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\S3\Exception\S3Exception;
use Robo\Common\DynamicParams;
use Robo\Contract\TaskInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;
use Symfony\Component\Process\Process;
use Aws\S3\S3Client;

class S3UploadFile extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $from = null;

    /** @var string */
    protected $to = null;

    /** @var string */
    protected $public = false;

    /**
     * Download remote file
     *
     * Example usage:
     * ```php
     * $this->taskUpload()
     *      ->from('/tmp/archive.zip')
     * 		->to('s3://example-bucket/archive.zip')
     * ->run();
     * ```
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        $client = $this->getClient(S3Client::class);
        $bucket = $this->getBucketName();
        $keyname = $this->getKeyName();

        try {
            $this->printTaskDebug("Uploading $this->from to s3://$bucket/$keyname..");
            $client->putObject(array(
                'Bucket'       => $bucket,
                'Key'          => $keyname,
                'SourceFile'   => $this->from,
                'ACL'          => $this->public ? 'public-read' : 'private'
            ));

        } catch (S3Exception $e) {
            return Result::error(
                $this,
                "Error uploading to S3: ".$e->getMessage()
            );
        }

        return Result::success($this);
    }

    /**
     * Get bucket name
     *
     * @return string
     */
    protected function getBucketName()
    {
        return parse_url($this->to,  PHP_URL_HOST);
    }

    /**
     * Get file key name
     *
     * @return string
     */
    protected function getKeyName()
    {
        return substr(parse_url($this->to,  PHP_URL_PATH), 1);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        $bucket = $this->getBucketName();
        $keyname = $this->getKeyName();
        $this->printTaskDebug("Uploading $this->from to s3://$bucket/$keyname..");
        return Result::success($this);
    }

}
