<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\AutoScaling\AutoScalingClient;
use Robo\Common\DynamicParams;
use Robo\Common\TaskIO;
use Robo\Contract\SimulatedInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;

class ASGDescribe extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $name = null;

    public function __construct($name)
    {
        parent::__construct();
        $this->name = $name;
    }

    /**
     * AutoScaling Describe
     *
     * Example usage:
     * ```php
     * $result = $this->taskASGDescribe('myautoscaling')
     *  ->run();
     * $minSize = $result->getData()['minSize'];
     * ```
     *
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        $client = $this->getClient(AutoScalingClient::class);
        $result = $client->describeAutoScalingGroups([
            'AutoScalingGroupNames' => [$this->name],
            'MaxRecords' => 1,
        ])['AutoScalingGroups'];


        if (count($result) == 0) {
            return Result::error($this, "AutoScaling group '$this->name' not found");
        }

        $infos = $result[0];

        $healthyInstancesCount = 0;
        $inServiceInstancesCount = 0;
        $terminatingInstancesCount = 0;
        foreach ($infos['Instances'] as $instance) {
            if ($instance['HealthStatus'] == 'Healthy') {
                $healthyInstancesCount++;
            }
            if ($instance['LifecycleState'] == 'InService') {
                $inServiceInstancesCount++;
            }
            if ($instance['LifecycleState'] == 'Terminating') {
                $terminatingInstancesCount++;
            }
        }

        return Result::success($this, "AutoScaling group '$this->name'", [
            'minSize' => $infos['MinSize'],
            'maxSize' => $infos['MaxSize'],
            'desiredCapacity' => $infos['DesiredCapacity'],
            'instances' => $infos['Instances'],
            'healthyInstancesCount' => $healthyInstancesCount,
            'inServiceInstancesCount' => $inServiceInstancesCount,
            'terminatingInstancesCount' => $terminatingInstancesCount
        ]);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        return $this->run();
    }
}
