<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\AutoScaling\AutoScalingClient;
use Robo\Common\DynamicParams;
use Robo\Common\TaskIO;
use Robo\LoadAllTasks;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;

class ASGScaleUp extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $name = null;

    /** @var bool */
    protected $wait = false;

    /** @var bool */
    protected $forceMax = false;

    /** @var int */
    protected $add = 1;

    public function __construct($name, $add = 1)
    {
        parent::__construct();
        $this->name = $name;
        $this->add = $add;
    }

    /**
     * AutoScaling scale up
     *
     * Example usage:
     * ```php
     * $this->taskASGScaleUp('myautoscaling', 1)
     *      ->waitUntilComplete()
     * ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $infos = $this->getASGInfo();

        $currentDesideredCapacity = intval($infos['desiredCapacity']);
        $this->printTaskDebug("Current desiredCapacity is '$currentDesideredCapacity'");

        $desideredCapacity = $currentDesideredCapacity + $this->add;
        $this->printTaskDebug("Scale up AutoScalingGroup '$this->name' to '$desideredCapacity'");
        $maxSize = intval($infos['maxSize']);

        if ($maxSize < $desideredCapacity) {
            $maxSize = $desideredCapacity;
            $this->printTaskDebug("Forced maxSize to $maxSize");
        }

        $editTask = $this->taskASGEdit($this->name)
            ->desiredCapacity($desideredCapacity)
            ->profile($this->profile)
            ->region($this->region);

        if ($this->forceMax) {
            $editTask->maxSize($maxSize);
        }
        $editTask->run();

        if ($this->wait) {
            while (true) {
                $infos = $this->getASGInfo();
                $this->printTaskInfo('Waiting for '.$desideredCapacity.' currently '.$infos['inServiceInstancesCount'].' instances in service..');
                if ($infos['inServiceInstancesCount'] == $desideredCapacity) {
                    $this->printTaskDebug("$desideredCapacity instances in service, finish wait");
                    break;
                }
                sleep(5);
            }
        }

        return Result::success($this);
    }

    /**
     * Set wait to true
     */
    public function waitUntilComplete()
    {
        $this->wait = true;
    }

    /**
     * Get AutoScalingGroup data
     *
     * @return array
     */
    protected function getASGInfo()
    {
        return $this->taskASGDescribe($this->name)
            ->profile($this->profile)
            ->region($this->region)
            ->run()
            ->getData();
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        // Disable wait, taskASGEdit will not apply the changes
        $this->wait = false;

        return $this->run();
    }
}
