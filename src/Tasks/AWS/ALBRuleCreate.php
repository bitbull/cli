<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\ElasticLoadBalancingV2\ElasticLoadBalancingV2Client;
use Aws\ElasticLoadBalancingV2\Exception\ElasticLoadBalancingV2Exception;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Result;

class ALBRuleCreate extends BaseAWSTask {

    use AWSTasks, UtilsTasks;

    /** @var string */
    protected $listenerArn = null;

    /** @var string */
    protected $targetGroupArn = null;

    /** @var string */
    protected $conditions = [];

    /** @var string */
    protected $ruleIdentifier = 'listener'; // 'listener' or 'priority'

    /** @var number */
    protected $priority = null;

    public function __construct($listenerArn, $priority = null)
    {
        parent::__construct();
        $this->listenerArn = $listenerArn;
        if ($priority !== null) {
            $this->priority = (int) $priority;
        }
    }

    /**
     * ALB rule create or update
     *
     * Example usage:
     * ```php
     * $result = $this->taskALBRuleCreate('alb-example', [
     *
     *  ])
     *  ->run();
     * ```
     *
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        /** @var ElasticLoadBalancingV2Client $client */
        $client = $this->getClient(ElasticLoadBalancingV2Client::class);
        $result = $client->describeRules([
            'ListenerArn' => $this->listenerArn
        ]);
        $rules = $result['Rules'];
        $machedRules = [];
        if ($this->ruleIdentifier === 'listener') {
            $targetGroupArn = $this->targetGroupArn;
            $machedRules = array_filter($rules, static function ($rule) use ($targetGroupArn) {
                if ($rule['IsDefault'] === true) {
                    return false;
                }
                foreach ($rule['Actions'] as $action) {
                    if ($action['TargetGroupArn'] === $targetGroupArn) {
                        return true;
                    }
                }
                return false;
            });
            usort($machedRules, static function ($ruleA, $ruleB) {
                return ($ruleA['Priority'] < $ruleB['Priority']) ? 1 : -1;
            });
        } else if ($this->ruleIdentifier === 'priority') {
            if ($this->priority === null) {
                return Result::error($this, 'Priority is required when using rule identifier "priority"');
            }
            $priority = $this->priority;
            $machedRules = array_filter($rules, static function ($rule) use ($priority) {
                if ($rule['IsDefault'] === true) {
                    return false;
                }
                return ((int) $rule['Priority']) === ($priority + 1);
            });
        } else {
            return Result::error($this, 'Rule identifier "' . $this->ruleIdentifier . '" not recognized');
        }

        $foundRule = null;
        $count = count($machedRules);
        if ($count > 0) {
            if ($count > 1) {
                $this->printTaskWarning("Found $count rules connected to the same targetGroupArn, will be selected the lowest priority");
            }
            $foundRule = reset($machedRules);
        }
        $this->printTaskDebug('Found ' . count($rules) . ' rules, ' . count($machedRules) . ' matched');

        if ($foundRule === null) {
            $this->printTaskDebug('No rule that match targetGroupArn found, creating it..');
            $priority = count($rules) + 1;
            $ruleCreated = false;
            do{
                try {
                    $client->createRule([
                        'ListenerArn' => $this->listenerArn,
                        'Actions' => [
                            [
                                'TargetGroupArn' => $this->targetGroupArn,
                                'Type' => 'forward'
                            ]
                        ],
                        'Conditions' => $this->conditions,
                        'Priority' => $priority++
                    ]);
                    $ruleCreated = true;
                }catch (ElasticLoadBalancingV2Exception $e) {
                    if ($e->getAwsErrorCode() === 'PriorityInUse'){
                        $this->printTaskDebug('Priority '.$priority.' is already in use, incrementing it..');
                        sleep(1); // slow down for avoiding AWS API throttling system
                        continue;
                    }
                    return Result::error($this, $e->getMessage());
                }
            } while ($ruleCreated === false);

            $this->printTaskDebug('Rule created!');
            return Result::success($this, "Rule created for listener '$this->listenerArn'", [
                'rule' => $foundRule
            ]);
        }

        // Too hard to find difference in rules, directly update on every call

        $this->printTaskDebug("Updating rule '$this->targetGroupArn'..");
        $result = $client->modifyRule([
            'RuleArn' => $foundRule['RuleArn'],
            'Actions' => [
                [
                    'TargetGroupArn' => $this->targetGroupArn,
                    'Type' => 'forward'
                ]
            ],
            'Conditions' => $this->conditions
        ]);
        $this->printTaskDebug('Rule updated!');
        return Result::success($this, "Rule updated for listener '$this->listenerArn'", [
            'rule' => $result['Rules'][0]
        ]);
    }

    /**
     * @param $paths array
     */
    public function addPathsCondition($paths)
    {
        $this->conditions[] = [
            'Field' => 'path-pattern',
            'Values' => $paths,
        ];
    }

    /**
     * @param $hosts array
     */
    public function addHostCondition($hosts)
    {
        $this->conditions[] = [
            'Field' => 'host-header',
            'Values' => $hosts,
        ];
    }

    /**
     * @param $methods array
     */
    public function addMethodCondition($methods)
    {
        $this->conditions[] = [
            'Field' => 'http-request-method',
            'Values' => $methods,
        ];
    }

    /**
     * @param $ips array
     */
    public function addSourceIpCondition($ips)
    {
        $this->conditions[] = [
            'Field' => 'source-ip',
            'Values' => $ips,
        ];
    }

    /**
     * @param $queryStrings array
     */
    public function addQueryStringCondition($queryStrings)
    {
        $this->conditions[] = [
            'Field' => 'query-string',
            'Values' => $queryStrings,
        ];
    }

    /**
     * @param $headers array
     */
    public function addHeaderCondition($headers)
    {
        $this->conditions[] = [
            'Field' => 'http-header',
            'Values' => $headers,
        ];
    }

    /**
     * @param $identifier string
     */
    public function useRuleIdentifier($identifier)
    {
        $this->ruleIdentifier = $identifier;
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        return Result::success($this, "Rule updated for listener '$this->listenerArn'", [
            'rule' => [
                'Actions' => [],
                'Conditions' => [],
                'SourceIpConfig' => [],
                'IsDefault' => false,
                'Priority' => 1,
                'RuleArn' => 'arn::',
            ]
        ]);
    }
}
