<?php

namespace Bitbull\Cli\Tasks\AWS;

trait AWSTasks
{
    /**
     * Assume role by profile name
     *
     * @return AssumeRole
     */
    protected function taskAssumeRole()
    {
        return $this->task(AssumeRole::class);
    }

    /**
     * Invalidate CloudFront CDN
     *
     * @param $distribution string
     * @return CloudFrontInvalidation
     */
    protected function taskCloudFrontInvalidation($distribution)
    {
        return $this->task(CloudFrontInvalidation::class, $distribution);
    }

    /**
     * Deploy using CodeDeploy
     *
     * @return CodeDeploy
     */
    protected function taskCodeDeploy()
    {
        return $this->task(CodeDeploy::class);
    }

    /**
     * Upload file to S3 bucket
     *
     * @return S3UploadFile
     */
    protected function taskS3UploadFile()
    {
        return $this->task(S3UploadFile::class);
    }

    /**
     * Download file from S3 bucket
     *
     * @return S3DownloadFile
     */
    protected function taskS3DownloadFile()
    {
        return $this->task(S3DownloadFile::class);
    }

    /**
     * Upload directory to S3 bucket
     *
     * @return S3UploadDirectory
     */
    protected function taskS3UploadDirectory()
    {
        return $this->task(S3UploadDirectory::class);
    }

    /**
     * Download directory from S3 bucket
     *
     * @return S3DownloadDirectory
     */
    protected function taskS3DownloadDirectory()
    {
        return $this->task(S3DownloadDirectory::class);
    }

    /**
     * Create Presigned URL
     *
     * @return S3PresignedURL
     */
    protected function taskS3PresignedURL($path)
    {
        return $this->task(S3PresignedURL::class, $path);
    }

    /**
     * Get AutoScaling infos
     *
     * @param $name string
     * @return ASGScaleUp
     */
    protected function taskASGDescribe($name)
    {
        return $this->task(ASGDescribe::class, $name);
    }

    /**
     * AutoScaling scale up
     *
     * @param $name string
     * @param $add int
     * @return ASGScaleUp
     */
    protected function taskASGScaleUp($name, $add = 1)
    {
        return $this->task(ASGScaleUp::class, $name, $add);
    }

    /**
     * AutoScaling scale down
     *
     * @param $name string
     * @param $remove int
     * @return ASGScaleDown
     */
    protected function taskASGScaleDown($name, $remove = 1)
    {
        return $this->task(ASGScaleDown::class, $name, $remove);
    }

    /**
     * Edit AutoScaling
     *
     * @param $name string
     * @return ASGScaleUp
     */
    protected function taskASGEdit($name)
    {
        return $this->task(ASGEdit::class, $name);
    }

    /**
     * Publish message to SNS topic
     *
     * @param $topicARN string
     * @return ASGScaleUp
     */
    protected function taskSNSPublish($topicARN)
    {
        return $this->task(SNSPublish::class, $topicARN);
    }

    /**
     * Describe SecurityGroup
     *
     * @param $securityGroupId string
     * @return SGDescribe
     */
    protected function taskSGDescribe($securityGroupId)
    {
        return $this->task(SGDescribe::class, $securityGroupId);
    }

    /**
     * Edit SecurityGroup
     *
     * @param $securityGroupId string
     * @return SGEdit
     */
    protected function taskSGEdit($securityGroupId)
    {
        return $this->task(SGEdit::class, $securityGroupId);
    }

    /**
     * Execute SSM command
     *
     * @param $documentName string
     * @return SSMExecuteCommand
     */
    protected function taskSSMExecuteCommand($documentName)
    {
        return $this->task(SSMExecuteCommand::class, $documentName);
    }

    /**
     * Wait SSM command to finish
     *
     * @param $commandId string
     * @return SSMDescribeCommand
     */
    protected function taskSSMDescribeCommand($commandId)
    {
        return $this->task(SSMDescribeCommand::class, $commandId);
    }

    /**
     * Describe ECS service
     *
     * @param $serviceName string
     * @return ECSServiceDescribe
     */
    protected function taskECSServiceDescribe($serviceName)
    {
        return $this->task(ECSServiceDescribe::class, $serviceName);
    }

    /**
     * Create ECS service
     *
     * @param $serviceName string
     * @return ECSServiceCreate
     */
    protected function taskECSServiceCreate($serviceName)
    {
        return $this->task(ECSServiceCreate::class, $serviceName);
    }

    /**
     * Create ALB target rule
     *
     * @param $loadBalancerArn string
     * @return ALBCreateTargetGroup
     */
    protected function taskALBCreateTargetGroup($loadBalancerArn)
    {
        return $this->task(ALBCreateTargetGroup::class, $loadBalancerArn);
    }

    /**
     * Describe ALB target rule
     *
     * @param $name string
     * @return ALBDescribeTargetGroup
     */
    protected function taskALBDescribeTargetGroup($name)
    {
        return $this->task(ALBDescribeTargetGroup::class, $name);
    }

    /**
     * Create ALB rules for ECS service
     *
     * @param $listenerArn string
     * @param $priority string
     * @return ALBRuleCreate
     */
    protected function taskALBRuleCreate($listenerArn, $priority = null)
    {
        return $this->task(ALBRuleCreate::class, $listenerArn, $priority);
    }

    /**
     * List load balancer listeners
     *
     * @param $loadBalancerArn string
     * @return ALBDescribeListeners
     */
    protected function taskALBDescribeListeners($loadBalancerArn)
    {
        return $this->task(ALBDescribeListeners::class, $loadBalancerArn);
    }

}
