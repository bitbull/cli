<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\Credentials\AssumeRoleCredentialProvider;
use Aws\Sts\StsClient;
use Bitbull\Cli\Runner;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Result;

class AssumeRole extends BaseAWSTask {

    use UtilsTasks;

    /** @var string */
    protected $credentialFile = null;

    public function __construct($credentialFile = null)
    {
        parent::__construct();

        $this->credentialFile = $credentialFile ?: ($this->getHomeDir() . '/.aws/credentials');
    }

    /**
     * Assume role from profile name
     *
     * Example usage:
     * ```php
     * $this->taskAssumeRole()
     * 		->profile('myprofile')
     * ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $profile = $this->profile ?: (getenv('AWS_PROFILE') ?: 'default');

        if (!$this->_exist($this->credentialFile)) {
            return Result::error($this, "Unable to find '$profile', credentials file '$this->credentialFile' does not exist");
        }

        $data = parse_ini_file($this->credentialFile, true);

        if (!isset($data[$profile])) {
            return Result::error($this, "Profile '$profile' not found in credentials file");
        }

        if (!isset($data[$profile]['role_arn'])) {
            $this->printTaskDebug("$profile has no role_arn set, trying to find access keys..");

            if (isset($data[$profile]['aws_access_key_id']) && isset($data[$profile]['aws_secret_access_key'])){
                $this->printTaskDebug("Using '".$data[$profile]['aws_access_key_id']."'' access key");
                return Result::success($this, "Loaded credentials from '$profile'", [
                    'credentials' => [
                        'key'    => $data[$profile]['aws_access_key_id'],
                        'secret' => $data[$profile]['aws_secret_access_key'],
                    ]
                ]);
            }

            return Result::error($this, "No access keys or role_arn set for profile '$profile'");
        }
        $roleArn = $data[$profile]['role_arn'];

        if (!isset($data[$profile]['source_profile'])) {
            return Result::error($this, "No source_profile set for profile '$profile'");
        }
        $sourceProfile = $data[$profile]['source_profile'];

        if (!isset($data[$sourceProfile])){
            return Result::error($this, "Source profile '$sourceProfile' not found in credentials file");
        }
        if (!isset($data[$sourceProfile]['aws_access_key_id']) || !isset($data[$sourceProfile]['aws_secret_access_key'])){
            return Result::error($this, "Source profile '$sourceProfile' does not have credentials keys");
        }

        $this->printTaskDebug("Assuming role '$roleArn' from profile '$sourceProfile'");
        $assumeRoleCredentials = new AssumeRoleCredentialProvider([
            'client' => new StsClient([
                'region' => $this->region,
                'version' => 'latest',
                'credentials' => [
                    'key'    => $data[$sourceProfile]['aws_access_key_id'],
                    'secret' => $data[$sourceProfile]['aws_secret_access_key']
                ]
            ]),
            'assume_role_params' => [
                'RoleArn' => $roleArn,
                'RoleSessionName' => strtolower(str_replace(' ', '-', Runner::APPLICATION_NAME))
            ]
        ]);

        return Result::success($this, "Assumed role '$roleArn' from '$profile'", [
            'credentials' => $assumeRoleCredentials
        ]);
    }

    /**
     * @return string
     */
    protected function getHomeDir()
    {
        // On Linux/Unix-like systems, use the HOME environment variable
        if ($homeDir = getenv('HOME')) {
            return $homeDir;
        }

        // Get the HOMEDRIVE and HOMEPATH values for Windows hosts
        $homeDrive = getenv('HOMEDRIVE');
        $homePath = getenv('HOMEPATH');

        return ($homeDrive && $homePath) ? $homeDrive . $homePath : null;
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        return $this->run();
    }
}
