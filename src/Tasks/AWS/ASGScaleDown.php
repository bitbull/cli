<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\AutoScaling\AutoScalingClient;
use Robo\Common\DynamicParams;
use Robo\Common\TaskIO;
use Robo\LoadAllTasks;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;

class ASGScaleDown extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $name = null;

    /** @var bool */
    protected $wait = false;

    /** @var bool */
    protected $forceMin = false;

    /** @var int */
    protected $remove = 1;

    public function __construct($name, $remove = 1)
    {
        parent::__construct();
        $this->name = $name;
        $this->remove = $remove;
    }

    /**
     * AutoScaling scale down
     *
     * Example usage:
     * ```php
     * $this->taskASGScaleDown('myautoscaling', 1)
     *      ->waitUntilComplete()
     * ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $infos = $this->getASGInfo();

        $currentDesideredCapacity = intval($infos['desiredCapacity']);
        $this->printTaskDebug("Current desiredCapacity is '$currentDesideredCapacity'");

        $desideredCapacity = $currentDesideredCapacity - $this->remove;
        $this->printTaskDebug("Scale down AutoScalingGroup '$this->name' to '$desideredCapacity'");

        if ($desideredCapacity == 0) {
            return Result::error($this, 'Set desiredCapacity to 0 causes service interruption, operation cancelled');
        }

        $minSize = intval($infos['maxSize']);

        if ($this->forceMin && $minSize > $desideredCapacity) {
            $minSize = $desideredCapacity;
            $this->printTaskDebug("Forced minSize to $minSize");
        }

        $editTask = $this->taskASGEdit($this->name)
            ->desiredCapacity($desideredCapacity)
            ->profile($this->profile)
            ->region($this->region);

        if ($this->forceMin) {
            $editTask->minSize($minSize);
        }
        $editTask->run();

        if ($this->wait) {
            while (true) {
                $infos = $this->getASGInfo();
                $this->printTaskInfo('Waiting for '.$desideredCapacity.' currently '.$infos['inServiceInstancesCount'].' instances in service..');
                if ($infos['inServiceInstancesCount'] == $desideredCapacity) {
                    $this->printTaskDebug("$desideredCapacity instances in service, finish wait");
                    break;
                }
                sleep(5);
            }
            while (true) {
                $infos = $this->getASGInfo();
                $this->printTaskInfo('Waiting for all instances to be fully terminated, '.$infos['terminatingInstancesCount'].' instances in terminating state..');
                if ($infos['terminatingInstancesCount'] == 0) {
                    $this->printTaskDebug("All instances terminated, finish wait");
                    break;
                }
                sleep(5);
            }
        }

        return Result::success($this);
    }

    /**
     * Set wait to true
     */
    public function waitUntilComplete()
    {
        $this->wait = true;
    }

    /**
     * Get AutoScalingGroup data
     *
     * @return array
     */
    protected function getASGInfo()
    {
        return $this->taskASGDescribe($this->name)
            ->profile($this->profile)
            ->region($this->region)
            ->run()
            ->getData();
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        // Disable wait, taskASGEdit will not apply the changes
        $this->wait = false;

        return $this->run();
    }
}
