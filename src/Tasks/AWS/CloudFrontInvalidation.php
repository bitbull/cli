<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\CloudFront\CloudFrontClient;
use Robo\Common\DynamicParams;
use Robo\LoadAllTasks;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;

class CloudFrontInvalidation extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $id = null;

    /** @var string */
    protected $paths = null;

    public function __construct($distribution)
    {
        parent::__construct();
        $this->id = $distribution;
    }

    /**
     * Download remote file
     *
     * Example usage:
     * ```php
     * $this->taskCloudFrontInvalidation()
     * 		->id('d111111abcdef8')
     * 		->paths([
     *          '/media/*'
     *      ])
     * ->run();
     * ```
     *
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        $client = $this->getClient(CloudFrontClient::class);
        try {
            $this->printTaskDebug("Creating invalidation '$this->id' with paths: ".json_encode($this->paths));
            $client->createInvalidation([
                'DistributionId' => $this->id,
                'InvalidationBatch' => [
                    'CallerReference' => date("Y-m-d H:i:0",time()), // one per minute
                    'Paths' => [
                        'Items' => $this->paths,
                        'Quantity' => 1,
                    ]
                ]
            ]);

        } catch (\Exception $e) {
            return Result::error(
                $this,
                "Error creating invalidation: ".$e->getMessage()
            );
        }

        return Result::success($this);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        $this->printTaskDebug("Creating invalidation for distribution '$this->id' with paths: ".json_encode($this->paths));
        return Result::success($this);
    }
}
