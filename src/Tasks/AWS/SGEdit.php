<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\Ec2\Ec2Client;
use Robo\Common\DynamicParams;
use Robo\Common\TaskIO;
use Robo\Contract\SimulatedInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;

class SGEdit extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $id = null;

    /** @var array */
    protected $ingressRules = [];

    /** @var array */
    protected $egressRules = [];

    public function __construct($id)
    {
        parent::__construct();
        $this->id = $id;
    }

    /**
     * Security Group Edit
     *
     * Example usage:
     * ```php
     * $result = $this->taskSGEdit('mysecuritygroup')
     *  ->addIngress([
     *      'type' => 'ipv4',
     *      'protocol' => 'tcp',
     *      'fromPort' => 123,
     *      'toPort' => 123,
     *      'value' => '0.0.0.0/0',
     *      'description' => 'My new rule'
     *  ]);
     *  ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $client = $this->getClient(Ec2Client::class);

        if (sizeof($this->ingressRules) == 0 && sizeof($this->egressRules) == 0){
            $this->printTaskInfo('No rules to import');
            return Result::success($this, "SecurityGroup '$this->id' untouched");
        }

        if (sizeof($this->ingressRules) > 0){
            $this->printTaskInfo(sizeof($this->ingressRules)." ingress rules to import");
        }
        foreach ($this->ingressRules as $ingressRules) {
            $this->printTaskDebug("Adding ingress rules ".$ingressRules['protocol']." for ".$ingressRules['value']." from port ".$ingressRules['fromPort']." to port ".$ingressRules['toPort']);
            $ipPermission = [
                'FromPort' => $ingressRules['fromPort'],
                'IpProtocol' => $ingressRules['protocol'],
                'ToPort' => $ingressRules['toPort'],
            ];
            if($ingressRules['type'] == 'ipv4'){
                $ipPermission['IpRanges'] = [
                    [
                        'CidrIp' => $ingressRules['value'],
                        'Description' => isset($ingressRules['description']) ? $ingressRules['description'] : ''
                    ]
                ];
            }else{
                $ipPermission['Ipv6Ranges'] = [
                    [
                        'CidrIpv6' => $ingressRules['value'],
                        'Description' => isset($ingressRules['description']) ? $ingressRules['description'] : ''
                    ]
                ];
            }
            $client->authorizeSecurityGroupIngress([
                'GroupId' => $this->id,
                'IpPermissions' => [
                    $ipPermission
                ],
            ]);
        }

        if (sizeof($this->egressRules) > 0){
            $this->printTaskInfo(sizeof($this->egressRules)." egress rules to import");
        }
        foreach ($this->egressRules as $egressRules) {
            $this->printTaskDebug("Adding egress rules ".$egressRules['protocol']." for ".$egressRules['value']." from port ".$egressRules['fromPort']." to port ".$egressRules['toPort']);
            $ipPermission = [
                'FromPort' => $egressRules['fromPort'],
                'IpProtocol' => $egressRules['protocol'],
                'ToPort' => $egressRules['toPort'],
            ];
            if($egressRules['type'] == 'ipv4'){
                $ipPermission['IpRanges'] = [
                    [
                        'CidrIp' => $egressRules['value'],
                        'Description' => isset($egressRules['description']) ? $egressRules['description'] : ''
                    ]
                ];
            }else{
                $ipPermission['Ipv6Ranges'] = [
                    [
                        'CidrIpv6' => $egressRules['value'],
                        'Description' => isset($egressRules['description']) ? $egressRules['description'] : ''
                    ]
                ];
            }
            $client->authorizeSecurityGroupEgress([
                'GroupId' => $this->id,
                'IpPermissions' => [
                    $ipPermission
                ],
            ]);
        }

        return Result::success($this, "SecurityGroup '$this->id' edited");
    }

    /**
     * Add ingress rule
     *
     * @param $rule
     */
    public function addIngress($rule)
    {
        array_push($this->ingressRules, $rule);
    }

    /**
     * Add egress rule
     *
     * @param $rule
     */
    public function addEgress($rule)
    {
        array_push($this->egressRules, $rule);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        if (sizeof($this->ingressRules) == 0 && sizeof($this->ingressRules) == 0){
            $this->printTaskInfo('No rules to import');
            return Result::success($this, "SecurityGroup '$this->id' untouched");
        }

        if (sizeof($this->ingressRules) > 0){
            $this->printTaskInfo(sizeof($this->ingressRules)." ingress rules to import");
        }
        foreach ($this->ingressRules as $ingressRules) {
            $this->printTaskDebug("Adding ingress rules ".$ingressRules['protocol']." for ".$ingressRules['value']." from port ".$ingressRules['fromPort']." to port ".$ingressRules['toPort']);
        }

        if (sizeof($this->ingressRules) > 0){
            $this->printTaskInfo(sizeof($this->egressRules)." egress rules to import");
        }
        foreach ($this->egressRules as $egressRules) {
            $this->printTaskDebug("Adding egress rules ".$egressRules['protocol']." for ".$egressRules['value']." from port ".$egressRules['fromPort']." to port ".$egressRules['toPort']);
        }

        return Result::success($this, "SecurityGroup '$this->id' edited");
    }

}
