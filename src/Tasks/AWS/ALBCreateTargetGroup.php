<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\ElasticLoadBalancingV2\ElasticLoadBalancingV2Client;
use Aws\ElasticLoadBalancingV2\Exception\ElasticLoadBalancingV2Exception;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Result;

class ALBCreateTargetGroup extends BaseAWSTask {

    use AWSTasks, UtilsTasks;

    /** @var string */
    protected $loadBalancerArn = null;

    /** @var string */
    protected $vpcId = null;

    /** @var string */
    protected $name = null;

    /** @var string */
    protected $port = '80';

    /** @var string */
    protected $protocol = 'HTTP';

    /** @var string */
    protected $healthCheckPath = '/';

    /** @var string stickiness in seconds */
    protected $stickiness = null;

    public function __construct($loadBalancerArn)
    {
        parent::__construct();
        $this->loadBalancerArn = $loadBalancerArn;
    }

    /**
     * Create target group
     *
     * Example usage:
     * ```php
     * $result = $this->taskALBCreateTargetGroup('arn::loadBalancerArn')
     *    ->name('target-group-name')
     *    ->port(80)
     *    ->stickiness(null)
     *    ->run();
     *  $targetGroupArn = $result->getData()['arn'];
     * ```
     *
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        /** @var ElasticLoadBalancingV2Client $client */
        $client = $this->getClient(ElasticLoadBalancingV2Client::class);

        if ($this->vpcId === null && $this->loadBalancerArn === null) {
            return Result::error($this, 'No vpcId or loadBalancerArn set');
        }

        if ($this->vpcId === null) {
            $loadBalancerResult = $client->describeLoadBalancers([
                'LoadBalancerArns' => [
                    $this->loadBalancerArn
                ]
            ]);

            if (count($loadBalancerResult['LoadBalancers']) === 0) {
                return Result::error($this, "Load balancer '$this->loadBalancerArn' not found");
            }

            $loadBalancer = $loadBalancerResult['LoadBalancers'][0];

            $this->printTaskDebug('Found load balancer '.$loadBalancer['LoadBalancerArn']);
            $this->vpcId = $loadBalancer['VpcId'];
        }

        $this->printTaskDebug('Loading target group..');
        try {
            $result = $client->describeTargetGroups([
                'Names' => [$this->name]
            ]);
            $this->printTaskDebug('Target group found, updating it..');
            $client->modifyTargetGroup([
                'TargetGroupArn' => $result['TargetGroups'][0]['TargetGroupArn'],
                'Name' => $this->name,
                'Port' => $this->port,
                'Protocol' => $this->protocol,
                'HealthCheckPath' => $this->healthCheckPath,
                'VpcId' => $this->vpcId
            ]);
            $this->printTaskDebug('Target group updated!');
        } catch (ElasticLoadBalancingV2Exception $e) {
            if ($e->getAwsErrorCode() === 'TargetGroupNotFound'){
                $this->printTaskDebug('Target group not found, creating it..');
                $result = $client->createTargetGroup([
                    'Name' => $this->name,
                    'Port' => $this->port,
                    'Protocol' => $this->protocol,
                    'HealthCheckPath' => $this->healthCheckPath,
                    'VpcId' => $this->vpcId
                ]);
                $this->printTaskDebug('Target group created!');
            } else {
                return Result::error($this, $e->getMessage());
            }
        }
        $targetGroup = $result['TargetGroups'][0];

        if ($this->stickiness !== null) {
            $result = $client->describeTargetGroupAttributes([
                'TargetGroupArn' => $targetGroup['TargetGroupArn'],
            ]);

            $client->modifyTargetGroupAttributes([
                'Attributes' => array_replace_recursive($result['Attributes'], [
                    [
                        'Key' => 'stickiness.enabled',
                        'Value' => 'true',
                    ],
                    [
                        'Key' => 'stickiness.type',
                        'Value' => 'lb_cookie',
                    ],
                    [
                        'Key' => 'stickiness.lb_cookie.duration_seconds',
                        'Value' => $this->stickiness,
                    ],
                ]),
                'TargetGroupArn' => $targetGroup['TargetGroupArn'],
            ]);
        }

        return Result::success($this, 'Created target group', [
            'arn' => $targetGroup['TargetGroupArn']
        ]);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        return Result::success($this, 'Created target group', [
            'arn' => 'arn::'
        ]);
    }
}
