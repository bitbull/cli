<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\Ssm\SsmClient;
use Robo\Result;
class SSMDescribeCommand extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $commandId = null;

    /** @var string */
    protected $wait = false;

    /** @var string */
    protected $throttle = 3;

    public function __construct($commandId)
    {
        parent::__construct();
        $this->commandId = $commandId;
    }

    /**
     * Describe command and wait until is completed
     *
     * Example usage:
     * ```php
     * $result = $this->taskSSMDescribeCommand('xxxxxxx')
     *   ->run();
     * $result->getData()['invocations']
     * ```
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        $client = $this->getClient(SsmClient::class);
        $this->printTaskDebug("Retrieving command '$this->commandId'..");

        $commandIsEnd = false;
        $response = null;
        while ($commandIsEnd === false) {
            $response = $client->listCommandInvocations([
                'Details' => true,
                'CommandId' => $this->commandId,
            ]);
            if ($this->wait === false){
                break;
            }

            $isOneCommandRunning = false;
            foreach ($response['CommandInvocations'] as $invocation) {
                if(\in_array($invocation['Status'], ['Pending','InProgress'])){
                    $isOneCommandRunning = true;
                    $this->printTaskDebug("Invocation on instance '".$invocation['InstanceId']."' in status '".$invocation['Status']."'");
                }
            }
            if($isOneCommandRunning === false){
                $commandIsEnd = true;
                $this->printTaskDebug("Command '$this->commandId' end..");
            }
            sleep($this->throttle);
        }

        return Result::success($this, "Command '$this->commandId'", [
            'invocations' => $response['CommandInvocations']
        ]);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        $this->run();
        return Result::success($this);
    }

}
