<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\S3\Exception\S3Exception;
use Robo\Common\DynamicParams;
use Robo\Common\ExecCommand;
use Robo\Contract\TaskInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;
use Symfony\Component\Process\Process;
use Aws\S3\S3Client;

class S3DownloadFile extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $from = null;

    /** @var string */
    protected $to = null;

    /**
     * Download remote file
     *
     * Example usage:
     * ```php
     * $this->taskDownload()
     * 		->from('s3://example.com/archive.zip')
     * 		->to('/tmp/archive.zip')
     * ->run();
     * ```
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        $client = $this->getClient(S3Client::class);
        $bucket = $this->getBucketName();
        $keyname = $this->getKeyName();

        try {
            $this->printTaskDebug("Downloading s3://$bucket/$keyname to $this->to..");
            $client->getObject(array(
                'Bucket' => $bucket,
                'Key'    => $keyname,
                'SaveAs' => $this->to
            ));

        } catch (S3Exception $e) {
            return Result::error(
                $this,
                "Error downloading from S3: ".$e->getMessage()
            );
        }

        return Result::success($this);
    }

    /**
     * Get bucket name
     *
     * @return string
     */
    protected function getBucketName()
    {
        return parse_url($this->from,  PHP_URL_HOST);
    }

    /**
     * Get file key name
     *
     * @return string
     */
    protected function getKeyName()
    {
        return substr(parse_url($this->from,  PHP_URL_PATH), 1);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        $bucket = $this->getBucketName();
        $keyname = $this->getKeyName();
        $this->printTaskDebug("Downloading s3://$bucket/$keyname to $this->to..");
        return Result::success($this);
    }
}
