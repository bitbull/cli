<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\Ec2\Ec2Client;
use Robo\Common\DynamicParams;
use Robo\Common\TaskIO;
use Robo\Contract\SimulatedInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;

class SGDescribe extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $id = null;

    public function __construct($id)
    {
        parent::__construct();
        $this->id = $id;
    }

    /**
     * Security Group Describe
     *
     * Example usage:
     * ```php
     * $result = $this->taskSGDescribe('mysecuritygroup')
     *  ->run();
     * $minSize = $result->getData()['minSize'];
     * ```
     *
     * @return Result
     */
    function run()
    {
        $client = $this->getClient(Ec2Client::class);

        try{
            $result = $client->describeSecurityGroups([
                'GroupIds' => [$this->id]
            ])['SecurityGroups'];
        }catch(\Throwable $e){
            $this->printTaskDebug($e);
            return Result::error($this, "SecurityGroup '$this->id' not found");
        }

        if (count($result) == 0) {
            return Result::error($this, "SecurityGroup '$this->id' not found");
        }

        $infos = $result[0];

        $ingressRules = $this->elaborateRules($infos['IpPermissions']);
        $egressRules = $this->elaborateRules($infos['IpPermissionsEgress']);

        return Result::success($this, "SecurityGroup '$this->id'", [
            'ingressRules' => $ingressRules,
            'egressRules' => $egressRules,
            'vpcId' => $infos['VpcId'],
            'id' => $infos['GroupId'],
            'name' => $infos['GroupName'],
            'description' => $infos['Description'],
            'ownerId' => $infos['OwnerId'],
            'tags' => isset($infos['tags']) ? $infos['tags'] : []
        ]);
    }

    private function elaborateRules($rules)
    {
        $elaboratedRules = [];

        foreach ($rules as $rule) {
            foreach ($rule['IpRanges'] as $ipRanges) {
                array_push($elaboratedRules, [
                    'ipRanges' => $ipRanges['CidrIp'],
                    'protocol' => $rule['IpProtocol'],
                    'description' => isset($ipRanges['Description']) ? $ipRanges['Description'] : '',
                    'port' => [
                        'from' => $rule['FromPort'],
                        'to' => $rule['ToPort'],
                    ],
                    'type' => 'ipv4'
                ]);
            }
            foreach ($rule['Ipv6Ranges'] as $ipRangesV6) {
                array_push($elaboratedRules, [
                    'ipRanges' => $ipRangesV6['CidrIpv6'],
                    'protocol' => $rule['IpProtocol'],
                    'description' => isset($ipRanges['Description']) ? $ipRanges['Description'] : '',
                    'port' => [
                        'from' => $rule['FromPort'],
                        'to' => $rule['ToPort'],
                    ],
                    'type' => 'ipv6'
                ]);
            }
        }

        return $elaboratedRules;
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        return $this->run();
    }
}
