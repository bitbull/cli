<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\S3\Exception\S3Exception;
use Aws\Sns\SnsClient;
use Robo\Common\DynamicParams;
use Robo\Contract\TaskInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;
use Symfony\Component\Process\Process;
use Aws\S3\S3Client;

class SNSPublish extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $topicARN = null;

    /** @var string */
    protected $message = null;

    /** @var string */
    protected $event = null;

    public function __construct($topicARN)
    {
        parent::__construct();
        $this->topicARN = $topicARN;
    }

    /**
     * Send message or event to SNS topic
     *
     * Example usage:
     * ```php
     * $this->taskSNSPublish('arn:::')
     *      ->message('Hello world')
     * ->run();
     *
     * $this->taskSNSPublish('arn:::')
     *      ->event([
     *         'sender' => 'cli',
     *         'target' => 'test',
     *         'time' => 15,
     *         'data' => 'Hello world'
     *      ])
     * ->run();
     * ```
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        $client = $this->getClient(SnsClient::class);

        if ($this->event !== null && is_object($this->event)) {
            $this->message = json_encode($this->event);
        }

        $this->printTaskDebug("Sending to SNS topic '$this->topicARN' message '$this->message'");
        $client->publish([
            'TopicArn' => $this->topicARN,
            'Message' => $this->message,
        ]);

        return Result::success($this);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        $this->printTaskDebug("Sending to SNS topic '$this->topicARN' message '$this->message'");
        return Result::success($this);
    }

}
