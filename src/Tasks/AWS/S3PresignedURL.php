<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\S3\Exception\S3Exception;
use Robo\Common\DynamicParams;
use Robo\Contract\TaskInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;
use Symfony\Component\Process\Process;
use Aws\S3\S3Client;

class S3PresignedURL extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $path = null;

    /** @var string */
    protected $expiresIn = '+20 minutes';


    public function __construct($path)
    {
        parent::__construct();
        $this->path = $path;
    }


    /**
     * Download remote file
     *
     * Example usage:
     * ```php
     * $this->taskS3PresignedURL('s3://bucket/object.txt')
     *      ->expiresIn('+20 ')
     * ->run();
     * ```
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        $client = $this->getClient(S3Client::class);
        $bucket = $this->getBucketName();
        $keyname = $this->getKeyName();

        try {
            $this->printTaskDebug("Requesting presigned URL for s3://$bucket/$keyname..");

            $cmd = $client->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key'    => $keyname
            ]);

            $request = $client->createPresignedRequest($cmd, $this->expiresIn);
            $presignedUrl = (string) $request->getUri();

            return Result::success($this, "Create presigned URL", [
                'url' => $presignedUrl
            ]);
        } catch (S3Exception $e) {
            return Result::error(
                $this,
                "Error uploading to S3: ".$e->getMessage()
            );
        }
    }

    /**
     * Get bucket name
     *
     * @return string
     */
    protected function getBucketName()
    {
        return parse_url($this->path,  PHP_URL_HOST);
    }

    /**
     * Get file key name
     *
     * @return string
     */
    protected function getKeyName()
    {
        return substr(parse_url($this->path,  PHP_URL_PATH), 1);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        $bucket = $this->getBucketName();
        $keyname = $this->getKeyName();
        $this->printTaskDebug("Requesting presigned URL for s3://$bucket/$keyname..");
        return Result::success($this, [
            'url' => 'https://fake.presigned.url/'
        ]);
    }

}
