<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\AwsClient;
use Aws\CodeDeploy\CodeDeployClient;
use Robo\Common\DynamicParams;
use Robo\LoadAllTasks;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;

class CodeDeploy extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $applicationName = null;

    /** @var string */
    protected $identifier = null;

    /** @var string */
    protected $deploymentGroup = null;

    /** @var string */
    protected $releaseBucket = null;

    /** @var string */
    protected $artifact = null;

    /**
     * Download remote file
     *
     * Example usage:
     * ```php
     * $this->taskCodeDeploy()
     * 		->applicationName('my-app')
     * 		->identifier('<commit hash>')
     * 		->deploymentGroup('my-group')
     * 		->releaseBucket('bucket')
     * 		->artifact('artifact.zip')
     * ->run();
     * ```
     *
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        $revision = array(
            'revisionType' => 'S3',
            's3Location' => array(
                'bucket' => $this->releaseBucket,
                'key' => $this->artifact,
                'bundleType' => 'zip'
            )
        );
        $this->printTaskDebug("Using revision 's3://$this->releaseBucket/$this->artifact'");
        $client = $this->getClient(CodeDeployClient::class);

        try {
            $this->printTaskDebug("Creating revision '$this->identifier' for application '$this->applicationName'..");
            $client->registerApplicationRevision([
                'applicationName' => $this->applicationName,
                'description' => $this->identifier,
                'revision' => $revision,
            ]);
        } catch (\Exception $e) {
            return Result::error(
                $this,
                "Error creating application revision: ".$e->getMessage()
            );
        }

        try {
            $this->printTaskDebug("Creating deployment on group '$this->deploymentGroup' for application '$this->applicationName'..");
            $client->createDeployment([
                'applicationName' => $this->applicationName,
                'deploymentGroupName' => $this->deploymentGroup,
                'revision' => $revision,
                'description' => $this->identifier,
            ]);
        } catch (\Exception $e) {
            return Result::error(
                $this,
                "Error creating deployment: ".$e->getMessage()
            );
        }


        return Result::success($this);
    }
}
