<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\ElasticLoadBalancingV2\ElasticLoadBalancingV2Client;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Result;

// TODO: function name misleading: it'll create a new resource if missing (side-effect)
class ALBDescribeTargetGroup extends BaseAWSTask {

    use AWSTasks, UtilsTasks;

    /** @var string */
    protected $name = null;

    public function __construct($name)
    {
        parent::__construct();
        $this->name = $name;
    }

    /**
     * ALB Describe target group
     *
     * Example usage:
     * ```php
     * $result = $this->taskALBDescribeTargetGroup('target-group-name')
     *    ->run();
     *  $targetGroupArn = $result->getData()['arn'];
     * ```
     *
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        /** @var ElasticLoadBalancingV2Client $client */
        $client = $this->getClient(ElasticLoadBalancingV2Client::class);

        $result = $client->describeTargetGroups([
            'Names' => [$this->name]
        ]);

        $count = count($result['TargetGroups']);

        if ($count === 0) {
            return Result::error($this, "Target group '$this->name' not found");
        }

        if ($count > 1) {
            $this->printTaskWarning('Found '.$count." target groups with same name '$this->name', the first one will be used");
        }

        $targetGroup = $result['TargetGroups'][0];

        return Result::success($this, 'Created target group', [
            'arn' => $targetGroup['TargetGroupArn']
        ]);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        return $this->run();
    }
}
