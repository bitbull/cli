<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\Ssm\SsmClient;
use Robo\Result;

class SSMExecuteCommand extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $documentName = null;

    /** @var array */
    protected $instanceIds = [];

    public function __construct($documentName)
    {
        parent::__construct();
        $this->documentName = $documentName;
    }

    /**
     * Describe command and wait until is completed
     *
     * Example usage:
     * ```php
     * $result = $this->taskSSMDescribeCommand('xxxxxxx')
     *   ->run();
     * $result->getData()['invocations']
     * ```
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        $client = $this->getClient(SsmClient::class);
        $this->printTaskDebug("Executing document '$this->documentName'..");

        $response = $client->sendCommand([
            'DocumentName' => $this->documentName,
            'InstanceIds' => $this->instanceIds
        ]);

        return Result::success($this, 'Command executed', [
            'id' => $response['Command']['CommandId'],
            'command' => $response['Command'],
        ]);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        return Result::success($this, 'Command executed', [
            'id' => 'xxxxxxxxxxxx',
            'command' => [
                'CommandId' => 'xxxxxxxxxxxx'
            ]
        ]);
    }
}
