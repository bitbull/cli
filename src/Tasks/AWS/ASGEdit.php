<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\AutoScaling\AutoScalingClient;
use Robo\Result;

class ASGEdit extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $name = null;

    /** @var int */
    protected $minSize = null;

    /** @var int */
    protected $maxSize = null;

    /** @var int */
    protected $desiredCapacity = null;

    public function __construct($name)
    {
        parent::__construct();
        $this->name = $name;
    }

    /**
     * AutoScaling scale up
     *
     * Example usage:
     * ```php
     * $this->taskASGEdit('myautoscaling')
     * 		->desiredCapacity(3)
     * 		->maxSize(3)
     * ->run();
     * ```
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        $dataToUpdate = $this->elaborateDataToUpdate();
        $client = $this->getClient(AutoScalingClient::class);
        $client->updateAutoScalingGroup($dataToUpdate);

        return Result::success($this);
    }

    protected function elaborateDataToUpdate()
    {
        $dataToUpdate = [
            'AutoScalingGroupName' => $this->name
        ];

        if ($this->minSize !== null) {
            $dataToUpdate['MinSize'] = $this->minSize;
        }
        if ($this->maxSize !== null) {
            $dataToUpdate['MaxSize'] = $this->maxSize;
        }
        if ($this->desiredCapacity !== null) {
            $dataToUpdate['DesiredCapacity'] = $this->desiredCapacity;
        }

        $this->printTaskDebug("Edit AutoScalingGroup with values: ".json_encode($dataToUpdate));

        return $dataToUpdate;
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        $dataToUpdate = $this->elaborateDataToUpdate();
        return Result::success($this);
    }

}
