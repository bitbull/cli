<?php

namespace Bitbull\Cli\Tasks\AWS;

use Aws\S3\Exception\S3Exception;
use Robo\Common\DynamicParams;
use Robo\Contract\TaskInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;
use Symfony\Component\Process\Process;
use Aws\S3\S3Client;

class S3UploadDirectory extends BaseAWSTask {

    use AWSTasks;

    /** @var string */
    protected $from = null;

    /** @var string */
    protected $to = null;

    /** @var string */
    protected $public = false;

    /** @var string */
    protected $force = false;

    /** @var string */
    protected $concurrency = 20;

    /**
     * Download remote file
     *
     * Example usage:
     * ```php
     * $this->taskUpload()
     *      ->from('/tmp/mydir')
     * 		->to('s3://example-bucket/remotepath/')
     * ->run();
     * ```
     * @throws \Exception
     * @return Result
     */
    function run()
    {
        $client = $this->getClient(S3Client::class);
        $bucket = $this->getBucketName();
        $keyname = $this->getKeyName();

        try {
            $this->printTaskDebug("Uploading directory $this->from to s3://$bucket/$keyname..");
            $client->uploadDirectory($this->from, $bucket, $keyname, array(
                'params'      => [
                    'ACL' => $this->public ? 'public-read' : 'private'
                ],
                'concurrency' => $this->concurrency,
                'force'       => $this->force
            ));
        } catch (S3Exception $e) {
            return Result::error(
                $this,
                "Error uploading to S3: ".$e->getMessage()
            );
        }

        return Result::success($this);
    }

    /**
     * Get bucket name
     *
     * @return string
     */
    protected function getBucketName()
    {
        return parse_url($this->to,  PHP_URL_HOST);
    }

    /**
     * Get file key name
     *
     * @return string
     */
    protected function getKeyName()
    {
        return substr(parse_url($this->to,  PHP_URL_PATH), 1);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        $bucket = $this->getBucketName();
        $keyname = $this->getKeyName();
        $this->printTaskDebug("Uploading directory $this->from to s3://$bucket/$keyname..");
        return Result::success($this);
    }

}
