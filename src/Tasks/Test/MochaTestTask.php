<?php

namespace Bitbull\Cli\Tasks\Test;

use Robo\Result;
use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;
use Robo\Exception\TaskException;

/**
 * Execute Mocha tests
 *
 * Example usage:
 * ```php
 * $this->taskMochaTest()
 *   ->configFile('grumphp.yml')
 *   ->run();
 * ```
 *
 * @return Result
 */
class MochaTestTask extends BaseTaskCommand {

    use DynamicParams;

    /**
     * @var string
     */
    protected $command = 'node_modules/mocha/bin/mocha';

    /**
     * @var string
     */
    protected $files = '';

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return $this->command . ' ' . $this->files;
    }
}
