<?php

namespace Bitbull\Cli\Tasks\Test;

trait TestTasks
{
    /**
     * Execute GrumPHP
     *
     * @return GrumPHPTask
     */
    protected function taskGrumPHP()
    {
        return $this->task(GrumPHPTask::class);
    }

    /**
     * Execute PHP CS
     *
     * @return PHPCodingStandardsTask
     */
    protected function taskPHPCodingStandards()
    {
        return $this->task(PHPCodingStandardsTask::class);
    }

    /**
     * Execute PHP MD
     *
     * @return PHPMessDetectorTask
     */
    protected function taskPHPMessDetector()
    {
        return $this->task(PHPMessDetectorTask::class);
    }

    /**
     * Execute PHP STAN
     *
     * @return PHPStaticAnalysisToolTask
     */
    protected function taskPHPStaticAnalysisToolTask()
    {
        return $this->task(PHPStaticAnalysisToolTask::class);
    }

    /**
     * Execute Mocha test
     *
     * @return MochaTestTask
     */
    protected function taskMochaTest()
    {
        return $this->task(MochaTestTask::class);
    }

}