<?php

namespace Bitbull\Cli\Tasks\Test;

use Robo\Result;
use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;

/**
 * Execute PHP CS
 *
 * Example usage:
 * ```php
 * $this->taskPHPCodingStandardsTask()
 *   ->configFile('phpcs.xml')
 *   ->warningSeverity(0)
 *   ->path('.')
 *   ->run();
 * ```
 *
 * @return Result
 */
class PHPCodingStandardsTask extends BaseTaskCommand {

    use DynamicParams;

    /**
     * @var string
     */
    protected $command = 'vendor/bin/phpcs';

    /**
     * @var string
     */
    protected $configFile = 'phpcs.xml';

    /**
     * @var string
     */
    protected $warningSeverity = 0;

    /**
     * @var string
     */
    protected $path = '.';

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return $this->command.' --standard='.$this->configFile.' --warning-severity='.$this->warningSeverity.' '.$this->path;
    }
}
