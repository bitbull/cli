<?php

namespace Bitbull\Cli\Tasks\Test;

use Robo\Result;
use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;

/**
 * Execute PHP MD
 *
 * Example usage:
 * ```php
 * $this->taskPHPMessDetectorTask()
 *   ->configFile('phpcs.xml')
 *   ->suffixes('php')
 *   ->path('.')
 *   ->run();
 * ```
 *
 * @return Result
 */
class PHPMessDetectorTask extends BaseTaskCommand {

    use DynamicParams;

    /**
     * @var string
     */
    protected $command = 'vendor/bin/phpmd';

    /**
     * @var string
     */
    protected $configFile = 'phpmd.xml';

    /**
     * @var string
     */
    protected $suffixes = 'php';

    /**
     * @var string
     */
    protected $path = '.';

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return $this->command.' '.$this->path.' text '.$this->configFile.' --suffixes '.$this->suffixes;
    }
}
