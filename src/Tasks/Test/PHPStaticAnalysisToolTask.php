<?php

namespace Bitbull\Cli\Tasks\Test;

use Robo\Result;
use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;

/**
 * Execute PHP MD
 *
 * Example usage:
 * ```php
 * $this->taskStaticAnalysisTool()
 *   ->level(0)
 *   ->path('.')
 *   ->run();
 * ```
 *
 * @return Result
 */
class PHPStaticAnalysisToolTask extends BaseTaskCommand {

    use DynamicParams;

    /**
     * @var string
     */
    protected $command = 'vendor/bin/phpstan analyse';

    /**
     * @var string
     */
    protected $level = 0;

    /**
     * @var string
     */
    protected $path = '.';

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return $this->command.' --level '.$this->level.' '.$this->path;
    }
}
