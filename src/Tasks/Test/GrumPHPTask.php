<?php

namespace Bitbull\Cli\Tasks\Test;

use Robo\Result;
use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;

/**
 * Execute GrumPHP
 *
 * Example usage:
 * ```php
 * $this->taskGrumPHP()
 *   ->configFile('grumphp.yml')
 *   ->run();
 * ```
 *
 * @return Result
 */
class GrumPHPTask extends BaseTaskCommand {

    use DynamicParams;

    /**
     * @var string
     */
    protected $command = 'vendor/bin/grumphp';

    /**
     * @var string
     */
    protected $configFile = 'grumphp.yml';

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return $this->command . ' --config '. $this->configFile .' run';
    }
}
