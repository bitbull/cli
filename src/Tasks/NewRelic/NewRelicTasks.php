<?php

namespace Bitbull\Cli\Tasks\NewRelic;

trait NewRelicTasks
{
    /**
     * Create deployment
     *
     * @param $appId
     * @return CreateDeployment
     */
    protected function taskNewRelicCreateDeployment($appId)
    {
        return $this->task(CreateDeployment::class, $appId);
    }

}