<?php

namespace Bitbull\Cli\Tasks\NewRelic;

use Bitbull\Cli\Tasks\BaseTask;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Common\DynamicParams;
use Robo\LoadAllTasks;
use Robo\Result;

class CreateDeployment extends BaseTask
{
    use UtilsTasks;

    /** @var string */
    protected $host = 'https://api.newrelic.com/v2';

    /** @var string */
    protected $apiKey = null;

    /** @var string */
    protected $appId = null;

    /** @var string */
    protected $revision = null;

    /** @var string */
    protected $changelog = null;

    /** @var string */
    protected $description = null;

    /** @var string */
    protected $user = null;

    /**
     * CreateDeployment constructor.
     * @param $appId
     */
    public function __construct($appId)
    {
        parent::__construct();
        $this->appId = $appId;
    }

    /**
     * Create New Relic deployment
     *
     * Example usage:
     * ```php
     * $response = $this->taskNewRelicCreateDeployment()
     *      ->apiKey('xxxxxxxxxxxxxxxxxxx')
     * 		->revision('REVISION')
     * 		->description('Added a deployments resource to the v2 API')
     * ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $this->printTaskDebug("Executing request to New Relic..");
        $response = $this->taskHttpRequest($this->host . '/applications/' . $this->appId . '/deployments.json')
            ->payload([
                'deployment' => [
                    'revision' => $this->revision,
                    'changelog' => $this->changelog,
                    'description' => $this->description,
                    'user' => $this->user
                ],
            ])
            ->headers([
                'X-Api-Key: '.$this->apiKey
            ])
            ->method('POST')
            ->run();

        if (!$response->wasSuccessful()) {
            $this->printTaskDebug('Error during request');
            return $response;
        }

        $this->printTaskDebug('Deployment successfully created');

        return Result::success($this, 'Deploy created');
    }
}
