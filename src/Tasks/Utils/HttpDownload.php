<?php

namespace Bitbull\Cli\Tasks\Utils;

use Aws\S3\Exception\S3Exception;
use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;
use Robo\Common\ExecCommand;
use Robo\Contract\TaskInterface;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;
use Symfony\Component\Process\Process;
use Aws\S3\S3Client;

/**
 * Download remote file
 *
 * Example usage:
 * ```php
 * $this->taskHttpDownload()
 * 		->from('http://example.com/archive.zip')
 * 		->to('/tmp/archive.zip')
 * ->run();
 * ```
 *
 * @return Result
 */
class HttpDownload extends BaseTaskCommand {

    use DynamicParams;

    /** @var string */
    protected $from = null;

    /** @var string */
    protected $to = null;

    /** @var string */
    protected $command = 'wget';

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return $this->command." -O $this->to $this->from";
    }
}
