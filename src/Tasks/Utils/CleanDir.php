<?php
namespace Bitbull\Cli\Tasks\Utils;

use Robo\Common\ResourceExistenceChecker;
use Robo\Result;
use Robo\Task\Filesystem\BaseDir;

/**
 * Deletes all files from specified dir, ignoring git files.
 *
 * ``` php
 * <?php
 * $this->taskCleanDir(['tmp','logs'])
 *      ->ignoreFile('.htaccess')
 *      ->run();
 * ?>
 * ```
 */
class CleanDir extends BaseDir
{
    use ResourceExistenceChecker;

    /** @var array */
    protected $ignoreFiles = [
        '.gitignore',
        '.gitkeep'
    ];

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        if (!$this->checkResources($this->dirs, 'dir')) {
            return Result::error($this, 'Source directories are missing!');
        }
        foreach ($this->dirs as $dir) {
            $this->emptyDir($dir);
            $this->printTaskInfo("Cleaned {dir}", ['dir' => $dir]);
        }
        return Result::success($this);
    }

    /**
     * @param string $path
     */
    protected function emptyDir($path)
    {
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($path),
            \RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($iterator as $path) {
            if ($path->isDir()) {
                $dir = (string)$path;
                if (basename($dir) === '.' || basename($dir) === '..') {
                    continue;
                }
                $this->fs->remove($dir);
            } else {
                $file = (string)$path;
                if (in_array(basename($file), $this->ignoreFiles)) {
                    continue;
                }
                $this->fs->remove($file);
            }
        }
    }

    /**
     * Ignore file name
     *
     * @param $fileName
     */
    public function ignoreFile($fileName)
    {
        array_push($this->ignoreFiles, $fileName);
    }

}
