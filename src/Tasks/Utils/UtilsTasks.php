<?php

namespace Bitbull\Cli\Tasks\Utils;

use Robo\Result;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

trait UtilsTasks
{
    /**
     * @return string|null
     */
    protected static function getTagEnv()
    {
        $tag = null;
        if (getenv('BITBUCKET_TAG') !== false && trim(getenv('BITBUCKET_TAG')) !== '') {
            $tag = getenv('BITBUCKET_TAG');
        } elseif (getenv('CI_COMMIT_TAG') !== false && trim(getenv('CI_COMMIT_TAG')) !== '') {
            $tag = getenv('CI_COMMIT_TAG');
        } elseif (getenv('TRAVIS_TAG') !== false && trim(getenv('TRAVIS_TAG')) !== '') {
            $tag = getenv('TRAVIS_TAG');
        } elseif (getenv('BITBUCKET_BRANCH') !== false && trim(getenv('BITBUCKET_BRANCH')) !== '') {
            $tag = getenv('BITBUCKET_BRANCH');
        } elseif (getenv('CI_COMMIT_REF_NAME') !== false && trim(getenv('CI_COMMIT_REF_NAME')) !== '') {
            $tag = getenv('CI_COMMIT_REF_NAME');
        } elseif (getenv('TRAVIS_BRANCH') !== false && trim(getenv('TRAVIS_BRANCH')) !== '') {
            $tag = getenv('TRAVIS_BRANCH');
        }
        return $tag;
    }

    /**
     * Download file
     *
     * @return Download
     */
    protected function taskDownload()
    {
        return $this->task(Download::class);
    }

    /**
     * Download file
     *
     * @return HttpDownload
     */
    protected function taskHttpDownload()
    {
        return $this->task(HttpDownload::class);
    }

    /**
     * Empty success task
     *
     * @return EmptySuccessTask
     */
    protected function taskEmptySuccess()
    {
        return $this->task(EmptySuccessTask::class);
    }

    /**
     * Empty error task
     *
     * @return EmptyErrorTask
     */
    protected function taskEmptyError($message = '')
    {
        return $this->task(EmptyErrorTask::class, $message);
    }

    /**
     * Exec HTTP Request
     *
     * @param $url
     * @return HttpRequest
     */
    protected function taskHttpRequest($url)
    {
        return $this->task(HttpRequest::class, $url);
    }

    /**
     * SCP Upload
     *
     * @return ScpUpload
     */
    protected function taskScpUpload()
    {
        return $this->task(ScpUpload::class);
    }

    /**
     * SCP Download
     *
     * @return ScpDownload
     */
    protected function taskScpDownload()
    {
        return $this->task(ScpDownload::class);
    }

    /**
     * Zip directory
     *
     * @param $directory
     * @return ZipDirectory
     */
    protected function taskZipDirectory($directory)
    {
        return $this->task(ZipDirectory::class, $directory);
    }

    /**
     * Clean directory
     *
     * @param $directorys
     * @return CleanDir
     */
    protected function taskCleanDirExt($directorys)
    {
        return $this->task(CleanDir::class, $directorys);
    }

    /**
     * Is file exist
     *
     * @param $path
     * @return boolean
     */
    protected function _exist($path)
    {
        $fs = new Filesystem();
        return $fs->exists($path);
    }

    /**
     * @param string $filename
     * @param string $baseDir
     * @param bool $recursive
     * @return bool
     */
    protected function _existsInFolder($filename, $baseDir, $recursive = true)
    {
        $finder = new Finder();
        if (!$recursive) {
            $finder->depth('== 0');
        }
        return count($finder->files()->in($baseDir)->name($filename)) > 0;
    }

    /**
     * Is path a directory
     *
     * @param $path
     * @return boolean
     */
    protected function _isDirectory($path)
    {
        return is_dir($path);
    }

    /**
     * Load text file content
     *
     * @param $file
     * @return string
     */
    protected function loadTextFile($file)
    {
        return file_get_contents($file);
    }

    /**
     * Load JSON file content
     *
     * @param $file
     * @return array
     */
    protected function loadJsonFile($file)
    {
        $content = file_get_contents($file);
        return json_decode($content,true);
    }

    /**
     * Load YAML file content
     *
     * @param $file
     * @return array
     */
    protected function loadYamlFile($file)
    {
        $content = file_get_contents($file);
        return (array) Yaml::parse($content);
    }

    /**
     * Load env file content
     *
     * @param $file
     * @return array
     */
    protected function loadEnvFile($file)
    {
        $content = file_get_contents($file);
        $lines = explode(PHP_EOL , $content);
        $envVars = [];
        foreach ($lines as $line) {
            $var = explode('=', $line);
            if(isset($var[1])){
                $envVars[$var[0]] = $var[1];
            }
        }
        return $envVars;
    }

    /**
     * Load csv file content
     *
     * @param $file
     * @param $separator
     * @return array
     */
    protected function loadCsvFile($file, $separator = ';')
    {
        $content = file_get_contents($file);
        $lines = explode(PHP_EOL , $content);
        $resultData = [];
        foreach ($lines as $line) {
            if(trim($line) == ''){
                continue;
            }
            $data = explode($separator, $line);
            if(sizeof($data) > 0){
                array_push($resultData, $data);
            }
        }
        return $resultData;
    }

    /**
     * Load JSON file content
     *
     * @param $length
     * @param $set
     * @return array
     */
    protected function strRandom($length, $set = 'default')
    {
        switch ($set) {
            case 'char':
                $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                break;
            case 'number':
                $chars = "0123456789";
                break;
            case 'char|number':
                $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                break;
            case 'specialchar':
                $chars = "!@#$%^&*()_-=+;:,.?";
                break;
            default:
                $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
                break;
        }

        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }

    /**
     * Ask ChoiceQuestion
     *
     * @param $question
     * @param $choices
     * @param $default
     * @param $errorMessage
     * @return string
     */
    protected function askChoice($question, $choices, $default = null, $errorMessage = 'Response %s is invalid.')
    {
        $questionObj = new ChoiceQuestion($this->formatQuestion($question), $choices);
        $questionObj->setErrorMessage($errorMessage);
        $response = $this->doAsk($questionObj, $choices, $default);
        return print_r($response, true);
    }

    /**
     * @param bool $stopOnFail
     * @return bool
     */
    protected function stopOnFail($stopOnFail = true)
    {
        $pre = Result::$stopOnFail;
        Result::$stopOnFail = $stopOnFail;
        return $pre;
    }

    /**
     * Elaborate Docker image tag from env variables
     *
     * @return string
     */
    protected function elaborateDockerTag($fallback = 'latest')
    {
        $tag = self::getTagEnv();
        if($tag !== null){
            $tag = preg_replace("/[^a-zA-Z\d\s-]/", '-', $tag);
        }else{
            $tag = $fallback;
        }

        if (strlen($tag) > 128) {
            $tag = substr($tag, 0, 128);
        }

        return $tag;
    }

    /**
     * Elaborate build identifier
     *
     * @return string
     */
    protected function elaborateBuildIdentifier()
    {
        $identifier = self::getTagEnv();
        if($identifier === null){
            $identifier = $this->strRandom(5, 'char|number');
        }

        return $identifier;
    }

    /**
     * Elaborate nice build identifier
     *
     * @param $fallback
     * @param $charReplacement
     * @param $limit
     * @return string
     */
    protected function elaborateNiceBuildIdentifier($fallback = 'latest', $charReplacement = '-', $limit = 128)
    {
        $identifier = self::getTagEnv();
        if($identifier === null || trim($identifier) === ''){
            $identifier = $fallback;
        }

        if (strlen($identifier) > $limit) {
            $identifier = substr($identifier, 0, $limit);
        }

        $identifier = preg_replace("/[^a-zA-Z\d\s$charReplacement]/", $charReplacement, $identifier);

        return $identifier;
    }

    /**
     * Find directories by name inside a paths
     *
     * @param $name
     * @param string $path
     * @return array
     */
    protected function findDirectories($name, $path = null)
    {
        if ($path == null) {
            $path = getcwd();
        }

        $finder = new Finder();
        $finder->directories()
            ->ignoreVCS(false)
            ->ignoreDotFiles(false)
            ->name($name)
            ->in($path);

        $dirs = [];

        foreach ($finder as $dir) {
            array_push($dirs, $dir->getPathname());
        }

        return $dirs;
    }

    /**
     * Find files by name inside a paths
     *
     * @param $name
     * @param string $path
     * @return array
     */
    protected function findFiles($name, $path = null)
    {
        if ($path == null) {
            $path = getcwd();
        }

        $finder = new Finder();
        $finder->files()
            ->ignoreVCS(false)
            ->ignoreDotFiles(false)
            ->name($name)
            ->in($path);

        $dirs = [];

        foreach ($finder as $dir) {
            array_push($dirs, $dir->getRealPath());
        }

        return $dirs;
    }
}
