<?php

namespace Bitbull\Cli\Tasks\Utils;

use Aws\S3\Exception\S3Exception;
use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;
use Robo\Common\ExecCommand;
use Robo\Contract\TaskInterface;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;
use Symfony\Component\Process\Process;
use Aws\S3\S3Client;

/**
 * Zip directory
 *
 * Example usage:
 * ```php
 * $this->taskZipDirectory('.')
 * 		->to('/tmp/archive.zip')
 * ->run();
 * ```
 *
 * @return Result
 */
class ZipDirectory extends BaseTaskCommand {

    use DynamicParams;

    /** @var string */
    protected $directory = null;

    /** @var string */
    protected $to = null;

    /**
     * Constructor
     *
     * @param $directory
     */
    function __construct($directory)
    {
        parent::__construct();

        $this->directory = $directory;
    }

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return "cd $this->directory && zip -r $this->to .";
    }
}
