<?php

namespace Bitbull\Cli\Tasks\Utils;

use Aws\S3\Exception\S3Exception;
use Bitbull\Cli\Tasks\AWS\AWSTasks;
use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;
use Robo\Common\ExecCommand;
use Robo\Contract\TaskInterface;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;
use Symfony\Component\Process\Process;
use Aws\S3\S3Client;

class Download extends BaseTask {

    use UtilsTasks, AWSTasks;

    /** @var string */
    protected $from = null;

    /** @var string */
    protected $to = null;

    /** @var string */
    protected $region = null;

    /** @var string */
    protected $profile = null;

    /**
     * Download remote file
     *
     * Example usage:
     * ```php
     * $this->taskDownload()
     * 		->from('http://example.com/archive.zip')
     * 		->to('/tmp/archive.zip')
     * ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $protocol = parse_url($this->from,  PHP_URL_SCHEME);
        switch ($protocol) {
            case 'http':
            case 'https':
                return $this->taskHttpDownload()
                    ->from($this->from)
                    ->to($this->to)
                    ->run();
            case 's3':
                return $this->taskS3DownloadFile()
                    ->from($this->from)
                    ->to($this->to)
                    ->region($this->region)
                    ->profile($this->profile)
                    ->run();
            default:
                return Result::error($this, "Protocol '$protocol' not supported for download'");
        }

    }
}
