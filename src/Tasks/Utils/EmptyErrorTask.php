<?php

namespace Bitbull\Cli\Tasks\Utils;

use Bitbull\Cli\Tasks\BaseTask;
use Robo\Common\DynamicParams;
use Robo\Result;

class EmptyErrorTask extends BaseTask {

    use DynamicParams;

    /** @var string */
    protected $message = "";

    /** @var array */
    protected $data = [];

    /**
     * EmptyErrorTask constructor.
     *
     * @param $message
     */
    function __construct($message)
    {
        parent::__construct();

        $this->message = $message;
    }

    /**
     * Empty task
     *
     * Example usage:
     * ```php
     * $this->taskEmptyError()->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        return Result::error($this, $this->message, $this->data);
    }
}
