<?php

namespace Bitbull\Cli\Tasks\Utils;

use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;
use Robo\Result;

/**
 * Enable or disable a specific module
 *
 * Example usage:
 * ```php
 * $this->taskScpUpload()
 * 		->from('./example.txt')
 * 		->to('/tmp')
 * 		->host('my-remote-server.com')
 * 		->key('./secure-key')
 * 		->username('root')
 * ->run();
 * ```
 *
 * @return Result
 */
class ScpDownload extends BaseTaskCommand {

    use DynamicParams;

    /** @var string */
    protected $executable = "scp";

    /** @var string */
    protected $from = null;

    /** @var string */
    protected $to = null;

    /** @var string */
    protected $key = null;

    /** @var string */
    protected $username = null;

    /** @var string */
    protected $host = null;

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        $command = $this->executable;

        if($this->key){
            $command .= ' -i '.$this->key;
        }

        return $command . $this->username.'@'.$this->host.':'.$this->from.' '.$this->to;
    }
}
