<?php

namespace Bitbull\Cli\Tasks\Utils;

use Bitbull\Cli\Tasks\BaseTask;
use Robo\Result;

class EmptySuccessTask extends BaseTask {

    /**
     * Empty task
     *
     * Example usage:
     * ```php
     * $this->taskEmptySuccess()->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        return Result::success($this);
    }
}
