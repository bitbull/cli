<?php

namespace Bitbull\Cli\Tasks\Utils;

use Bitbull\Cli\Tasks\BaseTask;
use Robo\Common\DynamicParams;
use Robo\Result;
use Robo\Robo;

class HttpRequest extends BaseTask {

    use DynamicParams;

    const CONTENT_TYPE_JSON = 'application/json';
    const CONTENT_TYPE_FORM = 'application/x-www-form-urlencoded';

    /** @var string */
    protected $url = null;

    /** @var array */
    protected $payload = [];

    /** @var array */
    protected $method = 'GET';

    /** @var array */
    protected $headers = [];

    /** @var array */
    protected $statusesOk = [200, 201];

    /** @var bool */
    protected $followRedirect = false;

    /** @var bool*/
    protected $verbose = false;

    /** @var bool*/
    protected $contentType = 'application/json';

    /**
     * Constructor
     */
    function __construct($url)
    {
        parent::__construct();

        $this->url = $url;

        if (Robo::output()->isVerbose()) {
            $this->verbose = true;
        }
    }

    /**
     * Http Request task
     *
     * Example usage:
     * ```php
     * $this->taskHttpRequest('http://example.com')
     *      ->method('GET')
     *      ->payload([])
     *      ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $curl = curl_init();
        $data_string = '';
        if ($this->contentType === self::CONTENT_TYPE_JSON) {
            $data_string = json_encode($this->payload);
        } elseif ($this->contentType === self::CONTENT_TYPE_FORM) {
            foreach ($this->payload as $payloadPartKey => $payloadPartValue) {
                if ($data_string !== '') {
                    $data_string .= '&';
                }
                $data_string .= $payloadPartKey.'='.$payloadPartValue;
            }
        }

        if (in_array($this->method, ['GET']) ) {
            $this->printTaskDebug("Executing $this->method request to $this->url..");
        } else {
            $this->printTaskDebug("Executing $this->method request to $this->url with data: ".$data_string);
        }

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CUSTOMREQUEST => $this->method,
            CURLOPT_URL => $this->url,
            CURLOPT_POSTFIELDS => $data_string,
            CURLOPT_HTTPHEADER => array_merge(array(
                'Content-Type: '.$this->contentType,
                'Content-Length: ' . strlen($data_string)
            ), $this->headers),
            CURLOPT_HEADER => 1,
            CURLOPT_FOLLOWLOCATION => intval($this->followRedirect),
            CURLOPT_MAXREDIRS => 3,
            CURLOPT_VERBOSE => $this->verbose
        ));
        $response = curl_exec($curl);
        if (curl_errno($curl) > 0) {
            return Result::error($this, 'CURL error: '.curl_error($curl));
        }

        $http_code = intval(curl_getinfo($curl, CURLINFO_HTTP_CODE));
        $headerSize = intval(curl_getinfo($curl, CURLINFO_HEADER_SIZE));
        $body = substr($response, $headerSize);
        curl_close($curl);

        $this->printTaskDebug("Request executed, code: $http_code, response: $response");

        if (in_array($http_code, $this->statusesOk)) {
            return Result::success($this, "Request successfully executed", [
                'response' => json_decode($body),
                'rawResponse'=> $body,
                'code' => $http_code,
                'headers' => $this->getHeaders($response),
                'cookies' => $this->getCookies($response),
            ]);
        } else {
            return Result::error($this, "Error calling $this->url, code: $http_code, response: $response", [
                'response' => json_decode($body),
                'rawResponse'=> $body,
                'code' => $http_code,
            ]);
        }
    }

    protected function getCookies($response)
    {
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $response, $matches);
        $cookies = [];
        foreach($matches[1] as $item) {
            parse_str($item, $cookie);
            $cookies = array_merge($cookies, $cookie);
        }
        return $cookies;
    }

    protected function getHeaders($response)
    {
        $headers = [];
        $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));
        foreach (explode("\r\n", $header_text) as $i => $line){
            if($i !== 0){
                list ($key, $value) = explode(': ', $line);
                $headers[$key] = $value;
            }
        }
        return $headers;
    }
}
