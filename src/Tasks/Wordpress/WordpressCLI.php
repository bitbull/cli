<?php

namespace Bitbull\Cli\Tasks\Wordpress;

use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;
use Robo\Result;

/**
 * Enable or disable a specific module
 *
 * Example usage:
 * ```php
 * $this->taskWPCLI()
 * 		->command('cli update')
 * 		->params()
 * ->run();
 * ```
 *
 * @return Result
 */
class WordpressCLI extends BaseTaskCommand {

    use DynamicParams;

    /** @var string */
    protected $executable = "wp";

    /** @var string */
    protected $command = null;

    /** @var array */
    protected $params = [];

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return $this->executable.' '.$this->command.' '. implode(' ', $this->params);
    }

    /**
     * Add parameter
     *
     * @param $params string
     * @return WordpressCLI
     */
    function addParam($params)
    {
        array_push($this->params, $params);

        return $this;
    }
}
