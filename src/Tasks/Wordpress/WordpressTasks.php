<?php

namespace Bitbull\Cli\Tasks\Wordpress;

trait WordpressTasks
{
    /**
     * Wordpress WP CLI task
     *
     * @return WordpressCLI
     */
    protected function taskWPCLI()
    {
        return $this->task(WordpressCLI::class);
    }

}