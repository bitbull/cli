<?php

namespace Bitbull\Cli\Tasks\Vagrant;

use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;
use Robo\Result;
use Robo\Robo;
use Bitbull\Cli\Tasks\BaseTask;
use Symfony\Component\Process\Process;

class VagrantConnect extends BaseTask {

    use DynamicParams;

    /** @var string */
    protected $command = 'vagrant ssh';

    /**
     * Connect to Vagrant using SSH
     *
     * Example usage:
     * ```php
     * $this->taskVagrantConnect()
     *   ->run();
     * ```
     *
     * @return Result
     */
    public function run()
    {
        $process = new Process($this->command, getcwd());
        $process->run();

        if(!$process->isSuccessful()){
            return Result::error($this, $process->getErrorOutput());
        }else{
            return Result::success($this);
        }
    }
}
