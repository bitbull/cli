<?php

namespace Bitbull\Cli\Tasks\Vagrant;

use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;

class VagrantDown extends BaseTaskCommand {

    use DynamicParams;

    /** @var string */
    protected $command = "vagrant";

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return $this->command.' destroy';
    }
}
