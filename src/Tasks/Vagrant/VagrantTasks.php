<?php

namespace Bitbull\Cli\Tasks\Vagrant;

trait VagrantTasks
{
    /**
     * Vagrant up
     *
     * @return VagrantUp
     */
    protected function taskVagrantUp()
    {
        return $this->task(VagrantUp::class);
    }

    /**
     * Vagrant down
     *
     * @return VagrantDown
     */
    protected function taskVagrantDown()
    {
        return $this->task(VagrantDown::class);
    }

    /**
     * Vagrant start
     *
     * @return VagrantStart
     */
    protected function taskVagrantStart()
    {
        return $this->task(VagrantStart::class);
    }

    /**
     * Vagrant stop
     *
     * @return VagrantStop
     */
    protected function taskVagrantStop()
    {
        return $this->task(VagrantStop::class);
    }

    /**
     * Vagrant connect
     *
     * @return VagrantConnect
     */
    protected function taskVagrantConnect()
    {
        return $this->task(VagrantConnect::class);
    }

}