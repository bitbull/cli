<?php

namespace Bitbull\Cli\Tasks;

use Robo\Result;
use Robo\Collection\CollectionBuilder;
use Robo\Common\DynamicParams;
use Robo\Common\TaskIO;
use Robo\Contract\SimulatedInterface;
use Robo\LoadAllTasks;
use Robo\Robo;
use Robo\Task\BaseTask as ParentTask;
use Symfony\Component\Process\Process;

abstract class BaseTask extends ParentTask implements SimulatedInterface
{
    use DynamicParams, TaskIO, LoadAllTasks;

    function __construct()
    {
        $builder = CollectionBuilder::create(Robo::getContainer(), $this);
        $this->setBuilder($builder);
    }

    abstract public function run();

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        return Result::success($this);
    }

    /**
     * Execute shell command
     *
     * @param string $command
     *
     * @return \Robo\Result
     */
    protected function executeCommand($command)
    {
        $cwd = getcwd();

        $this->printTaskDebug("Executing '$command' using '$cwd' as working directory..");

        $process = new Process($command, $cwd);
        $process->setTimeout(null);
        $process->setIdleTimeout(null);
        $process->run();
        $commandOutput = $process->getOutput();

        $this->printTaskDebug($commandOutput);

        if(!$process->isSuccessful()){
            return Result::error($this, $commandOutput.$process->getErrorOutput());
        }
        $this->printTaskDebug("Command '$command' executed");
        return new Result(
            $this,
            $process->getExitCode(),
            $commandOutput
        );
    }
}
