<?php

namespace Bitbull\Cli\Tasks;

use Robo\Common\DynamicParams;
use Robo\Common\ExecOneCommand;
use Robo\Contract\CommandInterface;
use Robo\Contract\PrintedInterface;
use Robo\Result;
use Symfony\Component\Process\Process;

abstract class BaseTaskCommand extends BaseTask implements CommandInterface, PrintedInterface
{
    use DynamicParams, ExecOneCommand;

    /**
     * @var string
     */
    protected $command = '';

    /**
     * @var string
     */
    protected $cwd = null;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $command = $this->getCommand();

        if ($this->cwd == null) {
            $this->cwd = getcwd();
        }

        $this->printTaskDebug("Executing '$command' using '$this->cwd' as working directory..");

        $process = new Process($command, $this->cwd);
        $process->setTimeout(null);
        $process->setIdleTimeout(null);
        $process->run();
        $commandOutput = $process->getOutput();
        $this->printTaskDebug($commandOutput);

        if(!$process->isSuccessful()){
            return Result::error($this, $commandOutput.$process->getErrorOutput());
        }
        $this->printTaskDebug("Command '$command' executed");
        return new Result(
            $this,
            $process->getExitCode(),
            $commandOutput
        );
    }

    abstract public function getCommand();
}
