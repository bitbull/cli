<?php

namespace Bitbull\Cli\Tasks\Git;

trait GitTasks
{
    /**
     * Get current commit info
     *
     * @return GitCommitInfo
     */
    protected function taskGitCommitInfo()
    {
        return $this->task(GitCommitInfo::class);
    }

}