<?php

namespace Bitbull\Cli\Tasks\Git;

use Robo\Common\DynamicParams;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;

class GitCommitInfo extends BaseTask {

    use DynamicParams;

    /**
     * Get current commit infos
     *
     * Example usage:
     * ```php
     * $result = $this->taskGitCurrentCommitMessage()
     *   ->run();
     * $title = $response->getData()['title'];
     * ```
     *
     * @return Result
     */
    function run()
    {
        $this->printTaskDebug("Retrieving current commit infos..");

        $result = $this->executeCommand('git log -1 --format="%H$%s$%b$%ae$%an"');

        if (!$result->wasSuccessful()) {
            return $result;
        }

        $commandMessage = str_replace(PHP_EOL , '', $result->getMessage());
        $output = explode('$', $commandMessage);

        return Result::success($this, 'Retrieved current commit infos', [
            'hash' => $output[0],
            'title' => $output[1],
            'body' => $output[2],
            'authorEmail' => $output[3],
            'authorName' => $output[4],
        ]);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        return $this->run();
    }
}
