<?php

namespace Bitbull\Cli\Tasks\Portainer;

use Bitbull\Cli\Tasks\BaseTask;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Common\DynamicParams;
use Robo\LoadAllTasks;
use Robo\Result;

class PortainerCreateOrUpdateStack extends BaseTask
{
    use DynamicParams, UtilsTasks, LoadAllTasks;

    /** @var string */
    protected $token = null;

    /** @var string */
    protected $host = null;

    /** @var string */
    protected $stackName = null;

    /** @var string */
    protected $endpointNum = 1;

    /** @var string */
    protected $swarmId = null;

    /** @var string */
    protected $dockerComposeFile = null;

    /** @var string */
    protected $envFile = null;

    public function __construct($host)
    {
        parent::__construct();
        
        $this->host = $host;
    }

    /**
     * Create or update an existing stack
     *
     * Example usage:
     * ```php
     * $response = $this->taskPortainerCreateOrUpdateStack('http://docker-swarm.example.com:9000')
     *      ->stackName('example')
     *      ->token('blabla')
     * ->run();
     * $stack = $response->getData()['stack'];
     * ```
     *
     * @return Result
     */
    function run()
    {
        $this->printTaskDebug('Retrieving all stacks data..');
        $response = $this->taskHttpRequest($this->host . '/api/stacks')
            ->headers([
                'Authorization: Bearer '.$this->token
            ])
            ->method('GET')
            ->run();

        $stacks = $response->getData()['response'];
        $this->printTaskDebug('Retrieved '.sizeof($stacks).' stacks');
        $stackToUpdate = null;
        foreach ($stacks as $stack) {
            if($this->swarmId == null){
                $this->swarmId = $stack->SwarmId;
            }

            if ($stack->Name == $this->stackName){
                $stackToUpdate = $stack;
                $this->printTaskDebug("Found stack with name $this->stackName");
                break;
            }
        }

        if($stackToUpdate === null){
            $this->printTaskInfo("Stack $this->stackName not found, creating it..");

            if($this->swarmId == null || $this->dockerComposeFile == null){
                return Result::error($this, 'SwarmId and DockerComposeFile are required to create a new stack');
            }

            $env = [];
            if($this->envFile !== null){
                $this->printTaskDebug("Loading environment variable from file $this->envFile..");
                $envVars = $this->loadEnvFile($this->envFile);
                foreach ($envVars as $envName => $envValue) {
                    array_push($env, [
                        'name' => $envName,
                        'value' => $envValue,
                    ]);
                }
                $this->printTaskDebug('Loaded '.sizeof($envVars).' environment variables');
            }

            $this->printTaskDebug("Creating new stack with name $this->stackName..");
            $response = $this->taskHttpRequest($this->host . '/api/stacks?method=string&endpointId=' . $this->endpointNum)
                ->headers([
                    'Authorization: Bearer ' . $this->token
                ])
                ->method('POST')
                ->payload([
                    'Name' => $this->stackName,
                    'SwarmID' => $this->swarmId,
                    'StackFileContent' => $this->loadTextFile($this->dockerComposeFile),
                    'Env' => $env,
                ])
                ->run();

            $this->printTaskInfo("Stack $this->stackName created.");
            return Result::success($this, 'Stack created', [
                'stack' => $response->getData()['response']
            ]);

        }else{
            $this->printTaskInfo("Stack $this->stackName exist, updating it..");

            if($this->dockerComposeFile == null){
                $response = $this->taskHttpRequest($this->host . '/api/stacks/' . $stackToUpdate->Id . '/stackfile?endpointId=' . $this->endpointNum)
                    ->headers([
                        'Authorization: Bearer '.$this->token
                    ])
                    ->method('GET')
                    ->run();
                $stackFileContent = $response->getData()['response']->StackFileContent;
            }else{
                $stackFileContent = $this->loadTextFile($this->dockerComposeFile);
            }

            $this->printTaskDebug("Updating stack with name $stackToUpdate->Name");
            $this->taskHttpRequest($this->host . '/api/stacks/' . $stackToUpdate->Id . '?endpointId=' . $this->endpointNum)
                ->headers([
                    'Authorization: Bearer '.$this->token
                ])
                ->payload([
                    'StackFileContent' => $stackFileContent,
                    'Env' => $stackToUpdate->Env,
                ])
                ->method('PUT')
                ->run();
            $this->printTaskInfo("Stack $stackToUpdate->Name updated");

            return Result::success($this, 'Stack updated', [
                'stack' => $stackToUpdate
            ]);
        }
    }
}
