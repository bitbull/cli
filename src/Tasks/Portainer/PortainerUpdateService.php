<?php

namespace Bitbull\Cli\Tasks\Portainer;

use Bitbull\Cli\Tasks\BaseTask;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Common\DynamicParams;
use Robo\LoadAllTasks;
use Robo\Result;

class PortainerUpdateService extends BaseTask
{
    use DynamicParams, UtilsTasks, LoadAllTasks;

    /** @var string */
    protected $token = null;

    /** @var string */
    protected $host = null;

    /** @var string */
    protected $serviceName = null;

    /** @var string */
    protected $image = null;

    /** @var string */
    protected $force = false;

    /** @var string */
    protected $endpointNum = 1;

    public function __construct($host)
    {
        parent::__construct();
        
        $this->host = $host;
    }

    /**
     * Update an existing service
     *
     * Example usage:
     * ```php
     * $response = $this->taskPortainerUpdateService('http://docker-swarm.example.com:9000')
     *      ->serviceName('example')
     *      ->token('blabla')
     * ->run();
     * $stack = $response->getData()['stack'];
     * ```
     *
     * @return Result
     */
    function run()
    {
        $this->printTaskDebug("Retrieving service data with name $this->serviceName..");
        $response = $this->taskHttpRequest($this->host . '/api/endpoints/' . $this->endpointNum . '/docker/services/' . $this->serviceName)
            ->headers([
                'Authorization: Bearer '.$this->token
            ])
            ->method('GET')
            ->run();

        $serviceData = $response->getData()['response'];
        $updatePayload = $serviceData->Spec;
        $serviceVersion = $serviceData->Version->Index;
        if ($this->image !== null) {
            $updatePayload->TaskTemplate->ContainerSpec->Image = $this->image;
        }
        if($this->force){
            $this->printTaskDebug('Enable force update');
            $updatePayload->TaskTemplate->ForceUpdate = 1;
        }

        $this->printTaskDebug("Updating service with name $this->serviceName..");
        $response = $this->taskHttpRequest($this->host . '/api/endpoints/' . $this->endpointNum . '/docker/services/' . $this->serviceName . '/update?version=' . $serviceVersion)
            ->headers([
                'Authorization: Bearer '.$this->token
            ])
            ->payload((array) $updatePayload)
            ->method('POST')
            ->run();

        $serviceUpdateData = $response->getData()['response'];
        if ($serviceUpdateData->Warnings !== null) {
            $this->printTaskError("Warnings during service $this->serviceName update, ".json_encode($serviceUpdateData->Warnings));
        }

        $this->printTaskDebug("Checking if service $this->serviceName is correctly updated..");
        $response = $this->taskHttpRequest($this->host . '/api/endpoints/' . $this->endpointNum . '/docker/services/' . $this->serviceName)
            ->headers([
                'Authorization: Bearer '.$this->token
            ])
            ->method('GET')
            ->run();

        $newServiceData = $response->getData()['response'];
        if ($newServiceData->Version->Index === $serviceVersion) {
            return Result::error($this, 'Service is not updated, has the same version number');
        }

        $this->printTaskInfo("Service version updated from $serviceVersion to {$newServiceData->Version->Index}");
        return Result::success($this, 'Service updated', [
            'stack' => $serviceData
        ]);
    }
}
