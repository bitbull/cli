<?php

namespace Bitbull\Cli\Tasks\Portainer;

trait PortainerTasks
{
    /**
     * Get auth token
     *
     * @param $host
     * @return PortainerGetAuthToken
     */
    protected function taskPortainerGetAuthToken($host)
    {
        return $this->task(PortainerGetAuthToken::class, $host);
    }

    /**
     * Get auth token
     *
     * @param $host
     * @return PortainerCreateOrUpdateStack
     */
    protected function taskPortainerCreateOrUpdateStack($host)
    {
        return $this->task(PortainerCreateOrUpdateStack::class, $host);
    }

    /**
     * Get auth token
     *
     * @param $host
     * @return PortainerUpdateService
     */
    protected function taskPortainerUpdateService($host)
    {
        return $this->task(PortainerUpdateService::class, $host);
    }

    /**
     * Get auth token
     *
     * @param $host
     * @return PortainerDeleteStack
     */
    protected function taskPortainerDeleteStack($host)
    {
        return $this->task(PortainerDeleteStack::class, $host);
    }

    /**
     * Execute command on container
     *
     * @param $host
     * @return PortainerCommandExec
     */
    protected function taskPortainerCommandExec($host)
    {
        return $this->task(PortainerCommandExec::class, $host);
    }
}