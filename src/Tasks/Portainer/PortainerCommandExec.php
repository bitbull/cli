<?php

namespace Bitbull\Cli\Tasks\Portainer;

use Bitbull\Cli\Tasks\BaseTask;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Common\DynamicParams;
use Robo\LoadAllTasks;
use Robo\Result;

class PortainerCommandExec extends BaseTask
{
    use DynamicParams, UtilsTasks, LoadAllTasks;

    /** @var string */
    protected $token = null;

    /** @var string */
    protected $host = null;

    /** @var string */
    protected $endpointNum = 1;

    /** @var string */
    protected $service = null;

    /** @var string */
    protected $command = null;

    /** @var string */
    protected $interpreter = 'sh';

    /** @var string */
    protected $interpreterCommandParam = '-c';

    /** @var string */
    protected $waitUntilComplete = false;

    public function __construct($host)
    {
        parent::__construct();

        $this->host = $host;
    }

    /**
     * Execute Command
     *
     * Example usage:
     * ```php
     * $response = $this->taskPortainerCommandExec('http://docker-swarm.example.com:9000')
     *      ->service('example')
     *      ->command('cd /tmp/ && ls')
     *      ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $this->printTaskDebug('Retrieving containers data..');
        $response = $this->taskHttpRequest($this->host . '/api/endpoints/' . $this->endpointNum . '/docker/containers/json?all=1&filters={%22name%22:{%22'.$this->service.'%22:true}}')
            ->headers([
                'Authorization: Bearer '.$this->token
            ])
            ->method('GET')
            ->run();

        $responseData = $response->getData()['response'];
        $containerId = null;
        foreach ($responseData as $container) {
            $containerId = $container->Id;
            $this->printTaskDebug('Found container: '.$container->Names[0]);
            break;
        }

        if ($containerId === null) {
            return Result::error($this, 'No containers found');
        }

        $this->printTaskDebug('Executing command on container..');
        $response = $this->taskHttpRequest($this->host . '/api/endpoints/' . $this->endpointNum . '/docker/containers/'.$containerId.'/exec')
            ->headers([
                'Authorization: Bearer '.$this->token
            ])
            ->method('POST')
            ->payload([
                'AttachStdin' => true,
                'AttachStdout' => true,
                'AttachStderr' => true,
                'Cmd' => [$this->interpreter, $this->interpreterCommandParam, $this->command],
                'DetachKeys' => "ctrl-p,ctrl-q",
                'Privileged' => true,
                'Tty' => true,
            ])
            ->run();
        $responseData = $response->getData()['response'];
        $execId = $responseData->Id;

        $response = $this->taskHttpRequest($this->host . '/api/endpoints/' . $this->endpointNum . '/docker/exec/'.$execId.'/start')
            ->headers([
                'Authorization: Bearer '.$this->token
            ])
            ->method('POST')
            ->followRedirect(true)
            ->payload([
                'Detach' => $this->waitUntilComplete === false,
                'Tty' => true,
            ])
            ->run();

        if ($this->waitUntilComplete === false) {
            return Result::success($this, 'Command executed');
        }
        $responseData = $response->getData()['rawResponse'];
        $this->printTaskInfo('Command output: '.PHP_EOL.$responseData);

        $response = $this->taskHttpRequest($this->host . '/api/endpoints/' . $this->endpointNum . '/docker/exec/'.$execId.'/json')
            ->headers([
                'Authorization: Bearer '.$this->token
            ])
            ->method('GET')
            ->run();
        $responseData = $response->getData()['response'];
        $isRunning = (boolean) $responseData->Running;
        $exitCode = $responseData->ExitCode;

        while ($isRunning) {
            $response = $this->taskHttpRequest($this->host . '/api/endpoints/' . $this->endpointNum . '/docker/exec/'.$execId.'/json')
                ->headers([
                    'Authorization: Bearer '.$this->token
                ])
                ->method('GET')
                ->run();
            $responseData = $response->getData()['response'];
            $isRunning = (boolean) $responseData->Running;
            $exitCode = $responseData->ExitCode;
            sleep(1);
        }

        if ($exitCode !== 0) {
            return Result::error($this, "Command exited with code: $exitCode");
        }

        return Result::success($this, 'Command executed', [
            'output' => $responseData,
            'exitCode' => $exitCode,
        ]);
    }
}
