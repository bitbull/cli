<?php

namespace Bitbull\Cli\Tasks\Portainer;

use Bitbull\Cli\Tasks\BaseTask;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Common\DynamicParams;
use Robo\LoadAllTasks;
use Robo\Result;

class PortainerDeleteStack extends BaseTask
{
    use DynamicParams, UtilsTasks, LoadAllTasks;

    /** @var string */
    protected $token = null;

    /** @var string */
    protected $host = null;

    /** @var string */
    protected $stackName = null;

    /** @var string */
    protected $swarmId = null;

    /** @var string */
    protected $endpointNum = 1;

    public function __construct($host)
    {
        parent::__construct();
        
        $this->host = $host;
    }

    /**
     * Delete an existing stack
     *
     * Example usage:
     * ```php
     * $response = $this->taskPortainerDeleteStack('http://docker-swarm.example.com:9000')
     *      ->stackName('example')
     *      ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $this->printTaskDebug('Retrieving all stacks data..');
        $response = $this->taskHttpRequest($this->host . '/api/endpoints/' . $this->endpointNum . '/stacks')
            ->headers([
                'Authorization: Bearer '.$this->token
            ])
            ->method('GET')
            ->run();

        $stacks = $response->getData()['response'];
        $this->printTaskDebug('Retrieved '.sizeof($stacks).' stacks');
        $stackToDelete = null;
        foreach ($stacks as $stack) {
            if($this->swarmId == null){
                $this->swarmId = $stack->SwarmId;
            }

            if ($stack->Name == $this->stackName){
                $stackToDelete = $stack;
                $this->printTaskDebug("Found stack with name $this->stackName");
                break;
            }
        }

        if($stackToDelete === null){
            return Result::error($this, "Stack $this->stackName not found");
        }

        $this->printTaskInfo("Stack $this->stackName exist, deleting it..");

        $this->taskHttpRequest($this->host . '/api/endpoints/' . $this->endpointNum . '/stacks/' . $stackToDelete->Id)
            ->headers([
                'Authorization: Bearer '.$this->token
            ])
            ->method('DELETE')
            ->run();
        $this->printTaskInfo("Stack $stackToDelete->Name deleted");

        return Result::success($this, 'Stack deleted', [
            'stack' => $stackToDelete
        ]);
    }
}
