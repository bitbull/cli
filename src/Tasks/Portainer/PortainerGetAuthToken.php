<?php

namespace Bitbull\Cli\Tasks\Portainer;

use Bitbull\Cli\Tasks\BaseTask;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Common\DynamicParams;
use Robo\LoadAllTasks;
use Robo\Result;

class PortainerGetAuthToken extends BaseTask
{
    use DynamicParams, UtilsTasks, LoadAllTasks;

    /** @var string */
    protected $username = null;

    /** @var string */
    protected $password = null;

    /** @var string */
    protected $host = null;

    public function __construct($host)
    {
        parent::__construct();

        $this->host = $host;
    }

    /**
     * Get a Portainer auth token
     *
     * Example usage:
     * ```php
     * $response = $this->taskPortainerGetAuthToken('http://docker-swarm.example.com:9000')
     *      ->username('admin')
     * 		->password('123abc')
     * ->run();
     * $token = $response->getData()['token'];
     * ```
     *
     * @return Result
     */
    function run()
    {
        $this->printTaskDebug("Request token for user $this->username..");
        $response = $this->taskHttpRequest($this->host . '/api/auth')
            ->payload([
                'Username' => $this->username,
                'Password' => $this->password,
            ])
            ->method('POST')
            ->run();
        
        if (!$response->wasSuccessful()) {
            $this->printTaskDebug('Error during token request');
            return $response;
        }

        $response = $response->getData()['response'];
        $token = $response->jwt;

        $this->printTaskDebug('Token successfully retrieved');

        return Result::success($this, 'Logged to portainer', [
            'token' => $token
        ]);
    }

    /**
     * Called in place of `run()` for simulated tasks.
     *
     * @param null|array $context
     * @throws \Exception
     * @return Result
     */
    public function simulate($context)
    {
        return Result::success($this, 'Logged to portainer', [
            'token' => 'xxxxxxxxxxxxxxxxxxxxxxxx'
        ]);
    }
}
