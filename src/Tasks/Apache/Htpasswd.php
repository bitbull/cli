<?php

namespace Bitbull\Cli\Tasks\Apache;

use Robo\Common\DynamicParams;
use Robo\Common\ExecCommand;
use Robo\Result;
use Bitbull\Cli\Tasks\BaseTask;
use Symfony\Component\Filesystem\Filesystem;

class Htpasswd extends BaseTask {

    use DynamicParams, ExecCommand;

    /** @var string */
    protected $executable = "";

    /** @var Filesystem */
    protected $fs = null;

    /** @var array */
    protected $users = [];

    /** @var string */
    protected $file = null;

    /**
     * Constructor
     */
    function __construct($file)
    {
        parent::__construct();

        $this->executable = "htpasswd";
        $this->fs = new Filesystem();
        $this->file = $file;
    }

    /**
     * Enable or disable a specific module
     *
     * Example usage:
     * ```php
     * $this->taskHtpasswd('./authfile')
     * 		->addUser('username', 'password')
     * ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $this->fs->touch($this->file);

        foreach ($this->users as $user) {
            $command = $this->executable.' -b '.$this->file.' '.$user['username'].' '.$user['password'];

            $result = $this->executeCommand($command);

            if (!$result->wasSuccessful()) {
                return $result;
            }
        }

        return Result::success($this);
    }

    /**
     * Add new user
     *
     * @param $username
     * @param $password
     * @return Htpasswd
     */
    function addUser($username, $password)
    {
        array_push($this->users, [
            'username' => $username,
            'password' => $password,
        ]);

        return $this;
    }
}
