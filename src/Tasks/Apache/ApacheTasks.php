<?php

namespace Bitbull\Cli\Tasks\Apache;

trait ApacheTasks
{
    /**
     * Invalidate CloudFront CDN
     *
     * @param $file string
     * @return Htpasswd
     */
    protected function taskHtpasswd($file)
    {
        return $this->task(Htpasswd::class, $file);
    }

}