<?php

namespace Bitbull\Cli\Tasks\Laravel;

use Bitbull\Cli\Tasks\BaseTaskCommand;
use Robo\Common\DynamicParams;
use Robo\Result;

/**
 * Enable or disable a specific module
 *
 * Example usage:
 * ```php
 * $this->taskArtisan()
 * 		->command('php artisan list')
 * 		->params([])
 * ->run();
 * ```
 *
 * @return Result
 */
class Artisan extends BaseTaskCommand {

    use DynamicParams;

    /** @var string */
    protected $executable = "artisan";

    /** @var string */
    protected $command = null;

    /** @var array */
    protected $params = [];

    /**
     * {@inheritdoc}
     */
    public function getCommand()
    {
        return 'php '.$this->executable.' '.$this->command.' '. implode(' ', $this->params);
    }

    /**
     * Add parameter
     *
     * @param $params string
     * @return Artisan
     */
    function addParam($params)
    {
        array_push($this->params, $params);

        return $this;
    }

}
