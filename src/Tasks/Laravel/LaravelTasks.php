<?php

namespace Bitbull\Cli\Tasks\Laravel;

trait LaravelTasks
{
    /**
     * Magento n98 task
     *
     * @return Artisan
     */
    protected function taskArtisan()
    {
        return $this->task(Artisan::class);
    }
}