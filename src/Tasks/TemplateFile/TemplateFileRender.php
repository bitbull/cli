<?php

namespace Bitbull\Cli\Tasks\TemplateFile;

use Bitbull\Cli\Tasks\BaseTask;
use Robo\Common\DynamicParams;
use Robo\Common\TaskIO;
use Robo\Result;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Twig_Environment;
use Twig_Loader_Filesystem;
use Symfony\Component\Filesystem\Filesystem;

class TemplateFileRender extends BaseTask {

    use DynamicParams, TaskIO;

    /** @var Twig_Environment */
    protected $engine = null;

    /** @var Filesystem */
    protected $fs = null;

    /** @var string */
    protected $template = '';

    /** @var array */
    protected $params = [];

    /** @var string */
    protected $destination = '';

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $loader = new Twig_Loader_Filesystem([
            __DIR__.'/../../Templates',
            __DIR__.'/../../Templates/Wordpress',
            __DIR__.'/../../Templates/Magento2',
            __DIR__.'/../../Templates/Docker',
            __DIR__.'/../../Templates/Laravel',
        ]);
        $this->engine = new Twig_Environment($loader);
        $this->fs = new Filesystem();
    }

    /**
     * Render Twig template file
     *
     * Example usage:
     * ```php
     * $this->taskTemplateFileRender()
     * 		->template('./templateFile.twig')
     * 		->params([])
     * 		->destination('./renderedFile.txt')
     * ->run();
     * ```
     *
     * @return Result
     */
    function run()
    {
        $destinationDirectory = dirname($this->destination);

        try {
            $this->fs->mkdir($destinationDirectory);
        } catch (IOExceptionInterface $e) {
            return Result::error($this, 'Cannot create directory '.$destinationDirectory.': '.$e->getMessage());
        }

        $this->printTaskDebug("Generating template content from $this->template..");

        try{
            $renderedContent = $this->engine->render(
                $this->template,
                $this->params
            );
            $this->printTaskDebug("Content generated");
        }catch (\Twig_Error_Loader $e){
            return Result::error($this, "Cannot find template $this->template");
        }catch (\Twig_Error_Syntax $e){
            return Result::error($this, "Template $this->template syntax error: ".$e->getMessage());
        }catch (\Twig_Error_Runtime $e){
            return Result::error($this, "Error rendering template $this->template: ".$e->getMessage());
        }

        $this->printTaskDebug("Writing $this->destination file..");
        $this->fs->dumpFile($this->destination, $renderedContent);
        $this->printTaskDebug("File $this->destination written");

        return Result::success($this);
    }
}
