<?php

namespace Bitbull\Cli\Tasks\TemplateFile;

trait TemplateFileTasks
{
    /**
    * Render Twig template
    *
    * @return TemplateFileRender
    */
    protected function taskTemplateFileRender()
    {
        return $this->task(TemplateFileRender::class);
    }
}