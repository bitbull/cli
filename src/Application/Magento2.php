<?php

namespace Bitbull\Cli\Application;

use Bitbull\Cli\Tasks\Magento2\Magento2Tasks;
use Robo\Result;

class Magento2 extends Composer
{
    use Magento2Tasks;

    /**
     * @inheritdoc
     */
    public function getPublicPath()
    {
        return 'pub';
    }

    /**
     * @inheritdoc
     */
    public function getDomainName()
    {
        $configUrl = $this->getAppConfig('url', null);
        if ($configUrl !== null) {
            return $configUrl;
        }
        /** @var Result $result */
        $result = $this->taskN98magerun2()
            ->command('sys:url:list')
            ->run();
        return $result->getMessage();
    }

    /**
     * @inheritdoc
     */
    public function getUnwantedPaths()
    {
        return [
            'CHANGELOG.md',
            'COPYING.txt',
            'LICENSE.txt',
            'LICENSE_EE.txt',
            'LICENSE_AFL.txt',
            'README.md',
            'auth.json.sample',
            'grunt-config.json.sample',
            'nginx.conf.sample',
            'package.json.sample',
            'php.ini.sample',
            'Gruntfile.js.sample',
            '.htaccess.sample',
            'PULL_REQUEST_TEMPLATE.md',
            'README_EE.md',
            'ISSUE_TEMPLATE.md',
            'CONTRIBUTING.md',
            'dev/',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDefaultConfigData()
    {
        return [
            'name' => 'magento',
            'env' => 'developer',
            'cache' => false,
            'additionalCache' => [],
            'url' => 'http://localhost',
            'admin' => 'admin',
            'sample' => 'no',
            'version' => '',
            'preferDist' => '',
            'langs' => ['en_US'],
            'theme' => [
                'frontend' => 'Magento/luma',
                'backend' => 'Magento/backend'
            ],
            'key' => '',
            'db' => [
                'host' => '127.0.0.1',
                'port' => '3306',
                'name' => 'magento',
                'username' => 'root',
                'password' => 'root',
            ],
            'redis' => [
                'host' => '127.0.0.1',
                'port' => '6379',
                'password' => null,
            ],
            'memcached_session' => [
                'host' => false,
                'port' => 11211,
                'driver' => 'memcached',
            ],
            'installDate' => date("D, j M Y H:i:s O"),
            'varnishHostsConfigFile' => '',
            'isEnterprise' => false, // Set to true for Enterprise Edition.
            'disableSessionLocking' => false,
            'enableRedisCacheCompression' => false, // Set to 'true' to enable compression at level 1 (fastest). Higher levels degrade performance.
            'enableRedisFpcCompression' => false, // Remember to set isEnterprise to true if using EE. EE FPC already uses compression and in case of EE, this flag will be used for Tags only.
            'redisCacheCompressionLib' => 'gzip', // Use the standard gzip compression lib but 'zstd' is highly recommended if installed on the host
            'redisSessionCompressionLib' => 'gzip', // Use the standard gzip compression lib but 'snappy' is highly recommended if installed on the host
            'redisCacheCompressionThreshold' => '860', // 860 is an arbitrary 'good enough' value
            'redisSessionCompressionThreshold' => '2048', // Default at 2048, could be set lower to improve compressibility
            'redisConnectRetries' => '2',  // Default is 1, but 2 reduces errors due to random connection failures
            'anonymize' => [
                'customer_entity' => [
                    'primaryKey' => 'entity_id',
                    'columns' => [
                        'email' => 'randomEmail',
                        'firstname' => 'firstName',
                        'lastname' => 'lastName',
                    ]
                ],
                'customer_address_entity' => [
                    'primaryKey' => 'entity_id',
                    'columns' => [
                        'company' => 'company|optional',
                        'firstname' => 'firstName',
                        'lastname' => 'lastName',
                        'city' => 'city',
                        'postcode' => 'postcode',
                        'street' => 'streetAddress',
                        'telephone' => 'phoneNumber',
                        'vat_id' => 'vatId|optional'
                    ]
                ],
                'customer_grid_flat' => [
                    'primaryKey' => 'entity_id',
                    'columns' => [
                        'email' => 'safeEmail',
                        'name' => 'name',
                        'shipping_full' => 'address',
                        'billing_full' => 'address',
                        'billing_firstname' => 'firstName',
                        'billing_lastname' => 'lastName',
                        'billing_telephone' => 'phoneNumber',
                        'billing_postcode' => 'postcode',
                        'billing_street' => 'streetAddress',
                        'billing_city' => 'city',
                        'billing_vat_id' => 'vatId|optional'
                    ]
                ],
                'sales_order' => [
                    'primaryKey' => 'entity_id',
                    'columns' => [
                        'customer_email' => 'safeEmail',
                        'customer_firstname' => 'firstName',
                        'customer_lastname' => 'lastName',
                    ]
                ],
                'sales_order_grid' => [
                    'primaryKey' => 'entity_id',
                    'columns' => [
                        'customer_email' => 'safeEmail',
                        'customer_name' => 'name',
                        'shipping_name' => 'name',
                        'billing_name' => 'name',
                        'shipping_address' => 'address',
                        'billing_address' => 'address'
                    ]
                ],
                'sales_order_address' => [
                    'primaryKey' => 'entity_id',
                    'columns' => [
                        'email' => 'safeEmail',
                        'firstname' => 'firstName',
                        'lastname' => 'lastName',
                        'company' => 'company|optional',
                        'city' => 'city',
                        'postcode' => 'postcode',
                        'street' => 'streetAddress',
                        'telephone' => 'phoneNumber',
                        'vat_id' => 'vatId|optional'
                    ]
                ],
                'sales_invoice_grid' => [
                    'primaryKey' => 'entity_id',
                    'columns' => [
                        'customer_email' => 'safeEmail',
                        'customer_name' => 'name',
                        'billing_name' => 'name',
                        'shipping_address' => 'address',
                        'billing_address' => 'address'
                    ]
                ],
                'sales_creditmemo_grid' => [
                    'primaryKey' => 'entity_id',
                    'columns' => [
                        'customer_email' => 'safeEmail',
                        'customer_name' => 'name',
                        'billing_name' => 'name',
                        'shipping_address' => 'address',
                        'billing_address' => 'address'
                    ]
                ],
                'quote' => [
                    'primaryKey' => 'entity_id',
                    'columns' => [
                        'customer_email' => 'safeEmail',
                        'customer_firstname' => 'firstName',
                        'customer_lastname' => 'lastName',
                    ]
                ],
                'quote_address' => [
                    'primaryKey' => 'address_id',
                    'columns' => [
                        'email' => 'safeEmail',
                        'firstname' => 'firstName',
                        'lastname' => 'lastName',
                        'company' => 'company|optional',
                        'city' => 'city',
                        'postcode' => 'postcode',
                        'street' => 'streetAddress',
                        'telephone' => 'e164PhoneNumber',
                        'vat_id' => 'vatId|optional'
                    ]
                ],
                'newsletter_subscriber' => [
                    'primaryKey' => 'subscriber_id',
                    'columns' => [
                        'subscriber_email' => 'randomEmail',
                    ]
                ],
                'sales_shipment_grid' => [
                    'primaryKey' => 'entity_id',
                    'columns' => [
                        'customer_email' => 'safeEmail',
                        'customer_name' => 'name',
                        'shipping_name' => 'name',
                        'billing_name' => 'name',
                        'shipping_address' => 'address',
                        'billing_address' => 'address'
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createConfig()
    {
        $configData = array_merge($this->getDefaultConfigData(), $this->getAppConfig());

        // This is for BC with previous configurations that specify a single "redis" entry for cache, sessions and FPC
        if (isset($configData['redis'])
            && isset($configData['redis']['host'])
            && $configData['redis']['host'] != $this->getDefaultConfigData()['redis']['host']) {

            $configData['redis_session'] = [
                'host' => $configData['redis']['host'],
                'port' => $configData['redis']['port'],
            ];

            $configData['redis_cache'] = [
                'host' => $configData['redis']['host'],
                'port' => $configData['redis']['port'],
            ];

            $configData['redis_fpc'] = [
                'host' => $configData['redis']['host'],
                'port' => $configData['redis']['port'],
            ];

            unset($configData['redis']);
        }

        // Create Magento 2 env.php config file

        return $this->taskTemplateFileRender()
            ->template('Magento2/env.php.twig')
            ->params($configData)
            ->destination('app/etc/env.php')
            ->run();
    }

    /**
     * @inheritdoc
     */
    public function install()
    {
        $configData = array_merge($this->getDefaultConfigData(), $this->getAppConfig());

        $collection = $this->collectionBuilder();
        $work = $collection->tmpDir();

        $collection->taskN98magerun2()
            ->cwd($this->rootPath)
            ->command('install')
            ->params([
                '--magentoVersionByName="'.$configData['version'].'"',
                '--dbHost="'.$configData['db']['host'].'"',
                '--dbUser="'.$configData['db']['username'].'"',
                '--dbPass="'.$configData['db']['password'].'"',
                '--dbName="'.$configData['db']['name'].'"',
                '--installSampleData='.($configData['sample'] ? 'yes' : 'no').'',
                '--useDefaultConfigParams=yes',
                '--magentoVersionByName="'.$configData['version'].'"',
                '--installationFolder="'.$work.'"',
                '--baseUrl="'.$configData['url'].'"'
            ]);

        $collection->taskFilesystemStack()
            ->mirror($work, $this->rootPath)
            ->remove($work);
        $collection->taskMagentoCLI()->command('module:enable')->params(['--all']);

        return $collection->run();
    }

    /**
     * @inheritdoc
     */
    public function build($env = 'local')
    {
        $configData = array_merge($this->getDefaultConfigData(), $this->getAppConfig());

        $collection = $this->collectionBuilder();
        $collection->addTask($this->getBuildTask($env));

        $self = $this;
        $pathsToClean = [
            'var/cache/',
            'var/log/',
            'var/page_cache/',
            'var/tmp/',
            'var/di/',
            'var/generation/',
            'var/view_preprocessed/',
            'generated/',
            'pub/static/',
        ];
        $collection->taskCleanDirExt(array_filter(
            array_map(function($path) use ($self) {
                return "{$self->rootPath}/{$path}";
            }, $pathsToClean),
            function ($path) use ($self) {
                return $self->_exist($path);
            })
        )->ignoreFile('.htaccess');

        $collection->taskMagentoCLI()->command('setup:di:compile');

        $langs = $configData['langs'];
        $themeFrontend = $configData['theme']['frontend'];
        $themesToBuild = [];
        if (is_string($themeFrontend)) {
            $themesToBuild = [ $themeFrontend ];
        } else if (is_array($themeFrontend)) {
            $themesToBuild = $themeFrontend;
        }
        foreach ($themesToBuild as $themeToBuild) {
            foreach ($langs as $lang) {
                $collection->taskMagentoCLI()->command('setup:static-content:deploy')
                    ->params([
                        '--theme='.$themeToBuild,
                        '-f',
                        $lang
                    ]);
            }
        }

        $themeBackend = $configData['theme']['backend'];
        foreach ($langs as $lang) {
            $collection->taskMagentoCLI()->command('setup:static-content:deploy')
                ->params([
                    '--theme='.$themeBackend,
                    '-f',
                    $lang
                ]);
        }

        return $collection->run();
    }

    /**
     * @inheritdoc
     */
    public function updateDatabase()
    {
        $collection = $this->collectionBuilder();
        $collection->taskMagentoCLI()->command('setup:upgrade')->params([
            '--keep-generated',
        ]);

        return $collection->run();
    }

    /**
     * @inheritdoc
     */
    public function cleanCache($types)
    {
        $task = $this->taskMagentoCLI()->command('cache:clean');
        if (\is_array($types) && \count($types) > 0) {
            foreach ($types as $type) {
                $task->addParam($type);
            }
        }
        return $task->run();
    }

    /**
     * @inheritdoc
     */
    public function cleanFlush($types)
    {
        $task = $this->taskMagentoCLI()->command('cache:flush');
        if (\is_array($types) && \count($types) > 0) {
            foreach ($types as $type) {
                $task->addParam($type);
            }
        }
        return $task->run();
    }

    /**
     * @inheritdoc
     */
    public function setCache($value)
    {
        if($value){
            return $this->taskMagentoCLI()
                ->command('cache:enable')
                ->run();
        }

        return $this->taskMagentoCLI()
            ->command('cache:disable')
            ->run();
    }

    /**
     * @inheritdoc
     */
    public function maintenance($state)
    {
        if($state){
            return $this->taskMagentoCLI()
                ->command('maintenance:enable')
                ->run();
        }

        return $this->taskMagentoCLI()
            ->command('maintenance:disable')
            ->run();
    }

    /**
     * @inheritdoc
     */
    public function reindex($indexes = [])
    {
        $task = $this->taskMagentoCLI()->command('indexer:reindex');
        if (\is_array($indexes) && \count($indexes) > 0) {
            foreach ($indexes as $index) {
                $task->addParam($index);
            }
        }
        return $task->run();
    }

    /**
     * @inheritdoc
     */
    public function resetIndex($indexes = [])
    {
        $task = $this->taskMagentoCLI()->command('indexer:indexer:reset');
        if (\is_array($indexes) && \count($indexes) > 0) {
            foreach ($indexes as $index) {
                $task->addParam($index);
            }
        }
        return $task->run();
    }

}
