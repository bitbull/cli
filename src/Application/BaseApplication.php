<?php

namespace Bitbull\Cli\Application;

use Bitbull\Cli\Tasks\Anonymizer\AnonymizerTasks;
use Bitbull\Cli\Tasks\TemplateFile\TemplateFileTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Collection\CollectionBuilder;
use Robo\Common\IO;
use Robo\LoadAllTasks;
use Robo\Robo;

class BaseApplication implements ApplicationInterface
{
    use TemplateFileTasks, LoadAllTasks, UtilsTasks, IO, AnonymizerTasks;

    /**
     * @var string
     */
    protected $rootPath;

    /**
     * @var string
     */
    protected $configKey = 'app';

    /**
     * @inheritdoc
     */
    public function __construct($rootPath, $configKey = 'app')
    {
        $this->rootPath = $rootPath;
        $builder = CollectionBuilder::create(Robo::getContainer(), $this);
        $this->setBuilder($builder);
        $this->configKey = $configKey;
    }

    /**
     * @inheritdoc
     */
    public function getAppConfig($key = null, $default = [])
    {
        if($key === null){
            $key = $this->configKey;
        }else{
            $key = $this->configKey.'.'.$key;
        }
        return Robo::config()->get($key, $default);
    }

    /**
     * @inheritdoc
     */
    public function getPublicPath()
    {
        return $this->rootPath;
    }

    /**
     * @inheritdoc
     */
    public function getDomainName()
    {
        return $this->getAppConfig('url', 'http://localhost');
    }

    /**
     * @inheritdoc
     */
    public function getUnwantedPaths()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getDefaultConfigData()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getDatabaseConfigurations()
    {
        $configData = array_merge($this->getDefaultConfigData(), $this->getAppConfig());
        $dbData = @$configData['db'] ?: [];

        return [
            'host' => @$dbData['host'] ?: @$dbData['hostname'],
            'port' => @$dbData['port'],
            'name' => @$dbData['name'] ?: @$dbData['database'],
            'username' => @$dbData['username'] ?: @$dbData['user'],
            'password' => @$dbData['password'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function createConfig()
    {
        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function install()
    {
        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function build($env = 'local')
    {
        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function updateDatabase()
    {
        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function cleanCache($types)
    {
        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function cleanFlush($types)
    {
        return $this->cleanCache($types);
    }

    /**
     * @inheritdoc
     */
    public function setCache($value)
    {
        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function maintenance($state)
    {
        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function anonymizeData()
    {
        $configData = array_merge($this->getDefaultConfigData(), $this->getAppConfig());
        if (!isset($configData['anonymize'])) {
            $this->say('Nothing to anonymize');
            return $this->taskEmptySuccess()->run();
        }

        $tables = $configData['anonymize'];

        $dbConfig = $this->getDatabaseConfigurations();
        $oldStopOnFail = $this->stopOnFail(false);
        array_walk($tables, function($table,$tableName) use($dbConfig){
            $task = $this->taskAnonymizeMySQLTable($tableName, $table['primaryKey'])
                ->host($dbConfig['host'])
                ->database($dbConfig['name'])
                ->username($dbConfig['username'])
                ->password($dbConfig['password']);
            foreach ($table['columns'] as $column => $formatter) {
                $task->column($column, $formatter);
            }
            $task->run();
        });
        $this->stopOnFail($oldStopOnFail);
        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function reindex($indexes = [])
    {
        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function resetIndex($indexes = [])
    {
        return $this->taskEmptySuccess()->run();
    }

}
