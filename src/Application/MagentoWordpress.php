<?php
/**
 * Created by PhpStorm.
 * User: fabio
 * Date: 24/02/18
 * Time: 11.16
 */

namespace Bitbull\Cli\Application;

use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\Collection\CollectionBuilder;
use Robo\LoadAllTasks;
use Robo\Result;
use Robo\Robo;

class MagentoWordpress extends BaseApplication
{
    use LoadAllTasks, UtilsTasks;

    /**
     * @var ApplicationInterface
     */
    private $magentoApp;

    /**
     * @var ApplicationInterface
     */
    private $wordpressApp;

    /**
     * @inheritdoc
     */
    public function __construct($rootPath, $configKey = 'app')
    {
        $this->magentoApp = new Magento($rootPath);
        $this->wordpressApp = new Wordpress($rootPath.$this->getAppConfig('wp.publicPath', '/wp'), $this->configKey.'.wp');

        // moved to the bottom since setBuilder called from parent expects the two members to be already initialized
        parent::__construct($rootPath, $configKey);
    }

    /**
     * @inheritdoc
     */
    public function setBuilder(CollectionBuilder $builder)
    {
        $this->builder = $builder;
        $this->magentoApp->setBuilder($builder);
        $this->wordpressApp->setBuilder($builder);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getPublicPath()
    {
        return $this->magentoApp->getPublicPath();
    }

    /**
     * @inheritdoc
     */
    public function getDomainName()
    {
        return $this->magentoApp->getDomainName();
    }

    /**
     * @inheritdoc
     */
    public function getUnwantedPaths()
    {
        return array_merge($this->magentoApp->getUnwantedPaths(), $this->wordpressApp->getUnwantedPaths());
    }

    /**
     * @inheritdoc
     */
    public function createConfig()
    {
        $resultMagento = $this->magentoApp->createConfig();
        if(!$resultMagento->wasSuccessful()){
            return $resultMagento;
        }

        $configData = $this->getAppConfig();
        if(!isset($configData['wp'])){
            return $this->taskEmptyError("Wordpress configurations not found")->run();
        }
        $resultWordpress = $this->wordpressApp->createConfig();
        if(!$resultWordpress->wasSuccessful()){
            return $resultWordpress;
        }

        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function install()
    {
        $resultMagento = $this->magentoApp->install();
        if(!$resultMagento->wasSuccessful()){
            return $resultMagento;
        }
        $resultWordpress = $this->wordpressApp->install();
        if(!$resultWordpress->wasSuccessful()){
            return $resultWordpress;
        }
        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function build($env = 'local')
    {
        $resultMagento = $this->magentoApp->build($env);
        if(!$resultMagento->wasSuccessful()){
            return $resultMagento;
        }
        $resultWordpress = $this->wordpressApp->build($env);
        if(!$resultWordpress->wasSuccessful()){
            return $resultWordpress;
        }

        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function updateDatabase()
    {
        $resultMagento = $this->magentoApp->updateDatabase();
        if(!$resultMagento->wasSuccessful()){
            return $resultMagento;
        }
        $resultWordpress = $this->wordpressApp->updateDatabase();
        if(!$resultWordpress->wasSuccessful()){
            return $resultWordpress;
        }

        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function cleanCache($types)
    {
        $resultMagento = $this->magentoApp->cleanCache();
        if(!$resultMagento->wasSuccessful()){
            return $resultMagento;
        }
        $resultWordpress = $this->wordpressApp->cleanCache();
        if(!$resultWordpress->wasSuccessful()){
            return $resultWordpress;
        }

        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function setCache($value)
    {
        $resultMagento = $this->magentoApp->setCache($value);
        if(!$resultMagento->wasSuccessful()){
            return $resultMagento;
        }
        $resultWordpress = $this->wordpressApp->setCache($value);
        if(!$resultWordpress->wasSuccessful()){
            return $resultWordpress;
        }

        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function anonymizeData()
    {
        $resultMagento = $this->magentoApp->anonymizeData();
        if(!$resultMagento->wasSuccessful()){
            return $resultMagento;
        }
        $resultWordpress = $this->wordpressApp->anonymizeData();
        if(!$resultWordpress->wasSuccessful()){
            return $resultWordpress;
        }

        return $this->taskEmptySuccess()->run();
    }

    /**
     * @inheritdoc
     */
    public function getDefaultConfigData()
    {
        $magentoConfig = $this->magentoApp->getDefaultConfigData();
        $magentoConfig['wp'] = $this->wordpressApp->getDefaultConfigData();

        return $magentoConfig;
    }

    /**
     * @inheritdoc
     */
    public function maintenance($state)
    {
        $resultMagento = $this->magentoApp->maintenance($state);
        if(!$resultMagento->wasSuccessful()){
            return $resultMagento;
        }
        $resultWordpress = $this->wordpressApp->maintenance($state);
        if(!$resultWordpress->wasSuccessful()){
            return $resultWordpress;
        }

        return $this->taskEmptySuccess()->run();
    }
}
