<?php

namespace Bitbull\Cli\Application;

use Bitbull\Cli\Tasks\Anonymizer\AnonymizerTasks;
use Bitbull\Cli\Tasks\Magento\MagentoTasks;

class Magento extends Composer
{
    use MagentoTasks, AnonymizerTasks;

    /**
     * @inheritdoc
     */
    public function getPublicPath()
    {
        return $this->getAppConfig('publicPath', $this->rootPath);
    }

    /**
     * @inheritdoc
     */
    public function getDomainName()
    {
        $configUrl = $this->getAppConfig('url', null);
        if ($configUrl !== null) {
            return $configUrl;
        }

        $result = $this->taskN98magerun()
            ->command('sys:url:list')
            ->run();
        return $result->getMessage();
    }

    /**
     * @inheritdoc
     */
    public function getUnwantedPaths()
    {
        $paths = [
            'downloader/',
            'dev/',
            'LICENSE.html',
            'LICENSE.txt',
            'LICENSE_EE.html',
            'LICENSE_EE.txt',
            'LICENSE_AFL.txt',
            'RELEASE_NOTES.txt',
            '.htaccess.sample',
            'install.php',
            'index.php.sample',
            'php.ini.sample',
            'Readme.md',
        ];

        $customCodePath = $this->getAppConfig('publicPath', null);
        if ($customCodePath !== null) {
            $paths = array_map(function($path) use ($customCodePath) {
                return "{$customCodePath}/{$path}";
            }, $paths);
        }

        return $paths;
    }

    /**
     * @inheritdoc
     */
    public function getDefaultConfigData()
    {
        return [
            'name' => 'magento',
            'env' => 'developer',
            'cache' => false,
            'disableModuleUpdate' => false,
            'fpc' => false,
            'url' => 'http://localhost',
            'session' => 'db',
            'disableSessionLocking' => '0',
            'admin' => 'admin',
            'key' => '',
            'db' => [
                'host' => '127.0.0.1',
                'port' => '3306',
                'name' => 'magento',
                'username' => 'root',
                'password' => 'root',
            ],
            'redis' => [
                'host' => '127.0.0.1',
                'port' => '6379',
                'password' => null,
            ],
            'composer' => [
                'vendor' => 'vendor'
            ],
            'installDate' => date("D, j M Y H:i:s O"),
            'anonymize' => [
                'customer_entity' => [
                    'primaryKey' => 'entity_id',
                    'columns' => [
                        'email' => 'randomEmail',
                    ]
                ],
                'sales_flat_order' => [
                    'primaryKey' => 'entity_id',
                    'columns' => [
                        'customer_email' => 'safeEmail',
                        'customer_firstname' => 'firstName',
                        'customer_lastname' => 'lastName',
                    ]
                ],
                'sales_flat_order_address' => [
                    'primaryKey' => 'entity_id',
                    'columns' => [
                        'email' => 'safeEmail',
                        'firstname' => 'firstName',
                        'lastname' => 'lastName',
                    ]
                ],
                'sales_flat_quote' => [
                    'primaryKey' => 'entity_id',
                    'columns' => [
                        'customer_email' => 'safeEmail',
                        'customer_firstname' => 'firstName',
                        'customer_lastname' => 'lastName',
                    ]
                ],
                'sales_flat_quote_address' => [
                    'primaryKey' => 'address_id',
                    'columns' => [
                        'email' => 'safeEmail',
                        'firstname' => 'firstName',
                        'lastname' => 'lastName',
                    ]
                ],
                'newsletter_subscriber' => [
                    'primaryKey' => 'subscriber_id',
                    'columns' => [
                        'subscriber_email' => 'randomEmail',
                    ]
                ]
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public function createConfig()
    {
        $configData = array_merge($this->getDefaultConfigData(), $this->getAppConfig());

        // This is for BC with previous configurations that specify a single "redis" entry for cache, sessions and FPC
        if (isset($configData['redis'])
            && isset($configData['redis']['host'])
            && $configData['redis']['host'] != $this->getDefaultConfigData()['redis']['host']) {

            $configData['redis_session'] = [
                'host' => $configData['redis']['host'],
                'port' => $configData['redis']['port'],
            ];

            $configData['redis_cache'] = [
                'host' => $configData['redis']['host'],
                'port' => $configData['redis']['port'],
            ];

            $configData['redis_fpc'] = [
                'host' => $configData['redis']['host'],
                'port' => $configData['redis']['port'],
            ];

            unset($configData['redis']);
        }

        $mageRoot = '.';
        if ($this->_exist('composer.json')) {
            $composer = $this->loadJsonFile('composer.json');
            $mageRoot =  isset($composer['extra']) && isset($composer['extra']['magento-root-dir']) ? $composer['extra']['magento-root-dir'] : $mageRoot;
        }
        $tasksCollection = $this->collectionBuilder();

        // Create Magento local.xml configuration file
        $tasksCollection->taskTemplateFileRender()
            ->template('Magento/local.xml.twig')
            ->params($configData)
            ->destination("{$mageRoot}/app/etc/local.xml")
        ;
        // Enable Cm_RedisSession if needed
        if (isset($configData['redis_session']) && isset($configData['redis_session']['host'])
            && $this->_exist("{$mageRoot}/app/etc/modules/Cm_RedisSession.xml"))
        {
            $tasksCollection->taskReplaceInFile("{$mageRoot}/app/etc/modules/Cm_RedisSession.xml")
                ->from('<active>false</active>')
                ->to('<active>true</active>')
            ;
        }
        // Update errors/local.xml :: email_address
        if (isset($configData['error_email'])) {
            $tasksCollection->taskReplaceInFile("{$mageRoot}/errors/local.xml")
                ->regex("/\<email_address\>.*?\<\/email_address\>/")
                ->to("<email_address>{$configData['error_email']}</email_address>")
            ;
        }

        return $tasksCollection->run();
    }

    /**
     * @inheritdoc
     */
    public function install()
    {
        $configData = array_merge($this->getDefaultConfigData(), $this->getAppConfig());

        $collection = $this->collectionBuilder();
        $work = $collection->tmpDir();

        $collection->taskN98magerun()
            ->command('sys:setup:run')
            ->params([
                '--dbHost="'.$configData['db']['host'].'"',
                '--dbUser="'.$configData['db']['username'].'"',
                '--dbPass="'.$configData['db']['password'].'"',
                '--dbName="'.$configData['db']['name'].'"',
                '--installSampleData='.($configData['sample'] ? 'yes' : 'no').'',
                '--useDefaultConfigParams=yes',
                '--magentoVersionByName="'.$configData['version'].'"',
                '--installationFolder="'.$work.'"',
                '--baseUrl="'.$configData['url'].'"'
            ]);
        $collection->taskFilesystemStack()
            ->mirror($work, $this->rootPath)
            ->remove($work);

        return $collection->run();
    }

    /**
     * @inheritdoc
     */
    public function updateDatabase()
    {
        $collection = $this->collectionBuilder();
        $collection->taskN98magerun()
            ->command('sys:setup:run');

        return $collection->run();
    }

    /**
     * @inheritdoc
     */
    public function cleanCache($types)
    {
        $task = $this->taskN98magerun()->command('cache:clean');
        if (\is_array($types) && \count($types) > 0) {
            foreach ($types as $type) {
                $task->addParam($type);
            }
        }
        return $task->run();
    }

    /**
     * @inheritdoc
     */
    public function cleanFlush($types)
    {
        $task = $this->taskN98magerun()->command('cache:flush');
        if (\is_array($types) && \count($types) > 0) {
            foreach ($types as $type) {
                $task->addParam($type);
            }
        }
        return $task->run();
    }

    /**
     * @inheritdoc
     */
    public function setCache($value)
    {
        if($value){
            return $this->taskN98magerun()
                ->command('cache:enable')
                ->run();
        }else{
            return $this->taskN98magerun()
                ->command('cache:disable')
                ->run();
        }
    }

    /**
     * @inheritdoc
     */
    public function maintenance($state)
    {
        $maintenanceFile = $this->rootPath."/maintenance.flag";

        if($state){
            return $this->_touch($maintenanceFile);
        }else{
            if($this->_exist($maintenanceFile)){
                return $this->_remove($maintenanceFile);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function reindex($indexes = [])
    {

        if (\is_array($indexes) && \count($indexes) > 0) {
            $task = $this->taskN98magerun()->command('index:reindex');
            foreach ($indexes as $index) {
                $task->addParam($index);
            }
        }else{
            $task = $this->taskN98magerun()->command('index:reindex:all');
        }
        return $task->run();
    }
}
