<?php

namespace Bitbull\Cli\Application;

use Bitbull\Cli\Tasks\Laravel\LaravelTasks;
use Bitbull\Cli\Tasks\TemplateFile\TemplateFileTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\LoadAllTasks;
use Robo\Robo;

class Laravel extends Composer
{
    use LaravelTasks;

    /**
     * @inheritdoc
     */
    public function getPublicPath()
    {
        return 'public/';
    }

    /**
     * @inheritdoc
     */
    public function getDomainName()
    {
        $configUrl = $this->getAppConfig('url', null);
        if ($configUrl !== null) {
            return $configUrl;
        }
        $envFile = $this->loadEnvFile('.env');
        if (!isset($envFile['APP_URL'])) {
            return parent::getDomainName();
        }

        return $envFile['APP_URL'];
    }

    /**
     * @inheritdoc
     */
    public function getUnwantedPaths()
    {
        return [
            'CHANGELOG.md',
            'readme.md',
            '.env.example',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDefaultConfigData()
    {
        return [
            'name' => 'Laravel',
            'env' => 'local',
            'key' => '',
            'debug' => true,
            'url' => 'http://localhost',
            'logChannel' => 'stack',
            'session' => 'file',
            'cache' => 'file',
            'queue' => 'file',
            'db' => [
                'connection' => 'mysql',
                'host' => '127.0.0.1',
                'port' => '3306',
                'name' => 'laravel',
                'username' => 'root',
                'password' => 'root',
            ],
            'redis' => [
                'host' => '127.0.0.1',
                'port' => '6379',
                'password' => null,
            ],
            'mail' => [
                'driver' => 'smtp',
                'host' => 'smtp.mailtrap.io',
                'port' => '2525',
                'name' => null,
                'user' => null,
                'password' => null,
            ],
            'anonymize' => [
                'users' => [
                    'primaryKey' => 'id',
                    'columns' => [
                        'email' => 'randomEmail',
                        'name' => 'name',
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createConfig()
    {
        $configData = array_merge($this->getDefaultConfigData(), $this->getAppConfig());

        // Create Laravel .env configuration file
        return $this->taskTemplateFileRender()
            ->template('Laravel/env.twig')
            ->params($configData)
            ->destination('.env')
            ->run();
    }

    /**
     * @inheritdoc
     */
    public function install()
    {
        $collection = $this->collectionBuilder();
        $collection->taskComposerCreateProject()
            ->preferDist(true)
            ->source('laravel/laravel')
            ->target($this->rootPath);

        $collection->taskArtisan()->command('migrate:install');

        return $collection->run();
    }

    /**
     * @inheritdoc
     */
    public function updateDatabase()
    {
        $collection = $this->collectionBuilder();
        $collection->taskArtisan()->command('migrate')->params([
            '--force'
        ]);
        return $collection->run();
    }

    /**
     * @inheritdoc
     */
    public function cleanCache($types)
    {
        $collection = $this->collectionBuilder();

        $collection->taskArtisan()->command('cache:clear');
        $collection->taskComposerDumpAutoload();
        $collection->taskArtisan()->command('queue:restart');

        return $collection->run();
    }

    /**
     * @inheritdoc
     */
    public function maintenance($state)
    {
        if($state){
            return $this->taskArtisan()
                ->command('down')
                ->run();
        }else{
            return $this->taskArtisan()
                ->command('up')
                ->run();
        }
    }
}
