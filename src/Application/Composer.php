<?php

namespace Bitbull\Cli\Application;

use Bitbull\Cli\Tasks\Laravel\LaravelTasks;
use Bitbull\Cli\Tasks\TemplateFile\TemplateFileTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\LoadAllTasks;
use Robo\Robo;

class Composer extends BaseApplication
{
    /**
     * @inheritdoc
     */
    public function install()
    {
        $configData = array_merge($this->getDefaultConfigData(), $this->getAppConfig());

        $collection = $this->collectionBuilder();
        $collection->taskComposerInit()
            ->projectName($configData['name']);

        return $collection->run();
    }

    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */
    public function build($env = 'local')
    {
        $task = $this->getBuildTask($env);
        return $task->run();
    }

    /**
     * Get build task
     *
     * @param string $env
     * @return \Robo\Task\Composer\Install|\Robo\Task\Composer\Update
     */
    protected function getBuildTask($env = 'local') {
        switch ($env) {
            case 'local':
                $task = $this->taskComposerUpdate()
                    ->workingDir($this->rootPath);
                break;
            case 'development':
            case 'dev':
                $task = $this->taskComposerInstall()
                    ->workingDir($this->rootPath)
                    ->preferSource();
                break;
            case 'production':
            case 'prod':
                $task = $this->taskComposerInstall()
                    ->workingDir($this->rootPath)
                    ->preferDist()
                    ->optimizeAutoloader()
                    ->noDev();
                break;
            default:
                $task = $this->taskComposerInstall()
                    ->workingDir($this->rootPath);
        }
        return $task;
    }

    /**
     * @inheritdoc
     */
    public function updateDatabase()
    {
        $composer = $this->loadJsonFile('composer.json');
        $dependencies = $composer['require'];
        if(array_key_exists('robmorgan/phinx', $dependencies)){
            return $this->taskExec('vendor/bin/phinx migrate')->run();
        }

        return parent::updateDatabase();
    }

    /**
     * @inheritdoc
     */
    public function cleanCache($types)
    {
        $collection = $this->collectionBuilder();
        $collection->taskComposerDumpAutoload();

        return $collection->run();
    }
}
