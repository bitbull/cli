<?php

namespace Bitbull\Cli\Application;

use Bitbull\Cli\Tasks\Laravel\LaravelTasks;
use Bitbull\Cli\Tasks\TemplateFile\TemplateFileTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Robo\LoadAllTasks;
use Robo\Robo;

class NPM extends BaseApplication
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */
    public function build($env = 'local')
    {
        $task = $this->taskNpmInstall()
            ->workingDir($this->rootPath);

        if($env === 'production' || $env === 'prod'){
            $task->noDev();
        }

        return $task->run();
    }

}