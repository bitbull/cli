<?php

namespace Bitbull\Cli\Application;

use Bitbull\Cli\Tasks\TemplateFile\TemplateFileTasks;
use Bitbull\Cli\Tasks\Utils\UtilsTasks;
use Bitbull\Cli\Tasks\Wordpress\WordpressTasks;
use Robo\Common\TaskIO;
use Robo\LoadAllTasks;
use Robo\Result;
use RandomLib;
use Robo\Robo;
use SecurityLib;

class Wordpress extends BaseApplication
{
    use WordpressTasks, TaskIO;

    /**
     * @inheritdoc
     */
    public function getPublicPath()
    {
        return $this->rootPath;
    }

    /**
     * @inheritdoc
     */
    public function getUnwantedPaths()
    {
        return [
            'wp-config-sample.php',
            'readme.html',
            'wp-admin/install.php',
            'license.txt',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getDefaultConfigData()
    {
        return [
            'name' => 'wordpress',
            'debug' => true,
            'url' => 'http://localhost',
            'db' => [
                'host' => '127.0.0.1',
                'port' => '3306',
                'name' => 'wordpress',
                'user' => 'root',
                'password' => 'root',
                'prefix' => 'wp_',
            ],
            'keys' => [
                'authKey' => null,
                'secureAuthKey' => null,
                'loggedInKey' => null,
                'nonceKey' => null,
                'authSalt' => null,
                'secureAuthSalt' => null,
                'loggedInSalt' => null,
                'nonceSalt' => null,
            ],
            'multisite' => [
                'allowMultisite' => false,
                'multisite' => false,
                'subdomainInstall' => false,
                'domainCurrentSite' => 'http://localhost',
                'pathCurrentSite' => '',
                'siteIdCurrentSite' => null,
                'blogIdCurrentSite' => null,
            ],
            'anonymize' => [
                'wp_users' => [
                    'primaryKey' => 'ID',
                    'columns' => [
                        'user_email' => 'randomEmail',
                        'display_name' => 'name',
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function createConfig()
    {
        $configData = array_merge($this->getDefaultConfigData(), $this->getAppConfig());

        // Create Wordpress wp-config.php configuration file
        return $this->taskTemplateFileRender()
            ->template('Wordpress/wp-config.php.twig')
            ->params($configData)
            ->destination("{$configData['publicPath']}/wp-config.php")
            ->run();
    }

    /**
     * @inheritdoc
     */
    public function install()
    {
        $configData = array_merge($this->getDefaultConfigData(), $this->getAppConfig());

        if(!isset($configData['admin_password'])){
            $configData['admin_password'] = $this->strRandom(32);
            $this->printTaskInfo('Generated password: '.$configData['admin_password']);
        }

        $collection = $this->collectionBuilder();
        $work = $collection->tmpDir();
        $collection->taskWPCLI()
            ->command('core install')
            ->params([
                '--path="'.$work.'"',
                '--url="'.$configData['url'].'"',
                '--title="'.$configData['title'].'"',
                '--admin_user="'.$configData['admin_user'].'"',
                '--admin_password="'.$configData['admin_password'].'"',
                '--admin_email="'.$configData['admin_email'].'"',
                '--skip-email'
            ]);
        $collection->taskFilesystemStack()
            ->mirror($work, $this->rootPath)
            ->remove($work);

        return $collection->run();
    }

    /**
     * @inheritdoc
     */
    public function maintenance($state)
    {
        $maintenanceFile = $this->rootPath."/wp-include/.maintenance";

        if($state){
            return $this->taskWriteToFile($maintenanceFile)
                ->line('<?php $upgrading = ' . time() . '; ?>')
                ->run();
        }else{
            if($this->_exist($maintenanceFile)){
                return $this->_remove($maintenanceFile);
            }
        }
    }
}