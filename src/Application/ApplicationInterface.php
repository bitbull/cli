<?php

namespace Bitbull\Cli\Application;

interface ApplicationInterface
{
    /**
     * ApplicationInterface constructor.
     * @param $rootPath
     * @param $configKey
     */
    public function __construct($rootPath, $configKey = 'app');

    /**
     * @param null $key
     * @param null $default
     * @return mixed
     */
    public function getAppConfig($key = null, $default = null);

    /**
     * @return array
     */
    public function getDefaultConfigData();

    /**
     * @return string
     */
    public function getPublicPath();

    /**
     * @return array
     */
    public function getUnwantedPaths();

    /**
     * @return array
     */
    public function getDomainName();

    /**
     * @return array
     */
    public function getDatabaseConfigurations();

    /**
     * @return \Robo\Result
     */
    public function createConfig();

    /**
     * @return mixed
     */
    public function install();

    /**
     * @param $env string
     * @return mixed
     */
    public function build($env = 'local');

    /**
     * @return mixed
     */
    public function updateDatabase();

    /**
     * @param $types array
     * @return mixed
     */
    public function cleanCache($types);

    /**
     * @param $types array
     * @return mixed
     */
    public function cleanFlush($types);

    /**
     * @param $value boolean
     * @return mixed
     */
    public function setCache($value);

    /**
     * @return mixed
     */
    public function anonymizeData();

    /**
     * @param $state
     * @return mixed
     */
    public function maintenance($state);

    /**
     * @param $indexes
     * @return mixed
     */
    public function reindex($indexes = []);

    /**
     * @param $indexes
     * @return mixed
     */
    public function resetIndex($indexes = []);
}
