<?php

namespace Bitbull\Cli\Application;

class ApplicationFactory
{
    /**
     * @var null|string
     */
    private $type = null;

    /**
     * @var null|string
     */
    private $rootPath = null;

    public function __construct($type, $rootPath = null)
    {
        $this->type = $type;
        $this->rootPath = $rootPath !== null ? $rootPath : getcwd();
    }

    /**
     * @return ApplicationInterface
     */
    public function getInstance()
    {
        switch ($this->type) {
            case "laravel":
                return new Laravel($this->rootPath);
                break;
            case "magento":
                return new Magento($this->rootPath);
                break;
            case "magentowp":
                return new MagentoWordpress($this->rootPath);
                break;
            case "magento2":
                return new Magento2($this->rootPath);
                break;
            case "wordpress":
                return new Wordpress($this->rootPath);
                break;
            case "composer":
                return new Composer($this->rootPath);
                break;
            case "npm":
                return new NPM($this->rootPath);
                break;
        }

        return new Composer($this->rootPath);
    }
}