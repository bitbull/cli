<?php
namespace Bitbull\Cli;

use Composer\Autoload\ClassLoader;
use League\Container\ContainerAwareInterface;
use Robo\Collection\CollectionBuilder;
use Robo\Contract\BuilderAwareInterface;
use Robo\Robo;
use Robo\Runner as RoboRunner;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Finder\Finder;

class Runner extends RoboRunner
{
    const ROBOCLASS = 'CustomCommands';
    const ROBOFILE = 'CustomCommands.php';

    const APPLICATION_NAME = 'Bitbull CLI';
    const VERSION = '0.0.0';
    const REPOSITORY = 'bitbull/cli';
    const CONFIG_FILE_NAME = '.bb-cli.yml';
    const COMMAND_CLASSES_NAMESPACE = 'Bitbull\\Cli\\Commands\\';

    /**
     * @var null|string
     */
    private $configFile = null;

    /**
     * @var \Composer\Autoload\ClassLoader
     */
    protected $classLoader = null;

    /**
     * Class Constructor
     *
     * @param null|string $roboClass
     * @param null|string $roboFile
     */
    public function __construct($roboClass = null, $roboFile = null)
    {
        $this->roboClass = $this->getCommandClasses();
        $this->dir = getcwd();
        $this->setSelfUpdateRepository(self::REPOSITORY);
    }

    /**
     * Autoload command's classes
     *
     * @return array
     */
    private function getCommandClasses() {
        $finder = new Finder();
        $finder->files()->in(__DIR__.DIRECTORY_SEPARATOR.'Commands')->notPath('BaseCommand.php');
        $classes = [];
        foreach ($finder as $file) {
            $path = $file->getRelativePath();
            $path = str_replace(DIRECTORY_SEPARATOR, '\\', $path);
            $className = pathinfo($file->getFilename(), PATHINFO_FILENAME);
            array_push($classes, self::COMMAND_CLASSES_NAMESPACE.$path."\\".$className);
        }
        return $classes;
    }

    /**
     * @param array $argv
     * @param null|string $appName
     * @param null|string $appVersion
     * @param null|\Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int
     */
    public function execute($argv, $appName = null, $appVersion = null, $output = null)
    {
        $argv = $this->shebang($argv);
        $argv = $this->processRoboOptions($argv);
        $argv = $this->processCustomOptions($argv);

        if (!$appName) {
            $appName = self::APPLICATION_NAME;
        }

        $appName = $this->getAsciiLogo() . "\n" . $appName;

        $app = new \Bitbull\Cli\Application($appName, $appVersion ? $appVersion : self::VERSION);
        $app->setRootPath($this->dir);
        $commandFiles = $this->getRoboFileCommands($output);
        return $this->run($argv, $output, $app, $commandFiles, $this->classLoader);
    }

    /**
     * Check for custom-specific arguments such as --config, process them,
     * and remove them from the array.
     *
     * @param array $argv
     * @return array
     */
    protected function processCustomOptions($argv)
    {
        // loading custom configuration file
        $pos = $this->arraySearchBeginsWith('--config', $argv) ?: array_search('-c', $argv);
        if ($pos !== false) {
            if (substr($argv[$pos], 0, 9) == '--config=') {
                $this->configFile = substr($argv[$pos], 9);
            } elseif (isset($argv[$pos +1])) {
                $this->configFile = $argv[$pos +1];
                unset($argv[$pos +1]);
            }
            unset($argv[$pos]);
        }

        // loading custom root path
        $pos = $this->arraySearchBeginsWith('--root', $argv) ?: array_search('-c', $argv);
        if ($pos !== false) {
            if (substr($argv[$pos], 0, 7) == '--root=') {
                $this->dir = substr($argv[$pos], 7);
            } elseif (isset($argv[$pos +1])) {
                $this->dir = $argv[$pos +1];
                unset($argv[$pos +1]);
            }
            unset($argv[$pos]);
        }

        return $argv;
    }

    /**
     * Interpolate environment variables from configuration
     *
     */
    private function configEnvInterpolation()
    {
        $config = Robo::config()->export();
        Robo::config()->import($this->replaceEnvVar($config));
    }

    /**
     * Replace environment variables
     *
     * @param $config array
     * @return array
     */
    private function replaceEnvVar($config)
    {
        foreach ($config as $key => $value) {
            if(is_array($value)){
                $config[$key] = $this->replaceEnvVar($value);
            }else{
                preg_match('/\$\{([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)\}/', $value, $match);
                if(sizeof($match) > 0){
                    $config[$key] = str_replace('${'.$match[1].'}', getenv($match[1]), $value);
                }
            }
        }
        return $config;
    }

    /**
     * @param \Composer\Autoload\ClassLoader $classLoader
     *
     * @return $this
     */
    public function setClassLoader(ClassLoader $classLoader)
    {
        $this->classLoader = $classLoader;
        return $this;
    }

    /**
     * @param string|BuilderAwareInterface|ContainerAwareInterface  $commandClass
     * @throws \ReflectionException if the class does not exist.
     * @return null|object
     */
    protected function instantiateCommandClass($commandClass)
    {
        $container = Robo::getContainer();

        // Register the RoboFile with the container and then immediately
        // fetch it; this ensures that all of the inflectors will run.
        // If the command class is already an instantiated object, then
        // just use it exactly as it was provided to us.
        if (is_string($commandClass)) {
            if (!class_exists($commandClass)) {
                return;
            }
            $reflectionClass = new \ReflectionClass($commandClass);
            if ($reflectionClass->isAbstract()) {
                return;
            }

            $commandFileName = "{$commandClass}Commands";
            $container->share($commandFileName, $commandClass);
            $commandClass = $container->get($commandFileName);
        }
        // If the command class is a Builder Aware Interface, then
        // ensure that it has a builder.  Every command class needs
        // its own collection builder, as they have references to each other.
        if ($commandClass instanceof BuilderAwareInterface) {
            $builder = CollectionBuilder::create($container, $commandClass);
            $commandClass->setBuilder($builder);
        }
        if ($commandClass instanceof ContainerAwareInterface) {
            $commandClass->setContainer($container);
        }

        if (method_exists($commandClass, '_init')) {
            $commandClass->_init();
        }

        return $commandClass;
    }

    /**
     * @param null|\Symfony\Component\Console\Input\InputInterface $input
     * @param null|\Symfony\Component\Console\Output\OutputInterface $output
     * @param null|Application $app
     * @param array[] $commandFiles
     * @param null|ClassLoader $classLoader
     *
     * @return int
     */
    public function run($input = null, $output = null, $app = null, $commandFiles = [], $classLoader = null)
    {
        // Create default input and output objects if they were not provided
        if (!$input) {
            $input = new StringInput('');
        }
        if (is_array($input)) {
            $input = new ArgvInput($input);
        }
        if (!$output) {
            $output = new \Symfony\Component\Console\Output\ConsoleOutput();
        }
        $this->setInput($input);
        $this->setOutput($output);

        // If we were not provided a container, then create one
        if (!$this->getContainer()) {
            $rootConfig = $this->dir.DIRECTORY_SEPARATOR.self::CONFIG_FILE_NAME;
            if(substr($this->configFile, 0, 1) == DIRECTORY_SEPARATOR){
                $customConfig = $this->configFile;
            }else{
                $customConfig = $this->dir.DIRECTORY_SEPARATOR.$this->configFile;
            }
            $config = Robo::createConfiguration([$rootConfig, $customConfig]);
            $container = Robo::createDefaultContainer($input, $output, $app, $config, $classLoader);
            $this->setContainer($container);
            $this->configEnvInterpolation();
        }

        $this->registerCommandClasses($app, $commandFiles);

        try {
            $statusCode = $app->run($input, $output);
        } catch (\Exception $e) {
            $statusCode = $e->getCode() ?: 1;
        }

        if ($statusCode) {
            foreach ($this->errorConditions as $msg => $color) {
                $this->yell($msg, 40, $color);
            }
        }
        return $statusCode;
    }

    /**
     * @param string $needle
     * @param string[] $haystack
     *
     * @return bool|int
     */
    protected function arraySearchBeginsWith($needle, $haystack)
    {
        for ($i = 0; $i < count($haystack); ++$i) {
            if (isset($haystack[$i]) && substr($haystack[$i], 0, strlen($needle)) == $needle) {
                return $i;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    private function getAsciiLogo()
    {
        return <<<LOGO
                                                  
                       ``````                     
                        ```````                   
                         ````````````:NMMMMMMMd+.`
                         `.o:.````````oMMMMMMMMMm`
                       ```:Nmh+.```````MMMMMMMMMM`
`                     ````.-.``````````MMMMMMMMMM`
``               .-/sy+:-.```````````.yMMMMMMMMMM`
 `            -ohmNMMMMMMmdhyso+++oshNMMMMMMMMMMd`
 ```       ```dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMdos`
 ```  ```````+MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmoyNM`
 ```````````oNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmmmmd`
 ````````./dMMMMMMMMMMMMMMMMMMdhhyyso++/::--...`` 
 ``:://ohmMMMMMMMMMMMMMMMMMMMs``                  
  `NNMMMMMMMMMMMMMMMMmMMMMMMo`                    
  `dMdyyMMm++/::sMM/.`mMMMMo`                     
  `sM+`.MM+     .MM.``dMMMo`                      
  `/M+`.MM.     `hM.``dMMo`                       
   .+-``+/`     `-+` `/+/`
                                                  
LOGO;

    }
}
