# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.5.2]
### Fixed
- Rise redis timeout and connection retries for M1.
- Rise FPC lifetime to 24 hours for M1
### New Configurations for Magento 1
- Enable FPC gzip compression for data and tags (M1).

## [1.5.1]
### Fixed
- Rise redis timeout and connection retries for M1

## [1.5.0]
### Added
- Add command to deploy AWS ECS service
- Add command to add AWS ALB listeners rules
- Add `bitbull/deploy` image build
- Add support for Magento2 multiple frontend themes
- Add healthCheckPath config
- Add ALB rule conditions support
### New Configurations for Magento 2
- 'isEnterprise' => false, // Set to true for Enterprise Edition.
- 'enableRedisCacheCompression' => false, // Set to 'true' to enable compression at level 1 (fastest). Higher levels degrade performance.
- 'enableRedisFpcCompression' => false, // Remember to set isEnterprise to true if using EE. EE FPC already uses compression and in case of EE, this flag will be used for Tags only.
- 'redisCacheCompressionLib' => 'gzip', // Use the standard gzip compression lib but 'zstd' is highly recommended if installed on the host
- 'redisSessionCompressionLib' => 'gzip', // Use the standard gzip compression lib but 'snappy' is highly recommended if installed on the host
- 'redisCacheCompressionThreshold' => '860', // 860 is an arbitrary 'good enough' value
- 'redisSessionCompressionThreshold' => '2048', // Default at 2048, could be set lower to improve compressibility
- 'redisConnectRetries' => '2',  // Default is 1, but 2 reduces errors due to random connection failures
### Fixed
- Fix identifier when environment variable contains an empty string
- Fix default theme Luma code
- Fix retro-compatibility for aws:alb-target-group-deploy

## [1.4.0]
### Added
- Add disableSessionLocking option for M2
- Add additionalCache settings to set non-standard Magento2 cache
### Fixed
- Fix phar test exclude (keep lib and src from twig)

## [1.3.1]  
### Fixed
- Fix database sync command destination not found

## [1.3.0]
### Added
- Add support for Magento2 to memcached
- Add support for Magento2 config file to load varnish host config file  
### Fixed
- Fix Swarm commander API key header

## [1.2.1]
### Fixed
- Fix Magento 2 env.php file template

## [1.2.0]
### Added
- Improve application cleaning
- Add swarm command to execute shell command into containers
- Mark phar archive with released version
### Fixed
- Fix Magento2 application clean
- Fix executable path when downloaded in composer project
- Fix autoload error

## [1.0.1] - 2018-07-17
### Added
- Changed existing pipelines and added pipeline for tags

## [1.0.0] - 2018-07-10
### Added
- Tag release for production

## [0.4.0] - 2018-03-21
### Added
- Improve install command, docker deploy

## [0.3.0] - 2018-03-21
### Added
- Add deploy pipeline

## [0.2.0] - 2018-03-08
### Added
- Refactor framework

## [0.1.0] - 2018-02-25
### Added
- Readme and contribution instruction
