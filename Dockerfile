FROM php:7.1.17-apache

RUN apt update
RUN apt install -y ssh wget git zip unzip

# Install Composer

ADD https://getcomposer.org/installer composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin/ --filename=composer
RUN rm composer-setup.php

# Install WP-CLI

ADD https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar /usr/local/bin/wp
RUN chmod +x /usr/local/bin/wp

# Install N98-Magerun

ADD https://files.magerun.net/n98-magerun.phar /usr/local/bin/n98-magerun
RUN chmod +x /usr/local/bin/n98-magerun

# Install N98-Magerun2

ADD https://files.magerun.net/n98-magerun2.phar /usr/local/bin/n98-magerun2
RUN chmod +x /usr/local/bin/n98-magerun2

# Install bb-cli

ADD bb-cli.phar /usr/local/bin/bb-cli
RUN chmod +x /usr/local/bin/bb-cli

# Image setup

RUN mkdir /app
VOLUME /app
WORKDIR /app
ENTRYPOINT ["bb-cli"]
CMD ["--version"]
